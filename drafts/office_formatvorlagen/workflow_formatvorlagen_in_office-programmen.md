# Zeit und Nerven bei deinen Hausarbeiten sparen - mit Formatvorlagen

- Deine

Formatvorlagen sind das wohl meist-ignorierte Super-Feature deines 
Office-Programms.

Sie helfen dir dabei, deinen Text zu strukturieren und einheitlich zu 
gestalten… und fast alle anderen Funktionen deines Office-Programms bauen 
darauf auf.\
Also, ja… wirklich - benutz' sie einfach und sie werden dein Leben signifikant 
verbessern! 

Im Grunde gibt es zwei verschiedene Formatvorlagen: 

1. *Zeichenvorlagen* für einzelne Zeichen (z.B. ein Wort)
2. *Absatzvorlagen* für ganze Absätze (jeder Fließtext, bis du einmal mit 
   `Enter` einen neuen Absatz beginnst)

Im Folgenden geht es um die *Absatzvorlagen*.

In einer *Absatzvorlage* stellst du ein, wie ein Absatz dieser Art aussehen 
soll: Schriftgröße, Zeilenabstand, Abstand über und unter dem Absatz, ob ein 
Rahmen drumherum sein soll und so weiter. Diese Vorlage ist dann das, was du in 
einer Leiste oft mit "Standard", "Überschrift 1", "Überschrift 2", "Zitat" 
auswählen kannst.

**Egal wie hässlich diese Einstellungen voreingestellt sind - benutze sie!**

Der große Vorteil ist:\
Jeder Text, den du mit einer Absatzvorlage erstellt hast lässt sich später an 
*einer* Stelle Ändern: in den Einstellungen der Vorlage.\
Damit musst du am Ende deiner Arbeit nur an 5 Stellen die Schriftart und den 
Zeilenabstand ändern, statt jede Überschrift einzeln anzupassen oder jeden 
Absatz noch einmal zu überprüfen.

Aber es kommt noch besser:\
Mit Absatzvorlagen weiß dein Office-Programm jetzt, dass eine Überschrift eine 
Überschrift ist, dein Titel der Titel, die Beschriftung eine Beschriftung und 
dein Zitat ein Zitat. Hä?\
Naja - wenn du einfach nur einen Text hineinschreibst und auf "Schriftgröße 12" 
und "Fett" drückst ist es nur "irgendein Text, Schriftgröße 12, Fett".

Sobald du deinem Programm beigebracht hast, was alles Überschriften sind, kann 
dein Programm dir ganz einfach ein Inhaltsverzeichnis aller Überschriften 
anfertigen. Oder alle Überschriften in der Reihenfolge automatisch nummerieren. 
Oder einen automatischen Verweis auf ein anderes Kapitel (Querverweis). Oder 
eine bestimmte Abbildung und ein Abbildungsverzeichnis, eine Tabelle auf einer 
Bestimmten Seite…

Und noch etwas:\
Erst dadurch, dass dein Programm z.B. die Überschriften kennt, kannst du auch 
eine Barrierefreie Datei erstellen. Screen-reader für Menschen mit 
beeinträchtigter Sicht z.B. brauchen "Sprungmarken". Sie müssen "wissen", was 
ein Kapitel ist um zu einem bestimmten Kapitel zu springen oder welcher Text 
ein Bild beschreibt. 

<!--So kannst du Verweise auf Seiten, Überschriften, Abbildungen und 
Inhaltsverzeichnisse erstellen, die sich je nach Position im Text und Namen 
automatisch ändern. Selber ständig die Nummern deiner Kapitel zu ändern ist 
damit Vergangenheit. Auch das Inhaltsverzeichnis ist dann mit einem Klick in 
neuer Reihenfolge – ohne dass du deine Fingerchen krümmen musst.

Ach ja: Wenn du einmal einen Stil (z. B. für eine Überschrift oder deinen 
Fließtext) angelegt hast, sehen all diese Text-Teile danach gleich aus. Nie 
wieder Angst vor falscher Schriftgröße! Yay!-->

## Hilfe
