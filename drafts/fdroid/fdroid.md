# F-Droid - dein gemütlicher App-Store für freie Software und Privatsphäre

## Background: Das Problem

Ey, mega gruselig:\
letztens hast du dich mit deiner Freundin über dieses eine Dings aus dem neuen 
Videobei YouGlotz unterhalten und heute hast du schon 4 mal das Teil als 
Werbung angezeigt bekommen!\
Wie kommt das!?

Große Plattformen wie Google sind mit ihren Technologien auf etwa 95% aller 
Webseiten und Apps irgendwie eingebaut. Sie bekommen also aus ganz vielen 
Quellen Informationen über dein Online-Verhalten. Auch deine Freunde erzählen - 
ohne dass sie das unbedingt wollen - dadurch immer "ein paar Details" über 
euch.

Nehmen wir mal an: so eine zentrale, große Plattform bekommt jeweils Daten aus 
vier verschiedenen Quellen: 

1. von einem Messenger 
2. einem Preisvergleichs-Portal 
3. einem sozialen Netzwerk 
4. einer Shopping-App 

Sie alle senden an die Plattform:

*"hier interessiert sich ein bestimmtes Gerät mit eindeutiger Werbe-ID und der 
folgenden Zusammenstellung von Apps aus einem bestimmten WLAN für einen 
bestimmte Begriffe und ein bestimmtes Produkt"*.

*"Genau! Und Außerdem gibt es Kontakte zu anderen Geräten mit folgender 
Telefon-Nummer und Werbe-ID"* (sagt der Messenger). 

Wenn ein solches Netzwerk also aus verschiedenen Quellen erfährt, wer von wo zu 
welcher Zeit welche Produkte aufruft, dann lässt sich sehr einfach sehr genau 
nachvollziehen, welche Personen das sind. Auch ob sich die Personen kennen ist 
dann kein Geheimnis, denn es ist sehr Eindeutig:\
wenn z.B. 3 unterschiedliche Geräte innerhalb von 6 Stunden aus der selben 
Stadt und dem selben WLAN eine ähnliche Produktkategorie und Suchbegriffe 
verwenden.\
Das melden schließlich das soziale Netzwerk und die Shopping-App.\
Die Geräte sind außerdem im selben Messenger zu gleichen Zeiten online und 
bewegen sich oft in der Woche im selben WLAN der Uni. Außerdem suchen sie über 
dieses Netzwerk an mehreren Tagen zu ähnlichen Uhrzeiten das Produkt. 

Und dann: Ende gut, alles gut!\
Eine Anzeige im sozialen Netzwerk wurde anscheinend via Messenger Versandt: 
denn nur 10 Minuten später wurde der angezeigte (eindeutige) Gutschein-Code 
schließlich in der Shopping-App bestellt. Nach 3 Tagen Funkstille in der Suche 
aller Geräte gab es dann auch eine weitere Bestellung des gleichen Produkts im 
selben Shop - ebenfalls mit einem Gutschein-Code aus dem sozialen Netzwerk.\
Und das wart ihr beide.
