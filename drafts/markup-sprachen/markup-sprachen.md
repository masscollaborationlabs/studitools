# Markup-Sprachen

> Markup-Sprachen (oder "Auszeichnungssprachen") sind konventionen oder 
> Standards, um einfache Textdateien zu schreiben. Aus diesen werden später 
> Webseiten, Dokumente oder PDF-Dateien generiert, die den "fertigen" Text 
> enthalten.

Vorstellen kannst du dir das so:\
Schreibst du ein Wort auf und machst einen Stern (Asterisk) davor und dahinter 
(etwa `*so*`), dann sticht dieses Wort aus deinem Text heraus. Irgendwie ist es 
*hervorgehoben* -- und das lässt sich später nutzen, wenn das Wort im Text auch 
betont sein soll (z.b. indem es kursiv gesetzt ist).

Alle Markup-Sprachen funktionieren so, nur sind sie dabei unterschiedlich 
komplex. Allgegenwärtig im Internet ist etwa HTML - die "HyperText Markup 
Language". Auch dieser Text hier ist in HTML geschrieben, was dein Browser als 
formatierten Text anzeigt.

Gute Gründe Markup-Sprachen zu verwenden sind:

* **es ist schlicht:**
  * sie sind einfach zu schreiben sein, damit du mehr Zeit mit dem Schreiben 
    verbringst, als mit dem Formatieren deiner Texte.
* **es ist nachhaltig:**
  * du kannst deinen Text auch in 50 Jahren anpassen und wiederverwenden, denn 
    du brauchst kein bestimmtes Programm dafür.
* **es ist vielseitig:**
  * du hast ein Tool für fast jeden Text: ob E-Mail, Blogbeitrag, Chatnachricht 
    oder Fachbuch, denn es gibt Konverter für alle Zwecke.
* **es bietet Wahlfreiheit:**
  * du kannst dich frei entscheiden, mit welchem Programm du deine Dateien 
    bearbeiten und später verwenden willst. Das gilt auch für andere oder im 
    Team: einfacher Text ist schlicht und portabel. 
* **es ist robust:**
  * Layout-Probleme gibt es zunächst nicht, denn deinen Text wandelst du erst 
    später ins gewünschte Format um.
  * die Textdateien lassen sich mit einer Versionsverwaltung dezentral 
    verwalten und ergänzen. Das gilt nicht nur für Programmcode, sondern auch 
    für z.B. sozialwissenschaftliche Texte. 

Wir stellen dir unterschiedliche Markup-Sprachen vor:

* [LaTeX]() etwa ist ein Textsatz-System, was ebenfalls lange Befehle für die 
  Formatierung nutzt.
* [Asciidoc]() hat wesentlich kürzere Befehle (Syntax) und ist außerdem nicht 
  als generelles Schreibtool konzipiert, sondern auf technische Dokumentationen 
  zugeschnitten.
* [Markdown]() ist eine sehr einfache Markup-Sprache, die mit vielen Zusätzen 
  fast jeden Zweck abdeckt und daher sehr häufig verwendet wird.

Am Ende hat jede dieser Markup-Sprachen hat ihre eigenen Vor- und Nachteile:\
Markdown etwa ist sehr schnell gelernt und liest und schreibt sich _sehr_ viel 
einfacher als LaTeX, hat aber auch wesentlich weniger Funktionen für komplexe 
Dokumente.

Hier ein Vergleich:

| **Sprache** | **Beispiel**                                                                  |
|-------------|-------------------------------------------------------------------------------|
| HTML        | `Dieser Text wird später <bold>fett</bold> oder <italic>kursiv</italic> sein` |
| LaTeX       | `Dieser Text wird später \bftext{fett} oder \emph{kursiv} sein`               |
| Asciidoc    | `Dieser Text wird später *fett* oder _kursiv_ sein`                           |
| Markdown    | `Dieser Text wird später **fett** oder *kursiv* sein`                         |

## Umwandeln
