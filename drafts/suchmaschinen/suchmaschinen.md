# Das Passende finden - mit Suchmaschinen

Wenn das Internet ein Eisberg ist, dann findest du mit Google nur die Spitze 
davon. Darunter liegt das Darknet… Milliarden von Webseiten und Inhalten, die 
nie ein Mensch zuvor gesehen hat…

Na gut, nicht alles was Google nicht findet ist gleich "Darknet"… aber 
tatsächlich findest du über Google nur einen sehr geringen Teil des Internets.\
Wie kann das sein?

Jede Suchmaschine "indiziert" Webseiten - erstellt also eine Sammlung ("Index") 
aller gefundenen Seiten. Dazu "Crawlen" (durchsuchen und speichern) 
Suchmaschinen die Inhalte anderer Webseiten. Nach deiner Suchanfrage stellt dir 
eine Suchmaschine nun passende Ergebnisse vor, die sie zuvor gesammelt hat und 
in denen deinen Suchbegriffe auftauchen.

Soweit, so gut.

## Personalisierung ("Meinten Sie: 'Stalking'"!?) 

Was Google und andere kommerzielle Dienste besonders macht ist, dass sie die 
Ergebnisse deiner Suche "personalisieren", in dem Sie viele Daten von dir zu 
einem Profil zusammenfassen und die Ergebnisse weiter "auf dich zuschneiden". 
Das hat leider wenig mit maßgeschneiderter Kleidung zu tun, sondern eher mit 
Stalking… 

**Hinweis:**
*Mit der oft voreingestellten Einstellung "Suchvorschläge anzeigen" 
übermittelst du jeden Tastendruck - schon vor der eigentlichen Suche an eine 
Suchmaschine.* 

- *Standort*: Aus welchem WLAN kommt deine Anfrage? Welche GPS-Koordinaten 
  verraten deinen Standort?
  - *Damit wird dir dein Supermarkt um die Ecke eher angezeigt als alle 
    gleichnamigen Supermärkte um die Ecke. Unternehmen können dir aber auch "im 
    Vorbeigehen" Werbung anzeigen.*
- *Profil* bzw. *Account*: Welche Inhalte, Nachrichten, Themen hast du über 
  deinen Account abonniert? Mit welchen anderen Personen, Adressen, Accounts 
  bist du in Kontakt?
  * *Damit dir eher nur Sachen vorgeschlagen werden, für die du dich bereits 
    interessierst oder Kontakte, die du kennst.*
- *Cookies*: Wenn du eine Webseite besuchst hinterlegt die einen "Cookie" mit 
  Daten-Krümeln, die der Webseite signalisieren, dass du sie besucht hast. 
  Darin sind unterschiedliche viele Daten über deinen Browser oder den 
  Zeitpunkt deines Besuchs, deine angeklickten Objekte, Dauer deines Besuchs 
  usw. gespeichert.
  * *Mit Cookies weiß eine Webseite, dass du gerade eingeloggt bist. Allerdings 
    auch, wann deine letzten Besuche waren oder auf welchen anderen Webseiten 
    du vorher schon warst (wenn du die Cookies nicht löschst).*
- *Referrer*: Klickst du z.B. bei einem Preisvergleichs-Portal ein bestimmtes 
  Angebot eines Shops an, leitet dich die Webseite an einen anderen Shop 
  weiter. Der Referrer sagt dem Shop, dass du von der Preisvergleichs-Seite 
  kommst.
  * *Durch Referrer kann dein Weg von einer Webseite zur anderen nachvollzogen 
    werden. Damit bekommst du bestimmte Angebote (z.B. Rabatt-Aktionen) - aber 
    auch auf jeder Seite eine "Sonderbehandlung": wenn du von einem 
    Musik-Portal kommst wird dir vielleicht eher Musik als Werbung 
    vorgeschlagen.*
- *Werbe-ID*: Dein Smartphone hat eine Werbe-ID, die das Gerät (und damit dich) 
  ziemlich eindeutig identifizieren kann. Diese wird von vielen Apps und 
  Diensten ausgelesen - z.B. von sozialen Netzwerken, die mehrere Merkmale 
  abgleichen, um dich zu identifizieren. Das gilt allerdings auch für Werbung - 
  deswegen auch "Werbe-ID", klar!
  - *Die Werbe-ID ist ein weiteres Merkmal, dass dein Smartphone bzw. dich 
    einer Webseite gegenüber ziemlich einzigartig macht: wenn eine bestimmte 
    Werbe-ID regelmäßig bei einer Webseite Inhalte aufruft… weißte bescheid!.*
- *weitere Geräte-Eigenschaften*: Welche Auflösung hat dein Display? Welche 
  Schriftarten hat dein Gerät installiert? Welche Version hat eigentlich dein 
  Browser? Welchen Browser verwendest du? Welche Apps hast du installiert? 
  * *All diese Merkmale können dazu verwendet werden, dich sehr eindeutig auf 
    einer Webseite zu erkennen. Das ist sogenanntes "Browser-Fingerprinting".*

**Hinweis:**\
*Werbe-Tracking kannst du sichtbar machen!
- Mit der Webseite [Cover your Tracks](https://coveryourtracks.eff.org/) der 
  US-Bürgerrechtsorganisation EFF (Electronic Frontier Foundation) kannst du 
  testen, wie einzigartig und wiedererkennbar dein Browser ist.
- Die Webseite [Webkoll](https://webbkoll.dataskydd.net/de/) der schwedischen 
  Datenschutzorganisation untersucht Webseiten und zeigt dir viele 
  datenschurtzrelevante Informationen an.
- Auf der Webseite [εxodus](https://reports.exodus-privacy.eu.org/de/) kannst 
  du Android-Apps aus dem freien F-Droid Store und dem Google Playstore auf 
  Werbe-Tracker untersuchen.
- Das Firefox-Addon 
  [Lightbeam](https://addons.mozilla.org/de/firefox/addon/lightbeam-3-0/) 
  visualisiert alle Seitenbesuche. Nach und nach siehst du damit, welche 
  Werbe-Firmen auf Webseiten vertreten sind.*

## Alternative Suchmaschinen



