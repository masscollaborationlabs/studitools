# E-Mail komfortabel machen - mit einem Mailprogramm

E-Mail ist und bleibt der wichtigste Kommunikationskanal - auch an der Uni.\
Und auch wenn sie oft verteufelt wird, kann E-Mails schreiben und verwalten 
richtig komfortabel sein… wenn du dafür ein eigenständiges Programm benutzt! 

Wir stellen dir in einem anderen Artikel das freie Mailprogramm [Thunderbird]() 
vor.

Aber warum solltest du überhaupt ein Programm statt dem Uni-Webmail nutzen?

Postfächer:
: in einem Mailprogramm kannst du mehrere Postfächer bzw. 
E-Mail-Adressen auf einen Blick einsehen und verwalten. Kein ansurfen von 10 
unterschiedlichen Webseiten mehr und ein schnellerer Überblick.\

Aliase:
: Natürlich kannst du deine Alias-Adressen ("Spitznamen" deiner richtigen 
E-Mail-Adresse) im Mailprogramm nutzen. Den "Spitznamen" deiner eigentlichen 
Mailadresse kannst du z.B. für Online-Shopping angeben. Wenn ein Spitzname dir 
zu viel Spam erhält, kannst du ihn einfach wieder löschen, ohne deine 
eigentliche E-Mail wechseln zu müssen.

**Hinweis:**\
*E-Mail Aliase musst du bei deinem jeweiligen E-Mail-Anbieter anlegen. Such 
nach Einstellungen von "Aliase" oder "Identitäten". Danach kannst du sie in 
deinem E-Mail-Programm wie eine neue E-Mail-Adresse anlegen, sodass du auch von 
diesem Alias schreiben kannst und Empfänger*innen nicht deine eigentliche 
Adresse sehen können.*

Filter:
: Mit Filtern sortierst du eingehende E-Mails schon beim Empfang automatisch in 
unterschiedliche Ordner ein: Freunde zu Freunden, Uni zu Uni, Newsletter zu 
Newsletter…

Vorlagen:
: Wenn du ständig ähnliche E-Mails schreiben musst, kannst du Vorlagen und 
Autovervollständigungen nutzen um Anreden, Grüße und Inhalte für z.B. spezielle 
Adressat*innen griffbereit zu haben.

Adressbuch:
: Mailprogramme können automatisch die Adressen von deinen E-Mails erfassen. 
Beim Schreiben einer E-Mail werden sie dir schon nach wenigen Buchstaben von 
der Autovervollständigung vorgeschlagen.

Threads / Diskurs-Ansicht:
: In der Diskurs-Ansicht sind deine E-Mails als eine Diskussion angeordnet. 
Antworten werden dabei eingerückt und unter der ursprünglichen E-Mail 
angezeigt. Das schafft Übersicht in längeren Diskussionen, weil du damit 
weitere Unter-Themen aufgreifen kannst. Und wenn das Thema erledigt ist, 
klappst du die Diskussion mit einem Klick wieder ein. So kann aus E-Mails ein 
richtiges Forum werden.

**Hinweis:**\
*In der Diskurs-Ansicht gilt: nicht einfach auf alte E-Mails antworten, wenn du 
an jemanden schreibst (die würden nämlich alten Thema zugeordnet)!\
Falls es ein neues Thema ist, schreibe lieber eine neue E-Mail - mit einem 
aussagekräftigen Betreff oder eröffne mit deiner Antwort ein neues 
(Unter-)Thema.*

Termine, Kalender, Aufgaben:
:  Mailprogramme bieten oft Kalender, in denen du Termine anlegen und mit 
deinem E-Mail-Anbieter synchronisieren kannst. Diese Kalender kannst du dann 
sowohl über Webmail, als auch dein Mailprogramm und z.B. ein Smartphone immer 
auf dem aktuellen Stand halten und von jedem Gerät verändern.\
Sogar Termin-Anfragen lassen sich dann per E-Mail an Personen schicken, die sie 
bestätigen oder ablehnen können. Wenn das nicht reicht, gibt es ja immer noch 
z.B. den 
[DFN-Terminplaner](https://blogs.uni-bremen.de/studytools/2020/03/12/dfn-terminplaner/).\
Zusätzlich kannst du so auch deine Kontakte oder Aufgaben synchronisieren. 

**Hinweis:**\
*Der Webmail-Dienst der Uni Bremen unterstützt all diese Funktionen: 
unterschiedliche Identitäten ("Aliase" / "Spitznamen"), geteilte Kalender 
(lesend / schreibend freigeben für andere Personen), Aufgaben und Kontakte.\
Die offenen Standards, die diesen Austausch ermöglichen heißen z.B. 
[CalDAV](https://de.wikipedia.org/wiki/CalDAV), 
[CardDAV](https://de.wikipedia.org/wiki/CardDAV) und 
[WebDAV](https://de.wikipedia.org/wiki/Webdav). Sie werden von den 
meisten[CardDAV] E-Mail-Anbietern unterstützt.\
Die Anleitungen für die nötigen Einstellungen an der Uni-Bremen findest du auf 
der Webseite des ZfN.*

Verschlüsselung und Privatsphäre:
: E-Mails sind eigentlich keine Briefe, sondern eher digitale Postkarten: jeder 
kann sie einfach lesen. Geschützt werden sie nur auf dem Weg von deinem PC zum 
Server deines E-Mail-Anbieters. Dein E-Mail-Anbieter könnte also deine E-Mails 
mitlesen oder auswerten (viele Gratis-Anbieter werten E-Mails für 
personalisiert Werbung oder eigene Dienste aus. Hast du die AGBs gelesen?).\
Damit wirklich nur die / der Empfänger*in die E-Mail lesen kann, gibt es 
verschiedene Wege deine E-Mails zu verschlüsseln. Dazu zählen Zertifikate 
("S/MIME") oder per PGP / GPG ("Pretty Good Privacy" bzw. "GNU Privacy 
Guard").\
E-Mails kannst du damit nicht nur verschlüsseln sondern auch signieren. Mit der 
Signatur wird die Identität von Absender*innen übeprüft: wenn die Person einen. 
Es Ein gutes Mailprogramm beherrscht diese Signaturprüfung und Verschlüsselung 
ebenfalls oder bietet die Funktion über Erweiterungen an.
