# Linux - tausende freie Betriebssysteme

* Yay, Windows Drölf ist da! Oh no, wieder mehr spionage-Funktionen und 
  "Sicherheits"-Einstellungen?!
* Dein neuen Betriebssystem soll angeblich ältere Geräte langsam machen. Also 
  lieber nicht updaten!?
* Wieso kann der Windows-Explorer eigentlich 2021 immer noch keine Tabs…!?

Du hast bestimmt schonmal von diesem "Linux" gehört: das ist irgend so ein 
Betriebssystem, was deine Nerd-Freunde immer benutzen. Soll super sein - aber 
du kannst mit Technik ja nicht so gut und… es ist *umsonst*(!) - das kann doch 
nicht gut sein!?

Äh, doch! Und du musst kein Technik-Chakra dafür haben.

Linux ist ein Betriebssystem - ähnlich wie auch Microsoft Windows, Apples MacOS 
oder Android für Smartphones.

**Fun-Fact:**\
*Android basiert im Kern ebenfalls auf Linux (so viel zu "du musst 
Technik-Expert\*in sein").*

## kleine Geschichtsstunde

Es war einmal die Geschichte von Linux, die du selbst auf Wikipedia nachlesen 
kannst.\
Hier das Wichtigste: Linux wurde von Studierenden entwickelt, weil sie ihre 
Computer daheim damals nicht mit einem Betriebssystem benutzen konnten. Denn 
das kostete ziemlich viel Geld und musste kompatibel zum Betriebssystem vieler 
Universitäten ("UNIX") sein. Sich also frei zu entscheiden 
ging nicht so richtig. Stattdessen: teure Software zum Kaufen - denn ohne geht 
gar nichts. (Klingt irgendwie vertraut, oder?)\
Ein Student namens Linus Torvalds hat dann ein eigenes Betriebssystem 
geschrieben. Das war kompatibel zu UNIX und er hat es kostenlos verteilt, denn 
alle sollten es benutzen oder es verbessern können, damit sie überhaupt 
irgendwas(!) an ihren Computern tun konnten.\
Und mit diesem neuen "Internet" ließen sich erstmals in der 
Menschheitsgeschichte ganze Bücher (oder Programm-Quellcode) mit einem 
Knopfdruck und ohne große Kosten um die Welt zu schicken.

## "freie Software"? Hä?

Durch diesen kooperativen Ansatz (statt "exklusiv" - wie in jmd. oder etwas 
"ausschließen") entstand mit der Zeit eine ganze (durchaus politische) Bewegung 
um sogenannte "freie Software". Die sollte Menschen grundsätzliche 
(Entscheidungs-)Freiheiten: 

- **Verwenden:** Freie Software darfst du für jeden Zweck nutzen: ohne 
  Einschränkungen wie Lizenzen oder anderen Beschränkungen (Hallo, 
  Geoblocking!).
- **Verstehen:** den Programm-Code darfst du dir ansehen und daraus lernen.
- **Verbreiten:** du darfst Freie Software kostenlos kopieren und weitergeben.
- **Verbessern:** Wenn du willst (und kannst), darfst du Freie Software 
  verändern, verbessern und dir anpassen, wie du magst. Und dann weitergeben. 

Klingt gut, oder!?

## Jaaa, ja… "bla bla bla… damals…!". Ich will das JETZT ausprobieren!

Okay, okay! Aber denk dran: es ist ist vollkommen anders, als ALLES was du 
kennst und total…\
Ja, ne - Quark! Es ist halt unterschiedlich, aber was in der Welt ist das 
nicht!?

Das meiste kennst du schon - z.B. *alle Programme, die wir dir hier bei 
Studytools vorstellen* - die gibts auch für Linux*. Es gibt genau so Fenster, 
die du groß- oder kleinmachen kannst, du kannst weiter deine Maus und Tastatur 
benutzen… und Katzenvideos gehen auch.

Was dir neu sein wird: Wahlfreiheit!\
Du kannst das Aussehen deines Desktops und deiner Fenster, der Symbole und 
allem selbst bestimmen. Ein Dark-Mode… (*der Autor lacht*) war vor 15 Jahren 
"neu". Du willst, dass dein Notebook beim Starten einen Pups-Ton macht? Klar, 
geht alles!

### die Qual der Wahl

Dein Betriebssystem wählst du aus unterschiedlichen "Distributionen" aus. Das 
ist ein Paket aus dem "Kern" des Betriebssystems ("Linux") und anderer freier 
Software: das generelle Aussehen und das Bedienkonzept von Desktop, Fenstern 
etc. (eine Desktop-Oberfläche bzw. Wndow-Manager), Video-Player, 
Textverarbeitung, Browser, Kalender…\
Von diesen Distributionen gibt es hunderte… für unterschiedliche Zwecke. Einige 
sind besonders einfach für Einsteiger\*innen, andere gut für 
Multimedia-Produktion, wissenschaftliche Berechnungen, Hacking oder bringen nur 
das allernötigste mit.

### Wie komme ich an Programm XY?

Jede Software bekommst du aus einer Art "Store" (nur das die Software nichts 
kostet und ebenfalls freie Software ist). Dazu lädst du "Pakete" herunter, die 
installiert werden. Installieren oder
deinstallieren kannst du das mit einem "Paketmanagement", die dir auch erlaubt 
50 Programme gleichzeitig zu (de)installieren.\

Nun hast du die Qual der Wahl - Linux ist, was du dir daraus machst!

Nutz' sie einfach - kostet ja nichts und du kannst es einfach ausprobieren, 
ohne etwas an deinen Daten kaputt zu machen.

**Hinweis**\
*Die meisten Distributionen kommen als sog. "Live-CDs" oder einfach 
"Live"-Systeme. Das bedeutet, du kannst sie von einem USB-Starten ohne sie 
wirklich auf deinem Gerät installieren zu müssen. Damit veränderst oder löschst 
du also erst einmal gar nichts, kannst aber alles so ausprobieren, als ob du 
Linux schon installiert hättest. Wenn du neustartest und den USB-Stick 
entfernst, ist alles wieder wie vorher.*

## Ich will… das… AUSPROBIEREN! JETZT!

Na gut, weil du so ungeduldig bist, hier ganz kurz das Wichtigste:

1. Schnapp' dir einen USB-Stick (mindestens 4GB Speicherplatz). **Achtung!** 
   *Alle Daten werden darauf gelöscht werden!*
2. Lade dir eine beliebige Distribution runter. Du bekommst ein "Abbild", dass 
   du auf dem USB-Stick installieren musst.
3. Um das "Abbild" auf dem USB-Stick zu installieren brauchst du ein spezielles 
   Programm, denn einfach kopieren geht nicht. Dein Rechner muss vom USB-Stick 
   Linux starten können. Ein Programm dafür ist z.B. 
   [UNetbootin](https://unetbootin.github.io/), dass für alle Betriebssysteme 
   existiert.
4. Nach der Installation von Linux auf dem USB-Stick musst du deinen Rechner 
   mit angestecktem USB-Stick starten. Dabei muss dein Rechner das Linux von 
   dem USB-Stick starten. Das musst du im BIOS / UEFI deines Rechners ggfs. 
   einstellen.
  - Versuche beim Start deines Rechners (sofort nach dem Anschalten) folgende 
    Tasten zu drücken: `F12`. `F1`, `F2`, `Esc` oder `entf` bzw. `del`.
  - Wenn alles klappt, kommt entweder
    1. ein Menü, in dem du die Start-Reihenfolge deiner Datenträger ändern 
       kannst. Hier stellst du deinen USB-Stick als erstes ein und startest 
       dann deinen Rechner neu.
    2. kommst du ins BIOS / UEFI deines Computers. Dort findest du meist ein 
       Menü namens `Boot`. Hier kannst du die Start-Reihenfolge deiner 
       Datenträger einstellen. Hier stellst du deinen USB-Stick als erstes ein 
       und startest dann deinen Rechner neu.

**Hinweis:**\
*Manchmal verhindert "Secure Boot" den Start von Linux, weil es von einem 
"nicht zertifizierten Gerät oder Hersteller" kommt. Du kannst die Einstellung 
"Secure Boot" ebenfalls im BIOS / UEFI deaktivieren. Das macht nichts kaputt, 
sondern deaktiviert eine Sicherheitsfunktion. In den Informationen deiner 
Linux-Distribution findest du ggfs. Informationen dazu, ob deine Distribution 
auch mit aktiviertem "Secure Boot" funktioniert.*

Wenn alles geklappt hat, sollte jetzt das Linux vom USB-Stick starten. 
Gratulation! 

Wenn nicht - toi, toi! Ließ nochmal die kurze Anleitung, schau bei den 
Informationen deiner Linux-Distribution zur Installation nach oder frag einen 
lieben Nerd in deiner Umgebung. 

**Tip:**\
*Nerds lieben Mate und brauchen manchmal neue Kapuzenpullover. ;)*

## Hilfe

- Webseite der "Linux Foundation"
  * <>
- Webseite "Distrowatch" mit vielen Neuigkeiten, Vorstellungen und einer Lise 
  der beliebtesten Linux-Distributionen (Sprache: EN)
  * <>
- 
