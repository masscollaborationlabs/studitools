# dein digitaler Campus

Moin und willkommen an der Uni Bremen!

An dieser Stelle wollen wir einen Rundblick machen, was für Dienste du von der
Universität zur Verfügung hast und wo du weitere Informationen zu deinem Studium
erhältst. Insbesondere gehen wir dabei auf alles an digitaler Infrastruktur ein.

Sobald du an der Universität Bremen eingeschrieben bist, bekommst du einen Account für
alle möglichen Dienste der Universität (und darüber hinaus).  
Dieser Account öffnet dir viele digitale "Türen" für dein Studium und Forschung:  
Portal für Veranstaltungen, Speicherplatz, Emails, Blogs, Prüfungs-Anmeldung und und und…

# das wichtigste in Kürze

Die Informationen zu deinem Uni-Account bekommst du per Post zugeschickt. 

Das wichtigste daran sind dein *Benutzer_innen-Name* und dein
*Passwort*. Dieser werden von fast allen Diensten der Universität benötigt.

## Stud.IP - deine Lernplattform, über die (fast) alles läuft

**Link:**
: [Stud.IP](elearning.uni-bremen.de/)

Alle Veranstaltungen (Vorlesungen, Seminare etc.) findest du über das
Studienportal [*Stud.IP*](https://elearning.uni-bremen.de/).  
Hier meldest du dich zu Veranstaltungen an und bekommst
alle digitalen Materialien der Veranstaltungen.  
Da *Stud.IP* der Dreh- und Angelpunkt deines Studiums ist, hast du hier auch die
Möglichkeit z.B.
[Studiengruppen](https://blogs.uni-bremen.de/studytools/2020/04/08/gruppenarbeit-studiengruppen-auf-stud-ip-nutzen/)
für dich und andere anzulegen,
[Lernräume](https://blogs.uni-bremen.de/studytools/2020/02/21/lernraeume-an-der-uni-bremen/)
an der Uni buchen, deine Dateien für Veranstaltungen zu speichern, [Video- und
Audio-Meetings](https://blogs.uni-bremen.de/studytools/2020/06/04/meetings-in-studiengruppen-mit-bigbluebutton/)
durchzuführen oder Literatur aus der Bibliothek in deinem eigenen Bereich zu
speichern. Außerdem hilft dir Stud.IP mit einem Kalender, den Überblick über
deine Veranstaltungen zu behalten.  

Stud.IP kann aber noch mehr: du kannst in Veranstaltungen (auch deinen eigenen
Studiengruppen) gemeinsam mit sogenannten *Etherpads* Texte schreiben oder
Informationen in einem eigenen *Wiki* (sowas wie bei Wikipedia) sammeln.

Über das integrierte Nachrichten-System, kannst du mit anderen Schreiben oder
Chatten. Das nutzen die Dozent_innen, um dir z.B. Informationen zu einer
Veranstaltung zu schreiben.
Für eine kleine Herausforderung zwischendurch kannst du dich beim Lernduell mit
anderen messen.

### Stud.IP verlinkt dich zum wichtigsten

Darüber hinaus findest du dort auch ein *schwarzes Brett* zum Kaufen, tauschen,
verschenken und mit Job-Angeboten -- und die wichtigsten Links: 

- zum [Webmail-Portal](https://webmail.uni-bremen.de/) des [Zentrum für Netze](#zentrumfuernetze)
- zum [PABO-Portal](http://www.uni-bremen.de/pabo) vom Prüfungsamt 
- zum [Mensa-Speiseplan](http://www.stw-bremen.de/de/essen-trinken/uni-mensa#plan)
- zum Cloud-Speicher [*Seafile*](https://blogs.uni-bremen.de/studytools/2020/04/01/datei-austausch-seafile-an-der-uni-bremen/) 
- zu den [Blogs](https://blogs.uni-bremen.de/studytools/2020/04/07/ublogs-einfach-bloggen-an-der-uni/)  

### Wenn du Fragen hast

Speziell wegen der Corona-Pandemie gibt die häufigsten
Fragen in einem [FAQ für die virtuelle Lehre vom Zentrum für Multimedia in der Lehre](https://www.uni-bremen.de/zmml/lehre-digital/faq.html) (ZMML; das sind
übrigens "wir";) ).

Wenn du Fragen hast oder Schwierigkeiten dich zurecht zu finden, sieh dir die
Hilfe zu Stud.IP an (oder die kurzen "Touren") -- und wenn du gar nicht mehr
klarkommst oder irgendwas nicht geht:  schreib uns eine [E-Mail an den
Support](mailto:info@elearning.uni-bremen.de).

## für alles mit Technik: das Zentrum für Netze (ZfN){#zentrumfuernetze}

**Link**:
: [Zentrum für Netze (ZfN)](https://www.uni-bremen.de/zfn)

Das Zentrum für Netze (ZfN) kümmert sich um die technische Infrastruktur der Uni
Bremen: das WLAN, die Server (Computer) auf denen die Dienste, wie z.B. die
Webseite der Uni Bremen oder die E-Mails.

### Onlinetools

Alle Einstellungen für deinen Account und die Funktionen, für die das ZfN
zuständig ist, kannst du mit den *Onlinetools* einstellen.

**Link:**
: [Onlinetools des ZfN](https://onlinetools.zfn.uni-bremen.de)

### WLAN

Um an der Uni überhaupt von deinem Laptop oder PC richtig arbeiten zu können,
musst du im WLAN der Universität sein. Das WLAN heißt *"Eduroam"* und ist an
vielen Universitäten Europas das gleiche (du kannst also auch in einer anderen
europäischen Uni mit dieser Verbindung einfach arbeiten).

Bei der ersten Einrichtung des WLANs hilft die die Anleitung des ZfN -- und wenn
du es nicht hinbekommst, hilft dir der WLAN-Support. Dort kannst du mit deinem
Gerät zur "Sprechstunde" kommen.

**Achtung!**  
Zugang zum WLAN hast du automatisch mit deinem Uni-Account -- allerdings gibt es
für das WLAN und alles andere zwei unterschiedliche Passwörter.

### E-Mail

Wichtigstes Kommunikationsmittel für alles (neben *Stud.IP*) ist dein
E-Mail-Account. Die Login-Daten sind die gleichen, wie die zu deinem
Uni-Account.

### Chat, Audio- und Video-Konferenzen

### Seafile

### andere Dienste

#### Software-Lizenzen

#### VPN

#### Mailinglisten

#### Dateicontainer

## Technik in der Lehre / in Veranstaltungen: Zentrum für Multimedia in der Lehre (ZMML)

mit anderen bekommst auch
Nachrichten von deinen Dozent_innen und Kommiliton_innen

Dein Uni-Account öffnet Türen

FAQ vom ZMML <https://www.uni-bremen.de/zmml/lehre-digital/faq.html>

für alles mit Technik: ZfN

das wichtigste: E-Mail & WLAN (Eduroam)

dein Portal für alles: Stud.IP

- Veranstaltungen
- Dateien
- Studiengruppen
- Etherpads
- Wikis

Veröffentlichen: Blogs der Uni

Administratives: 
- Zentrale Studienberatung <https://www.uni-bremen.de/zsb.html>
- Start Portal der Uni <https://www.uni-bremen.de/uni-start-portal/>
- MOIN-Portal und SfS
- ZPA und dezentrale Prüfungsämter
- Anleitungen und Handbücher für dein Studium: Formalia, Ablauf und so weiter in den Kompendien (z.B. Politikwissenschaft, IES, Soziologie)
- Schau auf den Seiten deines Instituts nach (Link zu Uni-Seite: "alle Insitutionen")

Speicherplatz: Seafile

Orte:

Recherche und Ansprechpartner: SUUB
Lageplan der Uni
Wie schreibe ich Arbeiten? Schreiblabore, Schreib-Portale und Beratungen (auch: Bremer Schreibcoach)
Lernräume an der Uni
Das Drumherum und du selbst: Beratungsstellen: PTB, Kivi, Familienfreundliches Studium
Weiterbilden: Studierwerkstatt

## zuständige Stellen

### ZfN - Zentrum für Netze

### ZMML - Zentrum für Multimedia in der Lehre

### SfS - Sekretariat für Studierende

### dein Fachbereich

Alles  Wissenswerte bzw. Notwendige findest du meistens bei deinem Fachbereich. 
Schaue also am besten dort nach offiziellen Ankündigungen, Regularien etc., wie 
z.B. nach deinem Studienverlaufsplan oder anderen Bedingungen nach.

Auf der Webseite der Uni werden alle Fachbereiche verlinkt:

- <https://www.uni-bremen.de/universitaet/organisation/alle-fachbereiche>

