# Untertitel automatisch erstellen in Kdenlive

Videos zu Untertiteln ist viel Arbeit - aber extrem hilfreich für alle, die 
(Fremd)Sprachen nicht verstehen oder barrierefreie Videos benötigen.\
Wie gut, dass es inzwischen Spracherkennung gibt!

**Hinweis:**\
*Spracherkennung ist extrem komplex und schwer. Daher passiert die 
Spracherkennung z.B. bei digitalen Assistenten großer Firmen meist online. Das 
aber bedeutet: deine (vielleicht sensiblen) Informationen schwirren durchs 
Internet - und landen bei irgendwelchen Firmen. Teils sind mit der 
"automatischen" Erkennung dann schlechtbezahlte Clickworker*innen beauftragt 
und hören sich deine belanglosen oder intimen Audioaufnahmen an.*

"Automatisch" bedeutet auch nicht "perfekt": Sprachanalyse ist so komplex, dass 
bei Fachwörtern auch die klügste Maschine überfragt ist. Du *musst* daher auf 
jeden Fall die Untertitel nacharbeiten!\
Für einfache und deutlich gesprochene Sätze funktioniert das aber ganz gut und 
kann dir eine Menge Arbeit ersparen.

**Tip:**\
*Zum manuellen Erstellen von Unteriteln stellen wir dir in einem anderen 
Artikel 
[Aegisub](https://blogs.uni-bremen.de/studytools/2020/11/06/untertitel-erstellen-mit-aegisub/) 
vor.*

In Kdenlive gibt es seit der Version 21.04 ist die Möglichkeit, automatische 
Untertitel deiner Tonspur(en) zu generieren. Wie?\
Eine ebenfalls freie Spracherkennungs-Software macht das möglich! Sie läuft 
außerdem auf deinem Rechner ("lokal") - also ohne, dass irgendwelche 
Audiodateien oder sensible Aussagen durchs Internet geschickt werden müssten. 
Die Audiodaten bleiben also bei dir und damit auch vertraulich.

Alles Nötige bringt Kdenlive bereits mit:\
unter `Projekt > Untertitel > Spracherkennung` findest du die Funktion, um die 
Spracherkennung nachzuinstallieren.

**Hinweis:**\
*Um eine bestimmte Sprache zu erkennen, musst du also das entsprechende 
Sprachpaket herunterladen, das Sprachanalyse vorher "trainiert" werden muss, 
eine Sprache zu erkennen.*

## Hilfe

- Beschreibung von Kdenlive zum Installieren der Spracherkennung (Sprache: EN)
  * <https://userbase.kde.org/Kdenlive/Manual/Timeline/Editing#Speech_to_text>
- Webseite zum Download der Sprachmodelle (Projekt "VOSK" von alphavephei)
  * <https://alphacephei.com/vosk/models>
