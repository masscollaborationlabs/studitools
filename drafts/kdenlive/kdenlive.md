# Kdenlive - dein Studio für Videoschnitt

- [OpenShot](https://blogs.uni-bremen.de/studytools/2020/08/29/openshot-filme-schneiden-leicht-gemacht/) 
  und 
  [Shotcut](https://blogs.uni-bremen.de/studytools/2020/08/30/shotcut-videos-schneiden-fuer-fortgeschrittene/) 
  sind irgendwie nichts für dich? 

Wie wäre es dann mit Kdenlive?\
Kdenlive ist ein Urgestein für Videoschnitt, dass dir so ziemlich alles bietet, 
was du von einem kompletten Profi-Schnittprogramm erwartest. Trotzdem ist es - 
wie 
[OpenShot](https://blogs.uni-bremen.de/studytools/2020/08/29/openshot-filme-schneiden-leicht-gemacht/) 
oder 
[Shotcut](https://blogs.uni-bremen.de/studytools/2020/08/30/shotcut-videos-schneiden-fuer-fortgeschrittene/) 
auch - freie Software (und daher nicht nur kostenlos, sondern auch ohne 
Einschränkungen nutzbar).

Kdenlive erfindet das Rad natürlich nicht neu und die Bedienung ist ähnlich 
anderer Schnittprogramme:\
Videospuren, Audiospuren findest du vermutlich genau so intuitiv, wie alle 
Mediendateien in deiner Bibliothek. Dabei lassen sich so ziemlich alle Formate 
einlesen und natürlich sind auch Bilder kein Problem.

Auf alles Material hast du eine *wirklich* große Auswahl an Filtern und 
Effekten, die oft auch Keyframe-fähig sind.\
Damit kannst du beispielsweise Bewegungen in deinen Videos tracken (z.b um die 
Gesichter von Passant*innen in deinem Beitrag verpixeln). Häufig benötigte 
Filter sind ebenfalls mit an Bord, beispielsweise für "Bauchbinden" oder 
Features wie Proxy-Bearbeitung deiner Clips (eine niedrig aufgelöste 
"Dummy"-Version deines Clips) .\
Und mit den umfangreichen Möglichkeiten Steuerung und Shortcuts anzupassen, 
machst du Kdenlive wirklich zu *deinem* Werkzeug. 

Auch Kdenlive bietet für den Export deiner Videos viele vorkonfigurierte 
Profile an - die du aber natürlich jederzeit weiter konfigurieren kannst. 

<https://kdenlive.org/de/>

## Hilfe

- Kdenlive Handbuch (Sprache: DE / Int.)
  * <https://userbase.kde.org/Kdenlive/Manual/de>
- Videotutorial "Videoschnitt mit Kdenlive für Anfänger - Workshop Kielux 2019 Aufzeichnung" (*Linux Guides DE*; YouTube)
  * <https://www.youtube.com/watch?v=XahaOIN8Q4g>
- Videotutorial "Wie ich Videos schneide! KDEnlive auf Linux | #Linux #KDEnlive" (*marcus-s*; YouTube)
  * <https://www.youtube.com/watch?v=7Pj5Zm3sq8g>
