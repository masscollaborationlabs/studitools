# Lernräume an der Uni Bremen

**Achtung!**\
***Aktuelle Informationen zu Nutzung der Lernräume (Buchung, Nutzungs- und 
Hygiene-Regelungen) findest du auf der Seite zu den Lernräumen der Uni Bremen:***

- <https://www.uni-bremen.de/universitaet/campus/lernraeume>

Abseits der Corona-Pandemie sind die Lernräume so gedacht:

An der Universität Bremen stehen dir Lernräume zur Verfügung, die du einfach 
über Stud.IP buchen kannst.
Jeder Lernraum besteht aus mehreren "Lerninseln". Diese sind zum Teil 
ausgestattet mit einem Flachbildschirm und vielen Anschlüssen für eure Geräte 
bei Gruppenarbeiten und Präsentationen (multimediale Lernräume). Es gibt auch 
Lernräume, die lediglich die Lerninseln ohne Multimedia-Ausstattung bieten.

Lernräume gibt es in den Gebäuden:

* GW1
* GW2
* MZH
* WiWi 1
* WiWi 2
* SUUB

Bei Stud.IP kannst du die Lerninseln über das Lernraum-Symbol in der Symbolleiste buchen.

<https://www.uni-bremen.de/lernraum>

**Hinweis:**\
_Solltest du mal keinen Raum finden aber mit anderen zusammenarbeiten müssen, 
stehen dir in Stud.IP weiterhin auch 
_[Studiengruppen](https://blogs.uni-bremen.de/studytools/2020/04/08/gruppenarbeit-studiengruppen-auf-stud-ip-nutzen/) zur Verfügung._
