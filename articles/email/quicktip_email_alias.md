# Quick-Tip: Email-Alias einrichten - ein Spitzname für deine Mailadresse

* Dein Uni-Account ist ganz einfach: `t_wcz7ikktwurpskv8sik4sub@uni-bremen`?
* Du willst dich einfach aus Gründen nicht mehr so nennen, wie deine 
  Uni-Mailadresse es tut? 
* Fünf Hilfskraft-Jobs + eine Mailadresse = Chaos pur!?

Da lässt sich was machen: lege dir einen "E-Mail Alias" an - quasi einen 
"Spitznamen" für deine blöde Mailadresse.

Alles Nötige findest du dafür bei den Onlinetools des ZfN:\
unter dem Punkt `Uni-Suche > E-Mail-Aliasnamen beantragen` anklicken und nach 
ein paar Klicks und einer Entscheidung (oder mehreren) hast du einen solchen 
Spitznamen. Alle Aliase werden allerdings vom ZfN manuell geprüft - es kann 
also eine Verzögerung geben, wenn du einen Alias erstellst.

**Achtung!**\
*Deine eigentliche Mailadresse der Uni bleibt gleich. Es ist wirklich nur ein 
Spitzname. Alle E-Mails an diese neue Adresse kommen auch in deinem bisherigen 
E-Mail-Postfach an.*

**Tip:**\
*Wenn du E-Mail-Aliase eingerichtet hast, kannst du z.B. in einem Mailprogramm 
wie 
[Thunderbird](https://blogs.uni-bremen.de/studytools/2021/01/16/thunderbird-ein-programm-sie-alle-zu-verwalten-e-mails-kalender-und-aufgaben/) 
oder auch bei 
[Webmail](https://blogs.uni-bremen.de/studytools/2021/01/16/zfn-webmail-e-mails-kalender-aufgaben-und-dateien-zu-deinem-uni-account/) 
Filter einrichten. Damit ordnest du eingehende Mails, je nachdem ob sie an die 
Alias-Adresse geschickt wurden oder an deine eigentliche Uni-Mailadresse.\
Außerdem kannst du dort "Identitäten" einrichten. Damit zeigen deine 
verschickten E-Mails den Alias als Absender*in. So schreibst und empfängst du 
E-Mails so, als hättest du eine andere Adresse.*

<https://onlinetools.zfn.uni-bremen.de/>
