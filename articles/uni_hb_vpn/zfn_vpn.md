# VPN - dein virtueller Zugang zum Uni-Netz

* Ohje, die SUUB hat zu - aber das Buch gibt es doch irgendwie digital!?
* Filme gucken über 
  [Kanopy](https://blogs.uni-bremen.de/studytools/2020/04/01/kanopy-filme-streamen-ueber-die-uni-bremen/) 
  will irgendwie gerade nicht?

Was du brauchst, ist der VPN-Zugang zum Uni-Netz!

Ein VPN ist ein "Virtual Private Network" - also ein virtuelles, privates 
Netzwerk. In diesem Fall ist es das Universitäts-Netzwerk.

An der Uni brauchst du einen VPN-Zugang, um z.B. auf digitale Bücher der SUUB 
zuzugreifen. Mit diesem Zugang surft du Webseiten so an, als wenn du direkt in 
der Uni sitzen würdest. Damit bist du automatisch als Angehörige\*r der 
Universität erkennbar für Seiten, die nur für Universitätsangehörige und 
Studierende zugänglich sind. Wissenschaftsverlage etwa bieten Uni-Bibliotheken 
einen besonderen Zugang, mit dem du kostenlos auf ihre Fachartikel zugreifen 
darfst.

Ein VPN-Zugang ist sinnvoll, wenn du über eine 
sichere Verbindung auf Daten im internen Netzwerk einer Organisation, Firma 
oder einem Heim-Netzwerk zugreifen willst. Das dient vor allem dem 
Datenschutz:\
Surfst du nämlich über das offene WLAN in einem Cafe, kann dein Datenverkehr 
leicht von anderen ausgelesen werden - vor allem, wenn du unverschlüsselte 
Seiten aufrufst (also mit `http://` statt dem sicheren `https://` in der 
Adresse).

**Hinweis:**\
*Es gibt viele VPN-Anbieter, die mit überzogenen Werbe-Versprechen locken: Sie 
wären ein anonymer und sicherer Zugang zum Internet, würden dich vor Hacking 
und Betrug schützen und so weiter.\
Das ist nicht nur übertrieben, sondern wiegt dich in falscher Sicherheit: mit 
einem VPN leitest du nämlich deinen gesamten Datenverkehr und Suchanfragen über 
den VPN-Anbieter! Der Anbieter kennt dich also sehr genau - und ob dieser so 
vertrauensvoll mit all deinen Daten umgeht, ist leider fraglich. Es gibt viele 
Fälle, in denen gerade solche "Schutz"-Mechanismen letztlich Daten ausspähen.*\

**Tip:**\
*Benutze am besten nur vertrauenswürde VPN-Anbieter (wie die Uni), die du nur 
kurzzeitig und für bestimmte Zwecke nutzt.*

Ob du für deine Zwecke wirklich allen Datenverkehr über den VPN-Zugang 
übertragen musst, erfährst du in den Anleitungen vom ZfN oder der SUUB.

**Hinweis:**\
*Oft wird bei VPN-Verbindungen auch von einem "Tunnel" gesprochen. Damit ist 
einfach die Verbindung gemeint - von einem Ende bis zu einem anderen - eben wie 
bei einem Tunnel, den dem nicht ersichtlich ist, was sich darin befindet.*

<https://www.uni-bremen.de/zfn/weitere-it-dienste/vpn>

## Hilfe

- Anleitungen des ZfN um das Uni-VPN einzurichten (alle Betriebssysteme):
  * <https://www.uni-bremen.de/zfn/weitere-it-dienste/vpn>
- Erklärung des ZfN zu den unterschiedlichen Tunnel-Arten des VPN:
  * <https://www.uni-bremen.de/zfn/weitere-it-dienste/vpn/tunnelarten>
- Anleitung der SUUB für den VPN-Zugang (auch für andere Hochschulen):
  * <https://www.suub.uni-bremen.de/infos/remote/>

* Warum VPNs oft nicht sinnvoll sind, erklärt u.a. der IT-Sicherheitsforscher 
  Mike Kuketz auf seinem Blog:
  + <https://www.kuketz-blog.de/empfehlungsecke-vpn-anbieter-sind-oftmals-nicht-noetig/>
  + <https://www.kuketz-blog.de/anonymitaet-die-haltlosen-werbeversprechen-der-vpn-anbieter/>
