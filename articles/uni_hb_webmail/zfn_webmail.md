# ZfN-Webmail - E-Mails, Kalender, Aufgaben und Dateien zu deinem Uni-Account 

Das wohl wichtigste Portal an der Uni neben dem Prüfungsamt und allem anderen 
zur Verwaltung ist: dein E-Mail-Konto.\
Mit deinem Uni-Account bekommst du auch ein E-Mail-Konto - aber noch mehr: 
Kalender, Aufgaben-Verwaltung und sogar ein Dateispeicher sind ebenfalls dabei.

Alle Funktionen findest du über das Webmail-Portal des ZfN. 

**Tipp:**\
*Viel komfortabler ist ein Mailprogramm wie [Thunderbird]() zu benutzen, um 
deine E-Mails, Kalender und Aufgaben zu verwalten. Thunderbird stellen wir dir 
auch hier auf Studytools vor.*

Auf dem Webmail-Portal kannst du aber natürlich auch eigentlich alles machen:\
E-Mails lesen und beantworten, Kalender anlegen, Termine eintragen und auch 
gleich die Kalender oder Termine mit anderen teilen. Mit E-Mail- und 
Kalender-Programmen lassen sich diese Kalender ebenfalls synchronisieren, so 
dass du sowohl in Webmail als auch auf deinem Smartphone oder Laptop immer auf 
dem aktuellen Stand bist.

**Hinweis:**\
*Weitere Funktionen zu deinem E-Mail-Account wie Aliase, Mailinglisten, 
Spam-Filter usw. findest du in den ZfN-Onlinetools (erst nach Anmeldung).*

<https://webmail.uni-bremen.de/>

## Hilfe

- Hilfe-Seiten des ZfN:
  * E-Mail (generell):
    + <https://www.uni-bremen.de/zfn/weitere-it-dienste/e-mail>
  * Webmail:
    + <https://www.uni-bremen.de/zfn/weitere-it-dienste/e-mail/webmailer>
  * E-Mail-Clients einrichten:
    + <https://www.uni-bremen.de/zfn/weitere-it-dienste/e-mail/e-mail-client-einrichten>
- ZfN-Onlinetools:
  * <https://onlinetools.zfn.uni-bremen.de>
- Handbuch zu Webmail (Horde 5) der Uni Frankfurt:
  * <https://webmail.server.uni-frankfurt.de/doku/pdf/main.pdf>
