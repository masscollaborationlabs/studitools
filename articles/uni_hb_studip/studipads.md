# StudIPads - gemeinsam Texten in Stud.IP

* Das Protokoll der Videokonferenz muss irgendwie gemeinsam erstellt werden?
* Deiner Studiengruppe immer den aktuellen Stand vom Text per E-Mail schicken 
  ist zu umständlich?

Zum Glück gibt es StudIPads!

StudIPads basieren auf der freien Software *Etherpad* (bzw. Etherpad Lite). 
Auch viele Organisationen und Vereine bieten solche Pads an, denn sie sind 
super praktisch um zusammen einen einfachen Text zu schreiben.

**Hinweis:**\
*Wenn ihr wirklich eher ein Office braucht, weil euer Text z.B. 
Inhaltsverzeichnisse, Abbildungen, Seitenzahlen oder Fußnoten braucht - schaut 
euch mal 
[OnlyOffice](https://blogs.uni-bremen.de/studytools/2020/07/16/online-bearbeitung-von-dokumenten-in-seafile/) 
an. Das ist eine Online-Office-Suite direkt in 
[Seafile](https://blogs.uni-bremen.de/studytools/2020/04/01/datei-austausch-seafile-an-der-uni-bremen/), 
der Campus-Cloud der Uni Bremen.*

In einem Pad können mehrere Personen gleichzeitig schreiben. Der Text wird je 
nach Person unterschiedlich eingefärbt und ständig gespeichert - so geht nichts 
verloren. Im Nachhinein kannst du dir sogar ansehen, wie der Text über die Zeit 
entstanden ist und einen Zustand als eine "Version" speichern. Das ist z.B. 
gut, wenn ihr euch auf eine endgültige Fassung des Texts geeinigt habt.

**Tip:**\
*Wenn eine Person keinen Stud.IP-Zugang hat: kein Problem!\
Ein StudIPad kann über einen Link öffentlich freigegeben werden. Soll eine 
Person das Pad nur ansehen dürfen, könnt ihr auch einen Schreibschutz 
aktivieren.*

Viel wichtiger ist aber: ihr könnt mit einfachen Formatvorlagen arbeiten und 
sogar Tabellen einfügen. In den Einstellungen eines Pads lassen sich dabei die 
Farben einstellen und auch ein Chat einblenden, mit dem ihr euch direkt beim 
Schreiben austauschen könnt. Außerdem gibt es ein automatisches 
Inhaltsverzeichnis, das alle Überschriften im Text automatisch ausliest. So 
behaltet ihr den Überblick bei längeren Dokumenten.

Wenn ihr fertig seit mit eurem Text, könnt ihr die Datei in unterschiedlichen 
Formaten exportieren - z.B. als HTML für eine Webseite, oder DOCX und 
ODT-Dateien für z.B. 
[LibreOffice](https://blogs.uni-bremen.de/studytools/2020/03/17/libreoffice-dein-office-fuer-alle/) 
oder andere Office-Programme.

**Hinweis:**\
*Die StudIPads sind ein Plug-In von Stud.IP-Veranstaltungen und Studiengruppen, 
das jeweils aktiviert werden muss. Wenn das Plug-In aktiviert ist, findest du 
es bei den Icons der Veranstaltung (der Notizblock mit "EP" und Stift). 
Ansonsten ist es im Reiter `StudIPad` einer Veranstaltung oder Studiengruppe zu 
finden.*

Ok, dann [bring mich zu Stud.IP!](https://elearning.uni-bremen.de/)

## Hilfe

- Hilfe-Seite zu StudIPads in der Stud.IP-Hilfe des ZMML:
  * <https://elearning.uni-bremen.de/>

- Projektseite der Software "Etherpad":
  * <https://etherpad.org/>
