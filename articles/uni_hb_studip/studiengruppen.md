# Gruppenarbeit? Studiengruppen auf Stud.IP nutzen!

Auf Stud.IP können alle Studierenden auch eigene Veranstaltungen anlegen - Studiengruppen! 

In Studiengruppen könnt ihr (fast) alle Tools und Möglichkeiten "normaler" Veranstaltungen nutzen: Teilnehmer\*innen einladen, Dateien hochladen, Wikis, Forum und Gruppenkalender, Stud.IPads, [Meetings](https://blogs.uni-bremen.de/studytools/2020/06/04/meetings-in-studiengruppen-mit-bigbluebutton/)…

Besonders praktisch ist das für Gruppenarbeiten:
ein gemeinsamer Kalender zeigt eure Termine an, im Wiki könnt ihr Quellen und Links sammeln und in den Stud.IPads (Etherpads) könnt ihr sogar gleichzeitig gemeinsam an einem Text schreiben. Stud.IPads lassen sich sogar über einen Link für Personen außerhalb der Universität freigeben.

**Neu im Sommersemester 2020:** [über das Modul "Meetings" könnt ihr mit _BigBlueButton_ Audiokonferenzen, Präsentationen, Bildschirm-Freigaben und ein Whiteboard nutzen -- auch in Studiengruppen.](https://blogs.uni-bremen.de/studytools/2020/06/04/meetings-in-studiengruppen-mit-bigbluebutton/)

**Studiengruppe auf Stud.IP anlegen:**

1. einloggen
2. auf `Veranstaltungen` gehen _oder_ im `Schnellzugriff` unter `meine Veranstaltungen`:
3. `Neue Studiengruppe anlegen` klicken (bei `Veranstaltungen` links unter `Aktionen`)
4. Name und Zugang für andere eingeben
5. Module auswählen
6. _Fertig_

_Alle aktivierten Module kannst du auch später noch verändern. Teilnehmende können wie in jeder Veranstaltung verschiedene Rechte besitzen (Gruppengründer_in, Moderator_in, Teilnehmer_in)._

## Hilfe

* Stud.IP Hilfe zu Studiengruppen:
  * <https://hilfe.studip.de/help/4.0/de/Basis/Studiengruppen>
* Hilfe-Blog des ZMML zu Stud.IP:
  * <https://blogs.uni-bremen.de/hilfestudipbremen/>
* PDF-Anleitung für das Erstellen von Studiengruppen:
  * <https://www.uni-bremen.de/fileadmin/user_upload/sites/zmml/Werkzeuge/Steckbriefe/DigitaleWerkzeuge_Studiengruppe.pdf>


