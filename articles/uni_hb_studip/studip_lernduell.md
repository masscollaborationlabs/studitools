# Lernduell in Stud.IP - fordert euer Wissen heraus!

* Für Prüfungen Büffeln ist immer so einsam?

Das Lernduell in Stud.IP ist ein Quiz, bei dem du gegen andere Personen der Uni 
Bremen antrittst. Die Fragen dafür kannst du entweder selbst erstellen oder auf 
die öffentlichen Fragenkataloge von anderen zugreifen.

Das klingt doch mal nach einem fairen Duell:\
Die Fragenkataloge kannst du in einen gemeinsamen Pool für alle zugänglich 
machen, damit z.B. auch folgende Semester den Lernstoff einer Vorlesung mit dem 
Lernduell wiederholen können.

**Hinweis:**\
*Falls du ein eigenständiges Programm suchst, mit dem du dir sogar interaktive 
Karteikarten zum Lernen anlegen kannst: schau dir mal 
[Anki](https://blogs.uni-bremen.de/studytools/2020/11/06/anki-lernen-mit-deinen-eigenen-digitalen-karteikarten/) 
an.*

Das Lernduell findest du in Stud.IP unter `Profil > Lernduell` (wenn es nicht 
dort ist, musst auf `mehr…` klicken und es dort freischalten).

[Okay, bring mich zu Stud.IP!](https://elearning.uni-bremen.de/)

## Hilfe

- Die offizielle Hilfe-Seite zum Lernduell in Stud.IP:
  * <https://blogs.uni-bremen.de/hilfestudipbremen/basis-lernduell/>
