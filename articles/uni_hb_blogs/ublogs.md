# Ublogs - einfach bloggen an der Uni

Ublogs ist die Blog-Plattform der Uni Bremen, die vom ZMML betreut wird. 
Dort kannst du beliebig viele Blogs anlegen - egal ob nur für dich sichtbar, für eure Projektpräsentation in einem Kurs oder eine Gruppenarbeit.

Mehr als die Login-Daten für deinen Uni-Account brauchst du nicht: 

Einfach einen Namen für dein Blog angeben und dein Wordpress-Blog ist mit zahlreichen Erweiterungen für all deine Ideen bereit. 
Durch die Vergabe von Accounts und Rollen könnt ihr auch Teams oder als Redaktion zusammenarbeiten.
Zahlreiche Themes und Baukasten-Systeme stehen zur Auswahl, um das Aussehen eines Blogs zu festzulegen.

_Ein neues Blog lässt sich erst nach der Anmeldung mit den Uni-Account-Daten anlegen. Die Blogs basieren auf der verbreiteten Open-Source-Software *Wordpress*._ 

<https://blogs.uni-bremen.de/>

\+ mehr lesen +

## Hilfe

- Informationen des ZMML zum Blog-System:
    - <https://www.uni-bremen.de/zmml/plattformen/blogs/>
* Kurzeinstieg, Grundlagen und Support:
    * <https://blogs.uni-bremen.de/support/>
* Einführung in das Blogsystem *UniBremenLogs* der Akademie für Weiterbildung:
    * <https://blogs.uni-bremen.de/files/2017/07/20160530_Anleitung_UBlogs.pdf>


