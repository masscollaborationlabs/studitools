# Große Dateien versenden - mit dem Temporären Dateicontainer vom ZfN

* Das Fotoprotokoll vom Workshop ist viel zu groß für deine E-Mail?
* Über 1GB Forschungsdaten an die Forschungsgruppe per USB-Stick als Päckchen 
  zu schicken ist… umständlich?

Klar, auch für solche Datenberge bietet dir das ZfN eine Lösung: den 
*Temporären Dateicontainer*!

Du findest den Dateicontainer in den Onlinetools vom ZfN unter der Überschrift 
`Kollaboration, Dateiaustausch & Blogs`.\
Dort kannst du eine Datei hochladen, die maximal 2GB groß sein darf. Zusätzlich 
wählst du noch aus, wie lang die Datei auf dem Server Zugänglich sein soll - 
denn nach diesem Zeitraum wird die Datei automatisch gelöscht.\
Der Upload so einer Datei dauert dann natürlich noch eine Weile…

Wenn alles geklappt hat, siehst du einen Link, den du anderen Personen schicken 
kannst.

**Hinweis**:\
*Mit 
[Seafile](https://blogs.uni-bremen.de/studytools/2020/04/01/datei-austausch-seafile-an-der-uni-bremen/) 
hast du die gleiche Funktionalität in schneller, schicker und komfortabler. 
Auch bei Seafile kannst du große Dateien hochladen und anschließend per Link 
für andere (auch nur für einen bestimmten Zeitraum) freigeben. Außerdem lässt 
sich der Zugriff per Passwort schützen. Das bietet der Temporäre Dateicontainer 
nicht!\
Der Vorteil vom Temporären Dateicontainer ist: du musst dich nicht mit Seafile 
auseinandersetzen, sondern kannst einfach mal eine größere Datei für andere 
freigeben. Und musst dich nicht mehr daran erinnern sie irgendwann zu löschen…* 

<https://onlinetools.zfn.uni-bremen.de>
