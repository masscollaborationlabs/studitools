# Materialien der Studierwerkstatt zum wissenschaftlichen Arbeiten

Die Studierwerkstatt der Uni Bremen bietet dir eine umfangreiche Sammlung mit 
Materialien und nützlichen Tipps: von Motivation, Präsentations-Tipps und
Schreibhilfen bis zu Methoden zum Lernen und Coachen von anderen.
Darin erfährst du z.B. welche Methoden dir beim Argumentieren und Strukturieren 
deiner Texte helfen, wie du Plakate zu Präsentation erstellt oder dein Wissen 
z.B. als Coach anderen vermitteln kannst.

Insgesamt findest du dort knapp 180 kurze Leitfäden zu den Kategorien 

- Motivation im Studium
- Prüfungen vorbereiten
- Präsentieren und vortragen
- Poster entwerfen
- Coaching
- Schreiben
- Rechtschreibung & Zeichensetzung
- Interaktive Methoden für Lehreinheiten, Tutorien, Workshops und Seminare
- Forschendes Studieren

Alle Arbeitsblätter und Anleitungen gibt es auf der Seite der Studierwerkstatt 
als PDF zum Herunterladen. Und wenn du lieber Videos dazu schauen würdest: die 
gibt es auch! Entweder direkt über Links in den Rubriken oder auf dem 
YouTube-Kanal *"Die Sendung mit der Studierwerkstatt"*.

**Hinweis:**\
*Teilweise stehen die Materialien unter einer [Creative Commons 
Lizenz](https://creativecommons.org/). Die Inhalte darfst du daher auch 
vervielfältigen und weitergeben oder bearbeiten (solange du dabei die 
Lizenzbedingungen befolgst.*

<https://www.uni-bremen.de/studierwerkstatt/online-hilfen>

- *"Die Sendung mit der Studierwerkstatt"* auf YouTube:
  - <https://www.youtube.com/channel/UCKx0XsKikUwZqSWCKhBeC0A>

