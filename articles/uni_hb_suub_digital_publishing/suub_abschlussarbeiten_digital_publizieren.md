# Abschlussarbeit publizieren - Open Access über die SUUB, nicht "für die Tonne"!

* Tagelang gegrübelt, wochenlang formuliert, Monate geschrieben - für immer 
  "für die Tonne"?

Quatsch!\
Du und deine Arbeit habt Stolz und Schulterklopfer verdient!

Über die SUUB kannst du deine Abschlussarbeiten (Bachelor-, Master- und 
Examensarbeiten) elektronisch publizieren lassen - aber **nur mit einer 
Empfehlung deiner / deines Betreuenden**.

Mit ein paar Schritten unterschreibst du einen Vertrag mit deinem 
Einverständnis und kannst die Arbeit dann einreichen. Dazu muss sie allerdings 
noch in einem speziellen PDF-Format gespeichert werden. Wie das geht kannst du 
in der SUUB erfahren.

**Hinweis:**\
*Für die Erstellung der PDF-Datei gibt es Anleitungen auf der Seite der SUUB. 
PDF/A-Dateien kannst du auch direkt mit 
[LibreOffice](https://blogs.uni-bremen.de/studytools/2020/03/17/libreoffice-dein-office-fuer-alle/) 
speichern.*

Nachdem deine Abschlussarbeit auf dem Dokumentenserver der Uni liegt, ist sie 
sofort weltweit auffindbar - und bekommt sogar einen DOI (Digital Objekt 
Identifier), mit dem andere die Arbeit z.B. direkt in einer Literaturverwaltung 
wie 
[Zotero](https://blogs.uni-bremen.de/studytools/2020/03/17/zotero-literaturverwaltung-fuer-alle/) 
oder 
[Jabref](https://blogs.uni-bremen.de/studytools/2020/07/16/jabref-literaturverwaltung-fuer-latex/) 
suchen und als Quelle speichern können. 

**Hinweis:**\
*Falls du überlegst z.B. deine Promotionsarbeit zu publizieren, kannst du sie 
hier auch als Open-Access veröffentlichen. Damit ist deine Arbeit weltweit 
einfach anderen Forschenden zugänglich - unter einer 
[Creative-Commons-Lizenz](https://creativecommons.org/) und ohne große Kosten 
und Aufwand.\
Mit einem Verlag solltest du so eine vorherige Veröffentlichung allerdings 
absprechen.*

Auch in deiner Bachelor-Arbeit kann eine Menge guter Arbeit und toller 
Erkenntnisse stecken, die etwas bewegen können, die du als Referenz anführen 
kannst und die ein positives Beispiel für andere sein kann.

Also husch, husch - diese "Wissensgesellschaft", von der alle reden, wartet 
schon auf deine klugen Gedanken!

<https://media.suub.uni-bremen.de/>

## Hilfe

- Infoseite der SUUB zu "Open-Access":
  * <https://www.suub.uni-bremen.de/literatur-verwalten/open-access-in-bremen/informationen-zum-thema-open-access/>
- Portal zu "Open-Access" der Uni Bremen:
  - <https://www.uni-bremen.de/forschung/forschungsprofil/open-access-an-der-universitaet-bremen/>
- FAQ der Deutschen Forschungsgemeinschaft (DFG) zu "Open-Access":
  * <https://www.dfg.de/foerderung/faq/open_access_faq/>
- Portal "Open Access" von open-acess.net
  - <https://open-access.net/startseite>

