# Schreibportal für die Geistes-, Kultur- und Sprachwissenschaften (FB 09 / 10)

An der Universität Bremen gibt es viele Angebote, die dich beim Schreiben von 
wissenschaftlichen Arbeiten unterstützen können: Nachschlagewerke wie der 
["Bremer 
Schreibcoach"](https://blogs.uni-bremen.de/studytools/2020/02/21/der-bremer-schreibcoach-ratgeber-fuer-wissenschaftliches-schreiben-im-studium-und-darueber-hinaus/) 
oder die Angebote und [Materialien der 
Studierwerkstatt](https://blogs.uni-bremen.de/studytools/2020/03/12/materialien-der-studierwerkstatt-uni-bremen-zum-wissenschafltichen-arbeiten/), 
[Lernvideos zum wissenschaftlichen 
Arbeiten](https://blogs.uni-bremen.de/studytools/2020/02/21/lernvideos-des-zmml/) 
und [Uni-übergreifende 
Selbstlernkurse](https://blogs.uni-bremen.de/studytools/2020/03/12/online-selbstlernkurs-der-uni-halle-zum-wissenschaftlichen-arbeiten/) 
oder Programme wie eine 
[Literaturverwaltung](https://blogs.uni-bremen.de/studytools/2020/03/17/zotero-literaturverwaltung-fuer-alle/)…

Für die Fachbereiche 09 (Kulturwissenschaften) und 10 (Sprach- und 
Literaturwissenschaften) gibt es außerdem noch einen hilfreichen Blog: das 
Schreibportal der Geistes-, Kultur- und Sprachwissenschaften.

**Hinweis:**\
**Ergänzend zum Blog gab es Beratungsangebote u.a. durch studentische 
Schreibcoaches. Die Beratungsangebote des FB 09 / 10 wurden jedoch im April 2021 
eingestellt.**

Auf dem Blog findest du allerdings nicht nur Tipps, wenn du in den 
Geisteswissenschaften bist -- auch für alle anderen Fachbereiche gibt es hier 
gute Hinweise und Informationen. 

<https://blogs.uni-bremen.de/schreibportalfb910/>

## andere Angebote

Von der Studierwerkstatt gibt es die [studentischen Lern- und 
Prüfungscoaches](https://www.uni-bremen.de/studierwerkstatt/lerncoaching), die 
dir bei in solchen Fällen ebenfalls Hilfestellung geben können. 

### Voll gut, aber ich promoviere (vielleicht)!?

Klar, auch für Marathon-Schreibende wie dich gibt es Unterstützung!\
Schau doch mal nach Workshops und Angeboten von 
[BYRD](https://www.uni-bremen.de/byrd) oder [perspektive 
promotion](https://www.uni-bremen.de/chancengleichheit/training-und-beratung/perspektive-promotion). 
Diese Angebote richten sich speziell an Promovierende (perspektive promotion 
speziell an Promovendinnen) und bieten z.B. Workshops zum Formatieren deiner 
Arbeit oder regelmäßige Schreibgruppen an. 

