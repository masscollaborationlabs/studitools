# Sicherungskopien bei Seafile: "Versionen" von Sammlungen aktivieren

* Du schreibst deine superwichtige Arbeit und willst keinen Zwischenschritt 
  davon verlieren?

Dann solltest du 
[Seafile](https://blogs.uni-bremen.de/studytools/2020/04/01/datei-austausch-seafile-an-der-uni-bremen/) 
benutzen, um regelmäßig automatisch eine Sicherungskopie anzulegen!\
Was dir dann für "jeden Zwischenschritt" noch fehlt: Versionen.

Bei deiner Sammlung in Seafile einfach auf den Pfeil nach unten klicken, 
`Versionen einrichten` auswählen und schon kannst du wählen, ob und wie viele 
Versionen einer Datei Seafile behalten soll und wie lange. 

**Hinweis:**\
*Versionen kannst du derzeit nur im Web-Client von Seafile aktivieren - und nur 
bei Sammlungen (keine einzelnen Ordner oder Dateien).*
