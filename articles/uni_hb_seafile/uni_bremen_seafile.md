# Campus-Cloud: Seafile an der Uni Bremen 

- Eine Sicherungskopie deiner Abschluss-Arbeit speichern?
- Deine viel zu große Präsentation mit den Bildern deinen Freunden an dieser 
  anderen Uni zum Korrigieren schicken?
- Deine 2 GB bei diesem anderen Dings sind voll?

Zu deinem Account an der Uni Bremen gehören dir dank dem [Zentrum für Netze 
(ZfN)](https://www.uni-bremen.de/zfn/) auch 100 GB Cloud-Speicherplatz.  
Über 
[Seafile](https://www.uni-bremen.de/zfn/weitere-it-dienste/storage-dateiaustausch/) 
kannst du damit Dateien und Ordner in verschiedenen "Bibliotheken" 
organisieren, Dateien speichern und auch für andere freigeben.

Auf die Daten kannst du entweder über die Webseite beim ZfN zugreifen oder aber 
einen Ordner auf deinen Geräten erstellen und das Programm (Client) benutzen. 
Seafile kümmert sich dann darum, die Dateien ständig zu synchronisieren.

**Achtung!**\
*Damit du anderen in Seafile einen Ordner freigeben kannst, müsst ihr euch im 
"zentralen Adressbuch" freischalten. Das findet ihr nach dem Login bei der 
Seafile-Webseite des ZfN in euren Account-Einstellungen. Anschließend könnt ihr 
bei einer Freigabe andere Personen mit ihren Uni-Account-Daten bzw. der E-Mail 
finden.*

- Für die Konfiguration des Seafile-Clients brauchst du folgende Angaben:
  - Servername: <https://seafile.zfn.uni-bremen.de>
  - Benutzername: dein Uni-Account *mit* `\@uni-bremen.de` (kein Alias!)
  - Passwort: \*\*\*\*\*\*\*\*\*\*\*\*\*

## Hilfe

- Offizielles Hilfe-Portal zu Seafile:
  * <https://de.seafile.com/support/benutzer/>
