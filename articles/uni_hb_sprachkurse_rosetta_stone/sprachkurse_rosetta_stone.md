# Sprachkurse mit "Rosetta Stone"

Dein russisch ist schon gut, aber noch nicht *so* gut wie dein Koreanisch!?
Der Weg zu deiner Sprachschule ist *sooooo* lang?

Dein Glück: die Universität hat eine Lizenz für das *Rosetta Stone*-Sprachenportal!
Dort kannst du Übungen für viele Sprachen nutzen und mit *"Rosetta Stone Advantage"* sogar deine bisherigen Sprachkenntnisse gezielt verbessern. 

Mehr Informationen und über das Angebot gibt es bei der [SuUB (Rosetta Stone)](https://elib.suub.uni-bremen.de/ebooks/rosetta.html).

Не сто́ит благодарности!
