# Wilmas Tutorials

Du hast Fragen zu Lern- und Arbeitstechniken an deiner Hochschule? Frag mal Wilma!
Die ausgezeichneten Videos erklären dir nicht nur, wie du Texte schreibst oder online recherchierst, sondern auch, wie eine Hausarbeit aufgebaut ist oder welche wissenschaftlichen Forschungsmethoden es gibt.

In den kurzen Videos erklären dir Studierende der Hochschule Fulda die Computerprogramme, denen sie im Studium immer wieder begegnen.

* "Wilmas Tutorials" gibt es auf YouTube:
  * <https://www.youtube.com/channel/UCMBLOJi-LCy1R-sVjLKTe6g/feed>

Die Tutorials sind Teil des Projektes _"Let's Learn -- Screencasts zu Studien-, Lern- und Arbeitstechniken von Studierenden für Studierende"_ im Fachbereich Sozialwesen an der Hochschule Fulda.

Noch mehr Informationen zu Werkzeugen für dein Studium findest du in [Wilmas Werkstatt](https://wilma.hypotheses.org/).
