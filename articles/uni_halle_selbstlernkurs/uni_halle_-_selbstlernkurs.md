# Online-Selbstlernkurs der Universität Halle zum wissenschaftlichen Arbeiten

Wie denkst du eigentlich "wissenschaftlich" und was sind eigentlich wissenschaftliche Methoden?
Darfst du dieses Bild in deiner Präsentation benutzen?
Deine Stimme wird ganz zittrig, wenn du an eure Präsentation nächste Woche denkst?

Das *Zentrum für multimediales Lehren und Lernen* (\@LLZ) der Universität Halle bietet dir einen offenen Selbstlernkurs mit Lernmaterialien, Denkaufgaben und Übungen rund ums wissenschaftliche Denken und Arbeiten. Über die Lernplattform ILIAS, an die auch die Universität Bremen angeschlossen ist, kannst du den offenen Selbstlernkurs aufrufen. Er enthält verschiedene Module, je nach dem, was du gerade brauchst:

- <https://ilias.uni-halle.de/goto.php?target=cat_61583&client_id=unihalle>

*Der gesamte Kurs steht unter der Lizenz [[CC BY-SA 4.0 DE]](https://creativecommons.org/licenses/by-sa/4.0/deed.de). Du kannst also die Materialien und Texte kopieren, weitergeben und bearbeiten werden, solange du den Namen angibst und alles wieder unter der gleichen Bedingung weitergibst.*

\+ mehr lesen +

Zu den Inhalten [[schreibt das \@LLZ der Universität Halle]](https://blog.llz.uni-halle.de/2018/08/offener-selbstlernkurs-fuer-studierende-grundlagen-des-wissenschaftlichen-arbeitens/):

> Die Fähigkeit, wissenschaftlich zu arbeiten bzw. eine wissenschaftliche Arbeit zu erstellen, gehört zu den Schlüsselqualifikationen für ein erfolgreiches Studium. Häufig werden die damit verbundenen Kenntnisse und Fertigkeiten jedoch wenig oder nur unzureichend, beziehungsweise am Rand gelehrt, geübt und durch Wiederholung gefestigt.
>
> \[...\]
>
> > Der [[Selbstlernkurs "Grundlagen des wissenschaftlichen Arbeitens im Studium"]](https://ilias.uni-halle.de/goto.php?target=cat_61583&client_id=unihalle) richtet sich vornehmlich an StudienanfängerInnen und Studierende, die ihr Wissen zur Thematik vertiefen beziehungsweise auffrischen wollen oder eine wissenschaftliche Arbeit verfassen möchten. Er kann nach Bedarf sowohl zum individuellen als auch zum kooperativen Lernen genutzt werden. Auch Lehrende der MLU sowie der Hochschulen im Verbund HET LSA sind herzlich dazu eingeladen, den Online-Kurs für sich und ihre Lehrveranstaltungen vollständig oder auszugsweise zu verwenden und gegebenenfalls für sich zu ergänzen.
