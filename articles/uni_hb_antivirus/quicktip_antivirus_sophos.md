# Quick-Tip: Sophos - Antivirus-Programm über die Uni

* Wusstest du, dass du ein Antivirus-Programm über die Uni kostenlos beziehen 
  kannst? 

*Sophos Anti-Virus* und *Sophos Client Firewall* stehen allen Mitarbeiter*innen 
und Studierenden der Uni Bremen für ihre Arbeits- und Privatrechner zur 
Verfügung. Sophos ist Plattformübergreifend für alle Betriebssysteme nutzbar.

Weitere Infos findest du auf der Anti-Virus-Seite der Universität Bremen.

<https://antivirus.uni-bremen.de/>

**Hinweis:**
*Für Fragen und Support wende dich bitte an die angegebene Support-E-Mail auf 
der Seite.*
