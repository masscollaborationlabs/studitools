# Mailinglisten für Gruppen und Projekte

Für ein größeres Projekt oder eure Initiative braucht ihr einen E-Mail Verteiler?

Dann benutzt eine Mailingliste!

Mailinglisten sind (oder auch "Mailverteiler") sind immer dann gut, wenn du regelmäßig denselben Menschen E-Mails schreibst und dabei eigentlich immer alle erreichen willst. Auch alle anderen können ganz einfach wieder an alle (zurück)schreiben.

Der Vorteil ist: alle müssen sich nur eine einzige E-Mail-Adresse merken. Die Mailingliste schickt deine E-Mail dann weiter an alle Personen, die auf der Liste stehen.

Neben dieser Grundfunktion gibt es dabei noch einige Extras: 

- jemand kann die Beiträge Moderieren, falls doch nicht alle Menschen auf der Liste einfach schreiben können sollen
- du kannst jeweils zusammenfassend alle E-Mails einmal pro Woche oder Monat bekommen 
- es kann ein Archiv aller bisherigen Nachrichten geben, so dass neue Personen sich "einlesen" können 
- usw.

Mailinglisten gibt es auf der ZfN-Seite unter "E-Mail" oder in den ZfN-*Onlinetools*. Nachdem du dort einen Wunschnamen eingegeben hast, bekommst in wenigen Minuten eine Bestätigung. Danach trägst du die benötigten E-Mail-Adressen dort ein und kannst anschließend deine erste Rundmail verfassen.

## Hilfe

- ZfN-Webseite zu E-Mails:
  - <https://www.uni-bremen.de/zfn/weitere-it-dienste/e-mail>
- Login zu den ZfN-Onlinetools
  - <https://onlinetools.zfn.uni-bremen.de>
- kurze Anleitung (PDF) der Universität Hildesheim zu Mailinglisten mit der Software "Mailman":
  - <https://www.uni-hildesheim.de/rz/mailman/anleitungMailmanowner.pdf>

