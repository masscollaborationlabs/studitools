# Lernvideos des ZMML zum wissenschaftlichen Arbeiten

Der Fachbereich 7 (Wirtschaftswissenschaften) und das ZMML haben im Rahmen des [ForstA-Projekts an der Universität Bremen](https://www.uni-bremen.de/forsta.html) 6 Videos zum wissenschaftlichen Arbeiten für dich erstellt. Sie erklären dir kurz, wie du an der Universität wissenschaftlich arbeitest und was das eigentlich konkret bedeutet.

Alle Videos findest du in der [Einführung in das Wissenschaftliche Arbeiten](https://ml.zmml.uni-bremen.de/series/5aa2a5fed42f1c730a8b4569) von Dr. Christiane Bottke.

Und falls du ein bestimmtes Kapitel suchst:

- [Kapitel 1: Besonderheiten wissenschaftlichen Arbeitens](https://ml.zmml.uni-bremen.de/video/5be2d726d42f1c6c6f8b456d?track_id=5c1cf4a3d42f1ce7138b4567)
- [Kapitel 2: Der wissenschaftliche Arbeitsprozess und Projekt- und Zeitplanung](https://ml.zmml.uni-bremen.de/video/5be2dc48d42f1c16748b4567?track_id=5bf5671fd42f1ca1448b4567)
- [Kapitel 3: Recherche & Materialauswertung](https://ml.zmml.uni-bremen.de/video/5be2d8f1d42f1c7e738b4567?track_id=5c1cf4f4d42f1c5d148b4567)
- [Kapitel 4: Entwurf, Gliederung & Erstellung der Arbeit](https://ml.zmml.uni-bremen.de/video/5be30e02d42f1c16748b456c)
- [Kapitel 5: Quellenangaben und Verzeichnisse erstellen](https://ml.zmml.uni-bremen.de/video/5be32168d42f1c6b778b456a)
- [Kapitel 6: Korrektur, Druck & Veröffentlichung](https://ml.zmml.uni-bremen.de/video/5beeea45d42f1c781f8b4568)
