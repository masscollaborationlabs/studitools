# OnlyOffice - gemeinsam Office-Dokumente in Seafile bearbeiten

* Gemeinsam an Office-Dokumenten schreiben oder eine Tabelle erstellen? Klar! 

In der [Campus-Cloud 
*Seafile*](https://blogs.uni-bremen.de/studytools/2020/04/01/datei-austausch-seafile-an-der-uni-bremen/) 
ist *OnlyOffice* integriert!

Was du dafür tun musst? Einfach auf die Seafile-Seite vom ZfN gehen und dich 
anmelden.\
Wenn du in deinen Dateien dort auf eine Microsoft-Office-Datei in den Formaten 
DOCX, XLSX oder PPTX klickst, öffnet sich OnlyOffice im Browser - und los 
geht's. Perfekt, um Dokumenten gemeinsam den letzten Schliff zu verpassen.

Für Absprachen gibt es neben der Kommentar-Funktion auch einen Chat. Ansonsten 
bietet OnlyOffice alles, was du von deinem Office gewohnt bist: Formatvorlagen, 
Tabellen, Grafiken, Änderungs-Verfolgung… oder eben eine vollwertige 
Tabellenkalkulation und Präsentationen.
Die Dateien kannst du anschließend auch wieder als Microsoft-Office-, 
LibreOffice-Datei oder PDF herunterladen.

**Achtung!**\
*Erstelle am besten immer eine Sicherheitskopie oder benutze das Feature 
[Versionen in deiner 
Seafile-Bibliothek](https://blogs.uni-bremen.de/studytools/2021/02/17/sicherungskopien-bei-seafile-versionen-von-sammlungen-aktivieren/). 
Damit geht dir keine Datei verloren oder Kaputt.*

**Anmerkung:**\
*Bei Dokumenten kann es leider immer passieren, dass die Formatierungen 
fehlerhaft sind: 
z.B. verrutschen Bilder oder die Schrift erscheint unterschiedlich. Das kann an 
unterschiedlich aktuellen Office-Versionen liegen oder auch unterschiedlicher 
Software (z.B. 
[LibreOffice](https://blogs.uni-bremen.de/studytools/2020/03/17/libreoffice-dein-office-fuer-alle/) 
und Office für Mac). **Einigt euch daher möglichst auf einen gemeinsamen 
Standard, den ihr alle nutzen könnt - am besten eine freie Software, die unter 
allen Betriebssystemen funktioniert.** Außerdem ersparen euch [Formatvorlagen 
in den 
Office-Programmen](https://blogs.uni-bremen.de/studytools/2021/03/05/formatvorlagen-in-office-programmen-office-bei-fussnote-such-die-ueberschrift/) 
die Arbeit, das Aussehen ständig neu einstellen zu müssen.*

**Tip:**\
* Ihr könnt auch direkt in Stud.IP gemeinsam schreiben: mit 
  [StudIPads](https://blogs.uni-bremen.de/studytools/2021/04/09/studipads-gemeinsam-texten-in-stud-ip/). 
  Das Plug-In kann für alle Veranstaltungen oder Studiengruppen in Stud.IP 
  aktiviert werden. Ein solches Pad kann ebenfalls für Personen ohne 
  Stud.IP-Zugang freigegeben werden.*

<https://seafile.zfn.uni-bremen.de/>

## Hilfe

- Seafile beim ZfN:
  - <https://seafile.zfn.uni-bremen.de/>
- Informationen zu Seafile des ZfN:
  * <https://www.uni-bremen.de/zfn/weitere-it-dienste/storage-dateiaustausch>
- Hilfe zu OnlyOffice:
  * <https://helpcenter.onlyoffice.com/index.aspx>
- Videos zu OnlyOffice:
  * <https://helpcenter.onlyoffice.com/video.aspx>
