![](https://seafile.zfn.uni-bremen.de/lib/94902d04-3549-47b6-855e-fe0b0518f8f6/file%2FBlog%20f%C3%BCrs%20Wissenschaftliche%20Arbeiten%2FBilder%2Fselbst%20erstellte%2Fkanopy.png?raw=1)

Kanopy ist ein Videostreaming-Portal über das viele (Independent-)Filme und 
hochwertige Dokumentationen zu finden sind. Das „Netflix der Uni Bremen" bietet 
dir einen Zugriff auf über 10.000 Videos zum Streamen an. Dabei gibt es 
Sammlungen zu bestimmten Themen oder Formaten (Dokus, Kurse, Spielfilme…).

**Hinweis:**\
*Für den Zugriff brauchst du deinen Uni-Account und musst die VPN-Verbindung 
(mit der Option "Tunnel-All-Traffic") ins Uni-Netz eingerichtet haben!*

* <https://unibremen.kanopy.com/>

Wie eine VPN-Verbindung für die Universität Bremen eingerichtet werden kann und 
was dabei zu beachten ist, erklärt sowohl das Zentrum für Netze (ZfN) als auch 
die Bibliothek (SUUB):

* <https://www.uni-bremen.de/zfn/weitere-it-dienste/vpn.html>
* <https://www.suub.uni-bremen.de/infos/remote/>
* Die Option "Tunnel-All-Traffic" ist dabei wichtig, weil Kanopy ein *externer* 
  Dienst ist. Den Unterschied erklärt das ZfN:

> Ich habe eine VPN-Verbindung erfolgreich aufgebaut, kann nun aber die Dienste
> der Bibliothek nicht nutzen?
> 
> Wenn man Dienste nutzen möchte, die für den Netzbereich der Universität 
> Bremen freigeschaltet sind, aber nicht im Netzbereich der Uni "gehostet" 
> werden (Verlage sind hier ein Beispiel), dann muß man beim Verbindungsaufbau 
> die Gruppe von "Tunnel-Uni-Bremen" auf "Tunnel-all-Traffic" umstellen. Für 
> eine detaillierte Erklärung lesen Sie bitte die Seite 
> [VPN-Tunnelarten](https://www.uni-bremen.de/zfn/weitere-it-dienste/vpn/tunnelarten.html).
>
> ACHTUNG: Durch diese Option wird der gesamte Datenverkehr nun durch den 
> Tunnel geleitet. Bitte den Tunnel mit dieser Option nur aktiviert lassen, 
> solange dies für das Arbeiten benötigt wird.
