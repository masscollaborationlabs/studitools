# Der Bremer Schreibcoach – Ratgeber für wissenschaftliches Schreiben im Studium und darüber hinaus

Nichts löschen, obwohl du deinen Text korrigieren sollst? 
Mitten im Schreiben aufhören?
Mal so richtig an den Aussagen von diesem einen Text rumnörgeln? 
25% deiner Schreibarbeit soll die Korrektur sein?

**Bitte?!**

Warum du das machen solltest, erklärt dir der Bremer Schreibcoach von Prof. Dr. Krings. Zu allen Phasen deines Schreibprojektes findest du dort mehr als 300 konkrete Ratschläge:

* Thema suchen
* Literatur recherchieren
* Material sammeln
* strukturieren
* formulieren
* revidieren
* Korrektur lesen
* gestalten

[Der Bremer Schreibcoach (PDF)](http://www.fb10.uni-bremen.de/homepages/krings/Der_Bremer_Schreibcoach.pdf)
