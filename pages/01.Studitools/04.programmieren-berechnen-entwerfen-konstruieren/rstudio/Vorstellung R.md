---
title: 'Statistik und Daten-Analyse mit R und Rstudio'
date: '28-05-2020'
publish_date: '28-05-2020'
taxonomy:
    category:
        - Programmieren
        - Berechnen
        - Entwerfen
        - Konstruieren
---

![](img/rstudio_rlogo.png)

===

# Statistik und Daten-Analyse mit R und Rstudio

Du brauchst ein Statistik-Programm, mit dem du auch zu Hause arbeiten kannst?  
Zum Glück gibt es "R": (d)eine Open-Source-Programmiersprache für Datenanalyse und Datenvisualisierung! 

R ist (neben der Programmiersprache *Python*) derzeit eines der beliebtesten Tools für alle Arten der Datenaufbereitung, -analyse und -visualisierung - und R wird ständig erweitert und verbessert. 
Mit "R-Studio" gibt es auch eine sogenannte "Entwicklungsumgebung", die mit einer grafischen Oberfläche mehr Übersicht als das normale R bietet (R hat sonst nur ein Eingabefenster für Code, was nicht sehr intuitiv für den Einsteig ist). 

!! *Achtung: RStudio funktioniert nicht ohne R!*

Durch Erweiterungen ("Pakete"), stehen dir enorm viele statistische Verfahren zur Verfügung. Wenn du ein bestimmtes Verfahren suchst, gib einfach in einer Suchmaschine das entsprechende Verfahren ein und z.B. "package in R".

Einige Anwendungsbereiche von R sind:

1. deskriptive Methoden (wie Häufigkeits- und Kontigenztabellen)
2. Plots und Diagramme (z.B. Paket `ggplot`)
3. statistische Analyse- und Strukturprüfungsverfahren (Regressionen, ANOVAs, Strukturgleichungsmodelle) (z.B. Paket `lavaan`)
4. Verfahren der prädiktiven Modellierung (Machine-Learning, Deep-Learning, Neural Networks, etc.; z.B. Paket `caret`)
6. textbasierte Analysen (quantitative Textanalyse, Text-Mining, Natural Language Processing, etc.; z.B. Paket `quanteda`)

!!! *Da R frei verfügbar ist, kannst du es auch außerhalb der Uni nutzen und weitergeben. So können andere oder du selbst später ebenfalls mit deinen Analysen weiterarbeiten.  
!!! An der Universität Bremen gibt es regelmäßig Lehrveranstaltungen für die Datenanalyse mit R und auch studentische Gruppen, die sich regelmäßig Treffen.*

!!!! <center><a href=https://cran.r-project.org>https://cran.r-project.org</a></center>

## Hilfe

* R Handbücher und Dokumentation (Sprache: Englisch):
  - <https://cran.r-project.org/manuals.html>
* R Dokumente aus der Community (Sprache: Englisch, versch. Sprachen):
  - <https://cran.r-project.org/other-docs.html>
- RStudio
  - <https://rstudio.com/products/rstudio/>

Buch-Empfehlungen zum Einstieg in R:

* Sebastian Sauer - *Moderne Datenanalyse mit R* (Sprache: Deutsch, Online-Ausgabe verfügbar)
  * <https://link.springer.com/book/10.1007%2F978-3-658-21587-3>
  * Garrett Grolemund, Hadley Wickham - *"R for Data Science"* (Sprache: Englisch, frei verfügbar)
    * <https://r4ds.had.co.nz/>
