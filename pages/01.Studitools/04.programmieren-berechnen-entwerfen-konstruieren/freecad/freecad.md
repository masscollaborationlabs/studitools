---
title: 'FreeCAD - technische Zeichnungen anfertigen, Bauteile und Maschinen planen'
date: '06-05-2021'
publish_date: '06-05-2021'
taxonomy:
    category:
        - Programmieren
        - Berechnen
        - Entwerfen
        - Konstruieren
---

![](img/freecad.png)

===

# FreeCAD - technische Zeichnungen anfertigen, Bauteile und Maschinen planen

* Das nötige Ersatzteil mit deinem eigenen 3D-Drucker erstellen?
* Auch im Homeoffice willst du Maschinen konstruieren - aber womit? 
* Deinen Fahrrad-Unterstand im Garten planen?

Um digital Bauteile zu konstruieren, technische Zeichnungen anzufertigen und 
Belastungssimulationen von Objekten zu erstellen brauchst du spezielle 
Software. "CAD" ist das Zauberwort (steht für "computer-aided design"). Und mit 
FreeCAD hast du dafür ein kostenloses Open-Source Programm, das auf allen 
Betriebssystemen funktioniert.

! **Hinweis:**  
! *CAD-Programme sind für die Erstellung technischer Zeichnungen und Konstruktion von Bauteilen angelegt - ob 2D oder 3D.  
! Wenn du eher ein Programm für grafisch eindrucksvolle Visualisierungen oder Animationen suchst, dann schau dir doch mal [Blender](../../audio-video-multimedia/blender/) an.*

Bei FreeCAD baust du aus einfachen geometrischen Formen deine komplexen 
Bauteile. Dabei kannst du mit ein bisschen mathematischer Magie ("boolschen 
operationen") Objekte miteinander in neue Formen bringen: verschmelzen, 
voneinander abziehen, ausschneiden oder nur die Überschneidungen der Objekte 
behalten. So modellierst du nach und nach z.B. Löcher für Befestigungen, 
Scharniere, Bohrungen und schließlich ganze Bauteile und Maschinen. Mit 
verschiedenen Werkzeugen legst du genaue Grad- und Maßangaben fest für 
Abstände, Aussparungen, Löcher und Kanten.

Okay - aber du müsstest eher etwas Zeichnen…?  
Klar! Entwürfe und Skizzen kannst du auch als 2D-Zeichnung erstellen: FreeCAD 
bringt auch einen Modus für technische Zeichnungen mit - etwa für Architektur 
oder Baupläne.

Da es bei technischen Zeichnungen oder der Fertigung von Maschinen und 
Bauteilen immer auf genaue Maße ankommt, liegt hier die große Stärke von 
CAD-Programmen: alle Kanten und Teile lassen sich mit sog. "Beschränkungen" 
versehen, die sich aufeinander beziehen. Löcher und Kantenlängen lassen sich 
damit vergrößern oder verkleinern, je nach beliebigen anderen Maßen deines 
Objekts. So kannst du schnell die Größe variieren, wenn das nötig sein sollte.

Der Knackpunkt bei der Sache?  
Den findest du mit FEM-Simulationen (finite Elemente Methode). Bauteile müssen 
nämlich oft Hitze oder Spannungen aushalten - und mit einer solchen 
Belastungs-Simulation kannst du schon vorher sehen, wann es heikel wird. 

!!!! <center><a href=https://www.freecadweb.org>https://www.freecadweb.org</a></center>

## Hilfe

- Offizielles FreeCAD-Wiki (Sprache: DE / Int.):
  * <https://wiki.freecadweb.org/Getting_started/de>
- Video-Tutorial "Der FreeCAD 0.19 Grundkurs (Deutsch)" von *flowwies Corner* (YouTube; Sprache: DE):
  * <https://www.youtube.com/watch?v=8tvBLCdyjI4&list=PLw48L7HmCgML1XsR7LSaXlUZkLHyrvVcR>
- Video-Tutorial "FreeCAD FEM Tutorial Simulation mit Kontakt" von *anisim Open Source Engineering Software* (YouTube; Sprache: DE):
  * <https://www.youtube.com/watch?v=Y9l1zYnMj4g>

