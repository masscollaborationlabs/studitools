---
title: 'Jupyter - Entwicklungsumgebung und interaktives Notizbuch für viele Programmiersprachen'
date: '06-05-2021'
publish_date: '06-05-2021'
taxonomy:
    category:
        - Programmieren
        - Berechnen
        - Entwerfen
        - Konstruieren
---

![](img/jupyter_lab.png)

===

# Jupyter - Entwicklungsumgebung und interaktives Notizbuch für viele Programmiersprachen

*Jupyter* ist eine Entwicklungsumgebung als Web-App (*JupyterLab*), das 
interaktive "Notizbücher" enthalten kann (*Jupyter Notebook*). Und auch, wenn 
der Name "Jupyter" klingt, als würde nur Python unterstützt: tatsächlich lassen 
sich sehr viele Programmiersprachen nutzen - darunter auch die für Statistik 
beliebte Sprache 
[R](../rstudio/).

! **Hinweis:**  
! *JupyterLab ist eine vollständige Neu- bzw. Weiterentwicklung vom früheren Projekt "Jupyter Notebook".*

Die Web-App läuft auf deinem Rechner (oder auf einer Plattform). Um darin zu 
arbeiten, musst du also lediglich auf eine Webseite gehen - und los geht's!

Der Grundgedanke dabei ist, dass du verschiedene "Bausteine" bzw. Module 
zusammensetzen kannst:  
Abschnitte mit Text (z.B. deiner Forschungsarbeit) wechseln sich dabei ab mit 
Blöcken, die Programmcode enthalten oder Grafiken und Plots deiner Daten.  
Dieser Programmcode ist interaktiv und du kannst einzelne Blöcke des Codes 
jederzeit verändern, neu berechnen und die Grafiken damit ebenfalls neu 
erstellen lassen. Sogar per Drag-n-Drop lassen sich diese Blöcke umsortieren. 
Am Ende speicherst du alles in einer einzigen Datei.

Damit kannst du also sowohl deine Forschungsarbeit schreiben, als auch 
programmieren - und am Ende präsentieren. Dein gesamter Text und Programmcode 
lässt sich einfach anderen weitergeben - inklusive der Ausgangsdaten und z.B. 
interaktiver Diagramme und Grafiken.  
Das macht z.B. deine Datenanalyse besonders anschaulich, nachvollziehbar und 
nachhaltig frei verfügbar:  
Alle Komponenten stützen sich nämlich auf andere freie Software und Standards 
wie etwa das Datenformat JSON, der Auszeichnungssprache Markdown oder dem 
Textsatz-System 
[LaTeX](../../06.schreiben/latex/). 
Damit lässt sich auch in 50 Jahren noch deine Forschungsarbeit nicht nur 
ansehen, sondern auch genau so reproduzieren - inklusive der von dir 
verwendeten Daten.

! **Hinweis:**  
! *Jupyter benötigt die Programmiersprache [Python](https://www.python.org/). Diese bringt auch ein eigenes Paketmanagement mit, um Python-Module und -Programme zu installieren. Damit kannst du Jupyter wie in der Anleitung beschrieben installieren.  
! Vielleicht etwas ungewohnt für Windows-Nutzer*innen dabei: die Installationsbefehle müssen auf der Kommandozeile eingegeben werden.*

<center><a href=https://jupyter.org>https://jupyter.org</a></center>

## Hilfe

- Offizielle Hilfe-Seite von Jupyter (Sprache: EN):
  * <https://jupyter.org/documentation>
- Installations-Anleitung von Jupyter (Sprache: EN):
  * <https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html>

- Video "Getting Started with JupyterLab | SciPy 2019 Tutorial | M. Bussonnier, 
  J. Grout, S. Stattel" von *Enthought* (YouTube; Sprache: EN):
  * <https://www.youtube.com/watch?v=RFabWieskak>


