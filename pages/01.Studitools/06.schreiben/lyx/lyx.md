---
title: 'LyX - LaTeX-Editor für Einsteiger*innen'
date: '07-09-2021'
publish_date: '07-09-2021'
taxonomy:
    category:
        - Schreiben
---

![](img/lyx.png)

===

# LyX - LaTeX-Editor für Einsteiger*innen

* Ohje, dieses LaTeX ist ganz schön… wie programmieren… *Urgs*!
* So viel Schreiben - kann ich das nicht einfach zusammenklicken?

Ja gut, 
[LaTeX](../latex/) 
kann am Anfang ganz schön unübersichtlich sein…  
Gut, dass es *LyX* gibt!

! **Hinweis:**  
! *Wenn du eine einfachere Methode zum Schreiben suchst, die aber ebenfalls auf 
! einfachen Textdateien basiert: schau dir mal den Artikel zu 
! [Markdown](../markdown/) 
! an. Damit kannst du ebenfalls deine Texte später mit LaTeX setzen.*

LyX ist ein Editor für das Textsatz-Programm LaTeX, dessen Aussehen eher an eine 
Office-Oberfläche angelehnt ist. Wenn du bisher eher wenig oder keine Erfahrung 
mit LaTeX hast, eignet sich LyX zum Einstieg hervorragend, denn viele Funktionen 
sind einfach über Menüs erreichbar. So kannst du ohne viel Vorwissen 
losschreiben und trotzdem den grandiosen Text- und Formelsatz von LaTeX nutzen.  
Wenn du dich schon besser auskennst, lässt LyX dich aber auch normale 
LaTeX-Befehle nutzen, wann immer du willst.

Viele Funktionen und Elemente für deinen Text kannst du bei LyX über 
Assistenten und Dialoge einfügen. Werkzeuge gibt es z.B. für den Formelsatz, 
Tabellen, Grafiken und natürlich gängige Formatvorlagen für Absätze.

Am Ende kannst du einfach mit einem Klick eine PDF von deinem Text generieren 
und LaTeX erledigt im Hintergrund den Rest. Nice!

!!!! <center><a href=https://www.lyx.org/>https://www.lyx.org/</a></center>

## Hilfe

- Wiki von LyX:
  * <https://wiki.lyx.org/>
- Mailinglisten des LyX-Projekts:
  * <https://www.lyx.org/MailingLists>
- Video-Tutorial "Typesetting Beautiful Mathematics with LaTeX, Lyx and MathJax" 
  (*Data Science Cornwall*, YouTube, Sprache: EN)
  * <https://www.youtube.com/watch?v=SGYUv2v-b34>
