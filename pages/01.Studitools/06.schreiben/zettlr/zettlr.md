---
title: 'Zettlr - der digitale Zettelkasten'
date: '16-07-2020'
publish_date: '16-07-2020'
taxonomy:
    category:
        - Schreiben
---

![](img/zettlr.png)

===

# Zettlr - der digitale Zettelkasten

Zettlr ist ein moderner und sehr flexibler Text-Editor: für Notizen, als Zettelkasten oder zum Schreiben einer längeren wissenschaftlichen Arbeit. Zettlr kann dich mit vielen nützlichen Funktionen unterstützen:

- universelles Dokumenten-Format ([Markdown](../markdown/))
- Gliederung in einzelne Projekte
- Schlagworte 
- Verknüpfung von Notizen mit Links
- Anbindung an deine Literaturverwaltung (z.B. [Zotero](../../sammeln-speichern-ordnen/zotero/), BibTex…)
- Unterstützung von Tabellen und Fußnoten
- Timer nach der "Pomodoro"-Methode für fokussiertes Schreiben
- ein "readability-mode", der deine Sätze auf ihre Lesbarkeit / Verständlichkeit prüft: sind deine Sätze zu lang, werden sie farbig markiert. 

Für die Dateien benutzt Zettlr die *Markdown*-Konvention. Alle Texte sind also einfache Textdateien, die du auch mit jedem anderen Text-Editor öffnen kannst.

Das besondere an Zettlr ist die Philosophie der Zettelkasten-Methode:  
Notizen und Gedanken auf vielen kleinen Zetteln können ganz neu kombiniert werden. Dadurch entstehen manchmal außergewöhnliche Gedankengänge und Zusammenhänge. So ein Zettelkasten wird dann zu einem "zweiten Gehirn": vieles vergisst du aber entdeckst es wieder neu - und bringst es so in einen ganz anderen Zusammenhang. Durch die Verknüpfung verschiedener Zettel kannst du einzelne Gedanken später in einen Fließtext bzw. eine sinnvolle Argumentation umwandeln. Schon viele berühmte Menschen in der Geschichte haben mit diesem System gearbeitet.

Deine Texte kannst du mit Zettlr als Office-Datei, Präsentation oder PDF exportieren. Für den Export als PDF nutzt Zettlr [LaTeX](../latex/) (das für seinen professionellen Textsatz berühmt ist). Fortgeschrittene können das Aussehen der PDFs also auch mit eigenen LaTeX-Vorlagen anpassen.

!!!! <center><a href=https://www.zettlr.com/>https://www.zettlr.com/</a></center>

## Hilfe

- Quick-Start-Guide (EN):
  * <https://docs.zettlr.com/en/5-minutes/>
- Video: *Introduction to Zettlr*:
  * <https://www.youtube.com/watch?v=vStbtF4_grE>
* Video-Reihe: Zettlr HowTo
  + <https://www.youtube.com/watch?v=dQ_tpVtLX8k&list=PL2ydvDwV-1u5ncnkxQWZhbHQdwpY2fx5f>

- Zettlr als Zettelkasten (EN):
  * <https://docs.zettlr.com/en/guides/guide-zettelkasten/>
- Zettlr als Notizbuch (EN):
  * <https://docs.zettlr.com/en/guides/guide-notes/>

Zur Zettelkasten-Methode:

- Zettelkasten (Wikipedia):
  - <https://de.wikipedia.org/wiki/Zettelkasten>
- Zettlr Handbuch: *The Zettelkasten method*:
  * <https://docs.zettlr.com/en/academic/zkn-method/>
- Ahrens, Sönke. 2017. *Das Zettelkasten-Prnzip. Erfolgreich wissenschaftlich Schreiben und Studieren mit effektiven Notizen.*
  - <https://takesmartnotes.com/>

