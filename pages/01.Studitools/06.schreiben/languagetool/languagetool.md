---
title: 'LanguageTool - Rechtschreib- und Grammatik-Prüfung'
date: '17-03-2020'
publish_date: '17-03-2020'
taxonomy:
    category:
        - Schreiben
---

![](img/languagetool.png)

===

# LanguageTool - Rechtschreib- und Grammatik-Prüfung

Mit LanguageTool kannst du deine Texte nicht nur auf Rechtschreibung, sondern auch auf Grammatik überprüfen. 

Das Open-Source-Programm LanguageTool gibt es in verschiedenen Varianten:  

- Webseite 
- Plug-In (für deinen Browser, [LibreOffice,](../libreoffice/) etc.) 
- eigenständiges Programm.

Und dann?  
Text eingeben und überprüfen lassen!

Gefundene Fehler werden farbig hervorgehoben und bekommen einen kleinen Hinweis, was z.B. grammatikalisch falsch sein könnte oder dass eine Formulierung Umgangssprachlich ist. Außerdem warnt dich LanguageTool auch vor Wiederholungen, doppelten Leerzeichen oder möglichen Umbrüchen in Abkürzungen wie "z.B.".

!!! **Hinweis:**
!!! Du kannst auch eigene Regeln für LanguageTool erstellen. So kannst du selbst z.B. dabei helfen, Korrekturvorschläge für eine bestimmte Sprache zu erstellen. Diese stehen anschließend auch allen anderen Nutzer\*innen zur Verfügung.

!!!! <center><a href=https://www.languagetool.org/de/>https://www.languagetool.org/de/</a></center>

## Hilfe

- LanguageTool Community
  * <https://community.languagetool.org/>
- LanguageTool Rule Editor (Online)
  * <https://community.languagetool.org/ruleEditor2/>
- andere Software, die LanguageTool unterstützt
  * <http://wiki.languagetool.org/software-that-supports-languagetool-as-a-plug-in-or-add-on>
