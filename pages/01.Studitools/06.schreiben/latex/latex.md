---
title: 'LaTeX - Textsatz für Wissenschaft, Buchdruck und Typograf*innen'
date: '12-12-2020'
publish_date: '12-12-2020'
taxonomy:
    category:
        - Schreiben
---

![](img/texmaker2.png)

===

# LaTeX - Textsatz für Wissenschaft, Buchdruck und Typograf*innen

- LaTeX ist ein… nein. Ja, genau. Du solltest nicht einfach "Latex" in deine 
  Suchmaschine eingeben… 

Egal was du da gerade gefunden hast - LaTeX ist ein Textsatz-System für 
Dokumente.  
Damit geht alles: ob Brief oder Lebenslauf, Bücher, technische Dokumentationen 
oder deine Doktorarbeit. 

Neben einer eingebauten Verwaltung von Quellen und Zitaten (BibTeX) gibt es 
außerdem viele Möglichkeiten für Tabellen und Grafiken, Verzeichnisse und 
typografische Feinheiten.

! **Hinweis:**  
! *Wenn du eine Literaturverwaltung für LaTeX suchst, solltest du dir entweder 
! [Jabref](../../.sammeln-speichern-ordnen/jabref/) 
! oder 
! [Zotero](../../sammeln-speichern-ordnen/zotero/) 
! einmal genauer ansehen. Beide Literaturverwaltungen findest du ebenfalls auf 
! Studytools.*

Gerade für umfangreiche Projekte, Zeichnungen und Formeln ist LaTeX besonders 
gut geeignet. Daher wird es gerade in den Natur- und Technikwissenschaften 
verwendet. 

LaTeX bzw. TeX wurde zwar schon in den 1970er Jahren programmiert, gilt aber 
bis heute noch immer als *der* Standard für Layout und Typografie. Die Qualität 
der Dokumente ist bis heute einfach unübertroffen gut und die Dokumente sind 
auch nach 50 Jahren noch mit demselben Programm zu bearbeiten!  
(Ha! Nimm *das*, Word!)

## Warum verwenden dann nicht alle LaTeX, wenn es so super ist?

Der größte Unterschied ist die Bedienung - die für viele erst einmal sehr 
ungewohnt ist:

- **bei Office-Programmen** wird das Layout des Dokuments schon während du 
  schreibst angezeigt - als wäre es schon gedruckt.
- **bei LaTeX** schreibst du Anweisungen für das Layout in deinen Text (ein 
  bisschen wie beim Programmieren). Das fertige Dokument wird erst nach dem 
  Umwandeln in ein Dokumentenformat wie PDF sichtbar.

## Vorteile von LaTeX

1. **keine kaputten Layouts:** Da LaTeX-Dokumente einfache Textdateien sind, 
   treten keine Layout-Fehler beim Schreiben auf, wie z.B. oft bei 
   unterschiedlichen Office-Programmen.
2. **freie Wahl des Editors:** LaTeX-Dokumente (Textdateien) sind theoretisch 
   mit jedem Texteditor zu bearbeiten. Mehr Komfort bieten allerdings spezielle 
   LaTeX-Editoren (wie z.B. 
   [TeXstudio](../texstudio/)), 
   mit Textvervollständigung Schaltflächen und Tastenkürzeln für wichtige 
   Funktionen haben (ähnlich wie ein Office-Programm).  
3. **gut für Bücher und lange Dokumente:** LaTeX ist ein Programm für den 
   Textsatz (früher war "Setzer*in" ein eigenständiger Beruf). Du als Texter*in 
   schreibst also Anweisungen, wie die Arbeit später gegliedert sein soll und 
   LaTeX (Setzer*in) kümmert sich darum, alles "in Form" zu bringen.\ Mit LaTeX 
   schreibst du beispielsweise `\tableofcontents` für ein Inhaltsverzeichnis. 
   Und selbst bei 4000-Seiten-Dokumenten mit 900 Bildern funktioniert LaTeX 
   noch superschnell.
4. **perfekte Formeln und Typografie:** Formeln funktionieren einfach perfekt 
   und Feineinstellungen für den Buchdruck gibt es auch. Den 
   Qualitätsunterschied siehst du dann einfach!

LaTeX sieht ein bisschen nach Programmieren aus und ist tatsächlich so - aber 
es lohnt sich. Gerade wenn du in Natur- oder Technikwissenschaften studierst 
oder an sehr langen Dokumenten arbeitest, solltest du LaTeX lernen.

! **Hinweis:**  
! *Den LaTeX-Editor 
! [TeXstudio](../texstudio/) 
! stellen wir dir in einem anderen Artikel vor.*

!!! **Tipp:**  
!!! *Die Standard-Schriftart ist wirklich gut(!) auf Papier, sieht aber vielleicht 
!!! für dich aus wie Keilschrift. Andere Schriften lassen sich aber problemlos 
!!! nutzen (schau im Latex Font Catalogue nach anderen Schriften).*

!!!! <center><a href=https://www.latex-project.org/>https://www.latex-project.org/</a></center>

## Hilfe

- Offizielle Hilfe des LaTeX Projekts (Sprache: EN)
  * <https://www.latex-project.org/help/>
- Deutschsprachige Anwendervereinigung TeX e.V. (DANTE e.V.):
  * <https://www.dante.de/>

### Allgemein

- Wikipedia-Artikel zu LaTeX (Sprache: DE)
  * <https://de.wikipedia.org/wiki/LaTeX>

- TeX Blog (Sprache: EN)
  * <https://texblog.org/>

### Installation

- Videotutorial zur Installation von MiKteX und Texmaker (Sprache: EN)
  * <https://www.youtube.com/watch?v=oI8W4MvFo1M>

#### LaTeX-Distributionen

- MikTeX (Sprache: EN)
  * <https://miktex.org/>
  - XeLaTeX (Sprache: EN)
    * <http://xetex.sourceforge.net/>
- TeX Live (Sprache: EN)
  * <https://tug.org/texlive/>

### Vorlagen und Schriften

- LaTeX Templates (Sprache: EN)
  * <https://www.latextemplates.com/>
- The LaTeX Font Catalogue (Sprache: EN)
  - <https://tug.org/FontCatalogue/>

### LaTeX-Editoren

- TeXmaker (Sprache: EN)
  * <https://www.xm1math.net/texmaker/>
- TeXstudio (Sprache: EN)
  * <https://www.texstudio.org/>
- LyX (Sprache: EN)
  * <https://www.lyx.org/>

### Literaturverwaltung

- Jabref (Sprache: EN)
  * <https://blogs.uni-bremen.de/studytools/2020/07/16/jabref-literaturverwaltung-fuer-latex/>
- Zotero (Sprache: EN)
  * <https://blogs.uni-bremen.de/studytools/2020/03/17/zotero-literaturverwaltung-fuer-alle/>
  * Zotero-Plugin "Better BibTeX" (Sprache: EN)
    + <https://retorque.re/zotero-better-bibtex/>
