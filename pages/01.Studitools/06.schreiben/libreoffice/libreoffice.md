---
title: 'LibreOffice - (d)ein Office für alle'
date: '17-03-2020'
publish_date: '17-03-2020'
taxonomy:
    category:
        - Schreiben
---

![](img/libreoffice_writer.png)

===

# LibreOffice - (d)ein Office für alle

- Eure Gruppenarbeit hat ständig Fehler, weil ihr unterschiedliche Office-Versionen habt?
- Dein OpenOffice ist irgendwie… nicht mehr so aktuell?
- Gibt es eigentlich noch was anderes als Microsoft-Office?

Klar!

LibreOffice ist (d)ein frei verfügbares und vollständiges Office für alle Lebenslagen:  
Schreiben (*Writer*), Tabellen (*Calc*) oder Präsentationen erstellen (*Impress*), zeichnen (*Draw*), mathematische Formeln setzen (*Math*)… oder sogar ausfüllbare PDF-Formulare für eine Umfrage.

Es steht in über 100 Sprachen zur Verfügung und funktioniert auf allen Betriebssystemen.  
Weil es Open-Source ist, kannst du es empfehlen und weitergeben, die Oberfläche deinen Wünschen anpassen oder es sogar in eine andere Sprache übersetzen.  
Und damit eure Arbeit auch gleich abgabefertig ist, kannst du sie direkt als PDF speichern, ohne zusätzliche Software.

Darf es sonst noch etwas sein?  
Mit Plug-Ins ("*extensions*") lässt sich die Funktionalität noch erweitern - z.B. mit der Literaturverwaltung [Zotero](../../sammeln-speichern-ordnen/zotero/) oder einer Rechtschreib- und Grammatik-Kontrolle wie [LanguageTool](../languagetool/).

! **Hinweis**  
! *LibreOffice ist das Nachfolgeprojekt vom bekannten OpenOffice, welches nicht 
! mehr weiterentwickelt wird. Aufgrund von Namensrechten durfte sich das neue 
! Projekt nicht "OpenOffice" nennen, als sich die Entwickler_innen 2010 dazu 
! entschlossen, das Projekt mit Hilfe einer Stiftung "(The Document Foundation)" 
! neu auszurichten.*

!!!! <center><a href=https://www.libreoffice.org/>https://www.libreoffice.org/</a></center>

# Hilfe

- Deutsch: <https://de.libreoffice.org/get-help/documentation/>
- Englisch: <https://documentation.libreoffice.org/en/english-documentation/>
- Extensions: <https://extensions.libreoffice.org/extensions>
- Kurzanleitung zum Erstellen einer Facharbeit:
    - <https://wiki.documentfoundation.org/images/3/3a/Kurzanleitung-Facharbeit-A5.pdf>
