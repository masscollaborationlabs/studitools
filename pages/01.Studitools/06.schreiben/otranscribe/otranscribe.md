---
title: 'oTranscribe - Texte transkribieren'
date: '16-07-2020'
publish_date: '16-07-2020'
taxonomy:
    category:
        - Schreiben
---

![](img/oTranscribe.png)

===

# oTranscribe - Texte transkribieren

Du suchst ein einfaches Programm um deine Interviews zu transkribieren?  
Dann ist *oTranscribe* genau das Richtige für dich!

Das Programm läuft direkt im Browser, aber du kannst es auch über die Webseite verwenden oder auf deinem Rechner installieren.

Die Bedienung bietet nur die einfachsten Funktionen:  
Vor- und Zurückspulen, Pausieren, und die Geschwindigkeit einstellen. Im Text kannst du einfache Formatierungen nutzen und Zeit-Marken einfügen. Audio- und Video-Dateien oder auch YouTube-Videos kannst du als Quellen einstellen.

Damit keine sensiblen Daten deiner Interviews irgendwohin geschickt werden, bleiben dein Quell-Material und eingegebenen Texte nur lokal in deinem Browser. So kannst du sicher sein, dass deine Quellen geschützt sind.

Transkripte kannst du als oTranscribe-Dateien speichern, falls du sie später weiter bearbeiten möchtest. Ansonsten werden neben einfachen Textdateien (TXT) und Markdown-Dateien (MD) auch Google Docs unterstützt. Die Dateiformate TXT und MD (Markdown) können mit jedem Text-Editor oder Office-Programm angesehen und bearbeitet werden. Die [Campus-Cloud "Seafile" an der Uni Bremen](https://blogs.uni-bremen.de/studytools/2020/04/01/datei-austausch-seafile-an-der-uni-bremen/) kann z.B. Markdown-Dateien als Dokumente mit Formatierung anzeigen.

!!! **Tipp**  
!!! Du kannst oTranscribe auch offline nutzen: wenn du einmal auf der oTranscribe-Webseite warst rufst du die Seite einfach offline wieder auf. Achte dabei auf die Einstellungen deines Browsers: der Zwischenspeicher darf dafür nicht gelöscht werden.  
!!! Ein kleiner Hack: als Webseiten-Schnappschuss in [Zotero](../../sammeln-speichern-ordnen/zotero/) kannst du die Webseite ebenfalls (in Zotero) speichern. Dabei werden auch alle nötigen Programmteile in Zotero gespeichert, so dass du anschließend oTranscribe über Zotero öffnen kannst.

!! **Achtung!**
!! ***Exportiere unbedingt in regelmäßigen Abständen dein Transkript als Datei, damit keine Arbeit umsonst war!***

!!!! <center><a href=https://otranscribe.com/>https://otranscribe.com/</a></center>

## Hilfe

- Hilfe zu oTranscribe:
  * <https://otranscribe.com/help/>
- Projektseite auf GitHub (u.a. Anleitung für Offline-Installation)
  * <https://github.com/oTranscribe/oTranscribe>

