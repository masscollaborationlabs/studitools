---
title: 'Der digitale College-Block: Xournal++'
date: '16-07-2020'
publish_date: '16-07-2020'
taxonomy:
    category:
        - Schreiben
---

![](img/xournalpp.png)

===

# Der digitale College-Block: Xournal++

* Du willst eine PDF mit handschriftlichen Notizen versehen?
* Mitschriften auf dem Tablet anfertigen?
* Die Folien mit gesprochenen Erklärungen aufnehmen, damit du sie auch in einem Jahr noch verstehst?

Mit Xournal++ kannst du handschriftliche Notizen in eine PDF oder eine neue Datei schreiben. Das Aussehen kann dabei wie ein klassischer College-Block sein: (un)liniert oder kariert. 

Dir stehen in verschiedene Schriften, Stifte, Linien, Marker, Farben und Formen zur Verfügung. Sogar Formeln ([LaTeX](../latex/)-Syntax) und Sprach-Aufzeichnungen kannst du als Notiz anlegen. Alle Elemente sind wie auf einer Folie über ein Blatt Papier aufgezeichnet und lassen auch nachträglich bewegen.

Deine bearbeiteten Dateien können als Xournal++-Datei gespeichert werden, als PDF oder verschiedene Bild-Formate (PNG und SVG).

!!! **Tipp:**
!!! Für handschriftliche Notizen eignet sich am besten ein Grafik-Tablett bzw. eine Stift-Eingabe.

!!!! <center><a href=https://github.com/xournalpp/xournalpp>https://github.com/xournalpp/xournalpp</a></center>

## Hilfe 

- Manual (Sprache: Englisch)
  * <https://github.com/xournalpp/xournalpp/wiki/User-Manual>
- FAQ (Sprache: Englisch)
  * <https://github.com/xournalpp/xournalpp/wiki/Frequently-Asked-Questions-&-Problem-Solving>
