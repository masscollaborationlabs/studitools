---
title: 'TeXstudio - umfangreicher Editor für LateX'
date: '07-05-2021'
publish_date: '07-05-2021'
taxonomy:
    category:
        - Schreiben
---

![](img/texstudio2.png)

===

# TeXstudio - umfangreicher Editor für LateX

TeXstudio ist ein umfangreicher Editor für das Textsatz-System 
[LaTeX](../latex/), 
der als freie Software auf allen Plattformen zur Verfügung steht.

TeXstudio unterstützt dich beim Schreiben deiner Dokumente mit vielen 
Funktionen, wie einer Gliederung deiner Überschriften, mehreren Sammlungen 
spezieller Zeichen für logische operationen, mathematischer, chemische oder 
anderer Formeln. Auch für Grafiken gibt es entsprechende Liste mit Befehlen für 
PSTricks und PGF/TikZ.

Ein Editor für LateX macht natürlich die Eingabe der vielen 
Formatierungsbefehle sehr viel einfacher: so schlägt dir TeXstudio schon beim 
Tippen entsprechende TeX-Makros bzw. Befehle vor und zeigt dir auch die Syntax 
und möglichen Parameter an. Falls du dabei Fehler macht, wird die entsprechende 
Zeile mit einer Warnung markiert.

Tabellen, Briefe oder Beamer-Dokumente (für Präsentationen) kannst du über 
eigene Assistenten im Menü anlegen. Außerdem findest du Schaltflächen für 
gängige Formatvorlagen, Referenzen und Schriftgrößen und für die 
Tabellen-Gestaltung. Dadurch eignet sich TeXstudio auch für Einsteiger\*innen.

Im Hintergrund aber lässt dich TeXstudio vieles Einstellen: ob 
Versionsverwaltung mit SVN oder Git, spezieller Compiler-Aufruf, 
Syntax-Vervollständigung eines speziellen Pakets oder Farbschema… fast alles 
lässt sich ändern.

Um eine Bibliografie bzw. dein Literaturverzeichnis anzulegen, kannst du direkt 
den jeweiligen Literatur-Typ aus einem Menü wählen. Auch abseits einer 
vollwertigen Literaturverwaltung speicherst du deine Quellen so in einer 
BibTeX-Datei für dein Literaturverzeichnis.
  
!!! **Hinweis:**  
!!! *Wenn du eine Literaturverwaltung für LaTeX suchst, dann sieh dir mal [Jabref](../../sammeln-speichern-ordnen/jabref/) oder [Zotero](../../sammeln-speichern-ordnen/zotero/) an. 
!!! Jabref ist für LaTeX ausgelegt und für Zotero gibt es eine sehr gute Erweiterung namens [better BibTeX](https://retorque.re/zotero-better-bibtex/).*

Am Ende alles in eine PDF umzuwandeln geht ebenfalls komfortabel per 
Knopfdruck. Und Falls dabei mal Fehler passieren sollten - die siehst du 
entsprechend im Ausgabe-Fenster mit dem Log.

!!!! <center><a href=https://www.texstudio.org/>https://www.texstudio.org/</a></center>

## Hilfe

- Wiki von TeXstudio auf Github.com:
  - <https://github.com/texstudio-org/texstudio/wiki>
- Video-Tutorial "LaTeX Tutorial 02: Installation von LaTeX und TeXstudio" (*Thomas Erben - Tutorials und Lehrvideos*, YouTube; Sprache: DE): 
  - <https://www.youtube.com/watch?v=j2qY-SDRV70>
