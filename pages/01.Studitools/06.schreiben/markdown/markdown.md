---
title: 'Markdown - Einfach. Gut. Schreiben.'
date: '07-12-2021'
publish_date: '07-12-2021'
taxonomy:
    category:
        - Schreiben
---

![](img/markdown.png)

===

# Markdown - Einfach. Gut. Schreiben.

> *"Ein Tool, sie alle zu schreiben, […] selbst im Dunkeln noch zu tippen und 
> ewig zu speichern…"*  
> - Der Herr der Ringe

Ja, Schreiben im digitalen Zeitalter ist nicht weniger kompliziert als früher: 
kam es damals auf Federkiel und Tinte an, hast du heute hunderte Programme zur 
Auswahl. Und darfst dich ständig umgewöhnen: vieles ist nicht kompatibel 
miteinander und verschwindet vielleicht nach 5 Jahren im Schicksalsb… sorry - 
in der Versenkung. Nerv!

Aber es geht auch anders!

Es gibt sie: eine Methode, mit der du (fast) überall schreiben kannst und am 
Ende trotzdem immer ein gut formatierter Text dabei herauskommt - egal ob 
Chat-Nachricht, Buch oder eine Webseite. Die Lösung heißt: Markdown!

!!! **Fun-Fact:**  
!!! *Alle Texte für Studytools werden mit Markdown geschrieben.*

*Markdown* ist eine Markup-Sprache (eine sogenannte "Auszeichnungssprache"). Mit 
Sonderzeichen bekommen Worte, Sätze oder Absätze bestimmte Funktionen 
zugewiesen. Nach der Umwandlung in das endgültige Format ist der fertige Text 
dann auch genau so formatiert.  
Das Tolle daran ist: du benötigst kein spezielles Programm zum erstellen und 
kannst mit wenigen Zeichen einen fertig formatierten Text schreiben. Der ist 
schon als "roher" Text gut lesbar - nicht erst nach dem Umwandeln.  
Die Basics von Markdown kannst du außerdem innerhalb weniger Minuten lernen, 
denn die sind wirklich intuitiv!

Und wie soll das jetzt "die eine Lösung für jeden Text" sein?  
Easy-Peasy: wahnsinnig viele Programme verstehen von sich aus schon Markdown! 

! **Hinweis:**  
! *Markdown ist eine Konvention um einfache Textdateien zu schreiben. Daher 
! brauchst du im Grunde kein besonderes Programm, um Markdown zu schreiben. 
! Hilfreich ist es aber trotzdem, denn ein Markdown-fähiger Editor kann dir mit 
! Farben ("Syntax-Highlighting") oder sichtbarer Formatierung die Bedeutung deiner 
! Sonderzeichen anzeigen. Damit sieht dein Text entweder bunt und besser lesbar 
! aus, oder gleich wie der fertige Text.*

Hier ein paar Beispiele:

1. Der Editor [Zettlr](../zettlr/) ist ein Markdown-Editor, der fürs wissenschaftliche 
   Schreiben konzipiert wurde. Damit kannst du alle Anforderungen umsetzen - 
   von Fußnoten bis Quellenangaben oder Diagrammen.
2. Das Mindmap-Programm [Freeplane](../../sammeln-speichern-ordnen/freeplane/) kann deine Mindmap als Markdown-Dokument 
   ausgeben. Dafür werden alle Überschriften in der Mindmap 
   Kapitel-Überschriften in Markdown. Schwupps, schon ist deine Mindmap ein 
   Text!
3. Auch im Chat-Programm Rocket.Chat der Uni-Bremen kannst du mit Markdown 
   deine Nachrichten formatieren.
4. Das Statistik-Programm [R](../../programmieren-berechnen-entwerfen-konstruieren/rstudio/) nutzt eine Markdown-Variante (R-Markdown) und 
   kann damit deine Datenanalysen als druckfertiges Dokument erstellen - ganz 
   ohne Office, aber mit allen Grafiken und Tabellen.

Aber damit nicht genug:  
Neben den vielen Online-Editoren, Programmen und Apps mit Markdown-Unterstützung
lassen sich mit Markdown auch Präsentationen erstellen, Blogs gestalten oder 
sogar Diagramme und Mindmaps zeichnen… 

Markdown ist also sehr flexibel nutzbar - es kommt nur darauf an, in welches 
Format es später umgewandelt werden soll und welches Programm du dazu nutzen 
willst. Deinen Text kannst du also für immer lesbar speichern, ohne auf 
Kostenpflichtige Software angewiesen zu sein.

!!! **Fun-Fact:**  
!!! *Markdown wurde 2004 von John Gruber und Aaron Swartz erdacht. Markdown 
!!! orientiert sich dabei am Stil für E-Mails und sollte vor allem eines: ganz 
!!! einfach lesbar und schreibbar sein - egal ob als einfacher oder formatierter 
!!! Text.*

## Markdown-Varianten

Dank dieser schlichten Eleganz gibt es inzwischen viele unterschiedliche 
Varianten von Markdown für unterschiedliche Bereiche:  
neben dem ursprünglichen "Markdown" gibt es nun "Github-flavored-Markdown" (GfM; 
von der gleichnamigen Coding-Plattform) und Varianten wie "R-Markdown" für das 
Statistik-Programm R.  
Das Projekt "[CommonMark](https://commonmark.org/)" versucht die vielen 
Varianten und ihre Spezialfälle zu bündeln, um daraus einen tatsächlichen 
Standard zu entwickeln.  
Und der Dokumentenkonverter [Pandoc](https://pandoc.org/) wandelt fast jedes 
Dokumentenformat in ein anderes um… und nutzt das unglaublich umfangreiche 
"Pandoc-Markdown". 

Wenn du mit Markdown aus deinem *einfachen* Text einen *schönen* Text zaubern 
möchtest, brauchst du also eine Software, die Markdown versteht und dir daraus 
etwas bastelt: eine HMTL-Webseite, ein E-Book, eine PDF, Office-Dokument oder 
Blog - für alle Bereiche gibt es unterschiedliche Software. Aber die ist 
erstmal gar nicht so wichtig…  
Wichtig ist, dass du Markdown schreiben und lesen kannst!

Also, guck mal…

## Beispiele

### Überschriften

Überschriften schreibst du mit einem oder mehreren Rauten-Symbolen vor der 
entsprechenden Zeile:`#`. In etwa so:

```
# Hier ist eine Überschrift

## eine Unter-Überschrift
```

### Schrift

Möchtest du später etwas in fettgedruckten Buchstaben sehen, brauchst du meist 
zwei Sterne ("Asterisk") `*` oder Unterstriche `_` um ein Wort, Satz oder 
Paragrafen zu machen - vorn und hinten (wie in `hier steht ein Wort in 
**Fettdruck**`).  
Für kursiv - eine "leichtere" Art der Hervorhebung - entsprechend nur einmal 
`*` oder `_`: `Dieses Wort ist *wichtig*!`.

### Listen

Listen kannst du einfach mit `-`, `+` oder auch `*` am Anfang einer Zeile 
beginnen. Dann allerdings mit einem Leerzeichen dahinter:

```
- hier steht
- ein Beispiel
- einer Liste
```

### Links

Einen Link zu einer Webseite kannst du mit unterschiedlichen Klammern erstellen:

```
[Hier geht es zum ZMML](https://www.zmml.de)
```

### Bilder

Ein Bild ist quasi wie ein Link, nur mit einem Ausrufungszeichen davor:

```
![Bildbeschreibung](meinbild.jpg)
```

### Tabellen

Tabellen kannst du mit `|` als Trennzeichen zwischen Spalten erstellen:

```
| Spaltenüberschrift 1 | Spaltenüberschrift 2 |
| ---                  | ---                  |
| Inhalt Spalte 1      | Inhalt Spalte 2      |
| noch was …           | und weiter           |
| noch mehr            | bla bla              |
```

### Fußnoten 

In einem Text kannst du`^[auch Fußnoten unterbringen]`, diese werden später 
automatisch nummeriert.

***

Okay, genug Beispiele jetzt! Mehr findest du in vielen Markdown-Tutorials 
online…

Und jetzt: ran an die Tasten, probier's aus!

## Hilfe

### Video-Tutorials

* "Academic Writing in Markdown" (*Nicholas Cifuentes-Goodbody*, CC BY-NC-ND 4.0, Sprache: EN)
  + <https://www.youtube.com/watch?v=hpAJMSS8pvs>
* "Besser schreiben mit Pandoc und Markdown von Albert Krewinkel" (*NooK*, YouTube; Sprache: DE)
  + <https://www.youtube.com/watch?v=yalEjU8zY3Y>
* "Wissenschaftliches Schreiben in Markdown" (*Alumni des Heidelberger Life-Science Lab* / *Hendrik Erz*, *Albert Krewinkel*, YouTube, Sprache: DE)
  + <https://www.youtube.com/watch?v=7awIB5FO__g>

### Artikel & Texte

* "Markdown" (John Gruber, Sprache: EN)
  + <https://daringfireball.net/projects/markdown/>
* CommonMark - A strongly defined, highly compatible specification of Markdown (Sprache: EN)
  + <https://commonmark.org/>
* GitHub Flavored Markdown Spec (Sprache: EN)
  + <https://github.github.com/gfm/>

* "wissenschaftliche Texte schreiben – mit Markdown und Pandoc" (*Justus 
  Holzberger*, CC-BY 4.0, Sprache: DE)
  + <https://vijual.de/2019/03/11/artikel-mit-markdown-und-pandoc-schreiben/>
  + als PDF: 
    <http://vijual.de/wp-content/uploads/2019/03/Artikel_schreiben_mit_Markdown_0.1.5.pdf>
* "R Markdown from R Studio" (Sprache: EN)
  + <https://rmarkdown.rstudio.com/>
* "Scientific Writing with Markdown" (*Jaan Tollander de Balsch*, CC-BY-NC-ND 4.0, Sprache: EN)
  + <https://jaantollander.com/post/scientific-writing-with-markdown/>
* "Learn Markdown in Y Minutes" (*Dan Turkel* und andere, CC-BY-SA 3.0 US, Sprache: EN)
  + <https://learnxinyminutes.com/docs/markdown/>

### weitere Programme

* Pandoc -  a universal document converter
  + <https://pandoc.org/#>
