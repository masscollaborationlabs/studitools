---
title: 'VLC – für alle(s) mit Medien'
date: '29-08-2020'
publish_date: '29-08-2020'
taxonomy:
    category:
        - Audio
        - Video
        - Multimedia
---

![](img/vlc2.png)

===

# VLC - für alle(s) mit Medien

Wenn du nur ein einziges Programm auf eine einsame Insel mitnehmen dürftest… 

…sollte es VLC sein.  
Du wirst jede Menge Zeit haben und eh nur die Musik und Filme auf deinem Gerät. Und kein Ladegerät. ;)

VLC ist *das* Programm für alles, was mit Multimedia, Audio und Video zu tun hat:

- Du willst einen Film gucken?
  Klar! Von BluRay über DVD bis Streaming kann VLC alles.
- Du willst weltweit freie Radio-Kanäle oder Podcasts Anhören?
  Ist in der Playlist schon voreingestellt.
- Deine Musikbibliothek willst du nach Interpreten und Titeln ordnen?
  Musik kannst du direkt in VLC taggen.
- Du willst deinen Desktop im Netzwerk Streamen, damit du anderen ein Tutorial live zeigen kannst?
  Sicher, VLC kann das.
- Videos und Musik soll VLC direkt im Browser abspielen?
  Ja.
- Deine Datei in ein anderes Format umwandeln?
  Kein Problem.
- Du hast nur eine Kommandozeile und willst deine Filme als [ASCII-Art](https://de.wikipedia.org/wiki/ASCII-Art) ansehen?
  Äh, na gut.

Musik hören geht natürlich auch…

VLC gibt es für alle Plattformen - sogar Android und iOS.

---

!!!! <center><a href=https://www.videolan.org/vlc/>https://www.videolan.org/vlc/</a></center>

## Hilfe

- FAQ zu VLC:
  * <https://www.videolan.org/support/faq.html>
- Das VLC-Wiki:
  * <https://wiki.videolan.org/>
