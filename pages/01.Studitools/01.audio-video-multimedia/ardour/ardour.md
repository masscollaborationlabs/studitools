---
title: 'Ardour - dein Tonstudio'
date: '26-11-2020'
publish_date: '26-11-2020'
taxonomy:
    category:
        - Audio
        - Video
        - Multimedia
---

![](img/ardour_2.png)

===

# Ardour - dein Tonstudio

- Für deinen Lieblingsfilm deinen eigenen Soundtrack entwickeln oder ein eigenes Hörbuch aufnehmen?
- Deine Podcasts und Videos brauchen den Zauber professionellen Mixings?
- [Audacity](../audacity/) ist dir zu… einfach?

Mit Ardour gibt es dein eigenes Tonstudio - frei verfügbar und umsonst. 
Sicherlich - super einfach ist so ein Tonstudio nicht in der Bedienung (so viele Knöpfe!) - das nicht. 
Aber: es ist dein eigenes Tonstudio!!!

Im Gegensatz zu einem einfachen Programm wie [Audacity](../audacity/) spielt Ardour in einer ganz anderen Konzerthalle:  
egal ob du ein Video vertonen willst, Mischpulte brauchst, Instrumente, Keyboards und Mikrofone anschließen willst oder deine Audiospuren einzeln mit Effekten belegen - mit Ardour geht quasi alles.

Ein großer Unterschied dabei ist beispielsweise, dass Ardour nicht-destruktiv ist. 
Wenn du also einen Effekt auf eine Aufnahme gelegt hast und am Ende deines Projektes feststellt, dass der Effekt doch ein bisschen zu stark ist, kannst du ihn einfach entfernen. 
Mit anderen Programmen müsstest du die Aufnahme dieses Teils noch einmal ganz neu aufnehmen (Aaaaarrggh!).  
Unterschiedliche Sprecher*innen in einen Hörspiel können so beispielsweise jeweils eigene Klang-Einstellungen haben - je nach Audiospur.

Audiospuren hast du in Ardour übrigens unendlich viele - um dich richtig auszutoben. 
Natürlich kannst du MIDI-Geräte anschließen oder dein Audio-Signal an andere Programme und Geräte schicken. 
Und ja, auch Effekte und Instrumente lassen sich als Plug-Ins in gängigen Formaten (VST, LV2…) in deinem Projekt nutzen.

Deinem nächsten Selfmade-Ohrwurm steht also nichts mehr im Wege.

---

!!!! https://ardour.org/

## Hilfe

- Offizielles Handbuch zu Ardour (Sprache: EN)
  * <https://manual.ardour.org/toc/>
* Offizielles Community-Forum (Sprache: EN)
  + <https://discourse.ardour.org/>
- Video-Tutorial *"Ardour 6 Quickstart (recording, editing, mixing and exporting)"* (*unfa*; YouTube)
  * <https://www.youtube.com/watch?v=bfTAKv4htDE>
