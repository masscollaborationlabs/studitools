---
title: 'Untertitel erstellen - mit Aegisub'
date: '06-11-2020 00:00'
publish_date: '06-11-2020 00:00'
taxonomy:
    category:
        - Audio
        - Video
        - Multimedia
---

![](img/aegisub.png)

===

# Untertitel erstellen - mit Aegisub

- NEIN, die neue Staffel von dieser Serie wirklich raus?
- DOCH: nur auf schwäbisch!?
- OOOOH! Der neue Tatort - endlich auch auf Plattdeutsch!

Wer kennt das nicht: finnischer Filmabend im Wohnheim aber irgendwer versteht das immer nicht?! Meh! (Stichwort: *Barrierefreiheit*)

Hilf deinen Freunden! Denn zum Glück gibt es Software, die dir beim Untertiteln von Filmen, Serien, Tutorials oder was auch immer hilft.  
Software wie Aegisub.

Mit Aegisub kannst du Untertitel für alle Arten von Videos oder Audiodateien erstellen. 
Aber nicht nur Untertitel: auch Beschriftungen lassen sich damit platzieren. 
So könntest du eine Schrift im Hintergrund der Filmszene genau dort übersetzen, wo sie im Original zu sehen ist.

Zur Übersetzung brauchst du sonst nichts weiter:  
Datei auswählen, Projekt starten und los geht’s! 

Mit Tastenkürzeln startest und stoppst du die Wiedergabe des Videos und trägst in bestimmten Zeitpunkten dann deinen Text ein, der zum Video angezeigt werden soll. 
Dabei musst du natürlich auf die korrekten Start- und Endzeitpunkte achten, in der der Text erscheinen soll - ist ja klar (niemand mag Spoiler - auch wenn es Millisekunden sind!).  
Aber Aegisub unterstützt dich dabei, nicht zu spoilern:
das Video wird immer nur in kurzen Teilen abgespielt, die sich perfekt für einen kurzen Satz eignen.

Ist dein Projekt fertig übersetzt, kannst du die Untertitel in im Ordner deines Videos speichern. 
Dafür stehen dir unterschiedliche Dateiformate zur Verfügung. 
Meistens erkennt dann ein Multimedia-Player wie [VLC](../vlc/) schon von allein, dass es eine Untertitel-Spur gibt.

! **Achso:**  
! *Wenn du Untertitel für Filme suchst, gibt es Seiten wie [OpenSubtitles.org](https://www.opensubtitles.org/de), auf die Programme wie VLC oft auch automatisch nach passenden Untertiteln suchen können. 
! Dort kannst du dann auch deine Übersetzungen hochladen, damit auch andere etwas davon haben.*

---

!!!! http://www.aegisub.org/

## Hilfe

- Dokumentation (Sprache: EN)
  * <http://docs.aegisub.org/>

## Videotutorials

- Playlist: *"Aegisub Tutorials"* (TJ Free; YouTube)
  * <https://www.youtube.com/watch?v=4gXF6Y-v6BE&list=PLqazFFzUAPc7BgGTaDAvvsGEoLolq09YP>
- *"TUTORIAL: UNTERTITEL MIT AEGISUB - Erstelle eine SRT-Datei"* (Judith Steiner; YouTube; Sprache: DE? AT? CH?)
  * <https://www.youtube.com/watch?v=yWumyokjax0>

