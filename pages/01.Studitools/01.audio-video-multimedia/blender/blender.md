---
title: 'Blender – 3D-Modelle und Animationen'
date: '29-10-2020'
publish_date: '29-10-2020'
taxonomy:
    category:
        - Audio
        - Video
        - Multimedia
---

![](img/blender.png)

===

#  Blender - 3D-Modelle und Animationen

Blender ist ein… ein was!? Ein Mixer? Ja, jein… also…

Blender ist ein freies Programm zum Erstellen von 3D-Grafiken, Animationen, Simulationen und allem, was noch so 3D ist. 
Also ja - es "mixt" so ziemlich alles zusammen, was du für 3D-Grafiken brauchst.

* die Uni nachbauen oder die Innenstadt neu planen? Ja, mach doch!
* Modelle für dein Lieblings-Spiel erstellen? Klar.
* 3D-Effekte in dein Handy-Video zaubern? Ja.
* 2D- und 3D-Animationen für einen Animationsfilm mischen? Auch das geht.
* Videoschnitt? Sicher.
* Mit "Ton" eine Skulptur erstellen und danach im 3D-Drucker ausdrucken? Na 
  klar!

Blender bietet dir alles, was dein 3D-Herz begehrt:  
Modelle bauen, Skulpturen aus digitalem "Ton" modellieren, animieren (Feuer! Rauch! Wasser!), Video-Editing, Motion-Capturing, 2D-Zeichnungen in 3D ("Grease Pencil") und ein mächtiges "Node"-System für Effekte und Texturen.  
Achso: wenn du ein CAD-Programm brauchst: das kann Blender auch, ja.

…na ok, das klingt jetzt alles sehr fancy.

Aber ja:  
wenn du ein Programm suchst, was irgendwie 3D-Grafiken oder Modelle erstellen und animieren kann - vom einfachen Modell in Bauklotz-Optik bis hin zum Blockbuster-Film - dann benutze Blender!

Was Blender alles kann, das kannst du dir regelmäßig in einem der vielen "Open Movies" ansehen. 
Und wenn du wissen willst, wie das alles geht, dann gibt es tausende guter Tutorials und Anleitungen.

---

!!!! <center><a href=https://www.blender.org/>https://www.blender.org/</a></center>

## Hilfe

- Offizielle Tutorials
  * <https://www.blender.org/support/tutorials/>
- Offizielle Support-Seite (Dokumentation, Handbücher, Tutorials etc.)
  * <https://www.blender.org/support/>
- YouTube-Channel der Blender Foundation
  * <https://www.youtube.com/user/BlenderFoundation>

- Blender "Open Movies" (YouTube):
  - <https://www.youtube.com/playlist?list=PL6B3937A5D230E335>
