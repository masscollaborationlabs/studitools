---
title: 'Shotcut – Videos schneiden für Fortgeschrittene'
date: '30-08-2020'
publish_date: '30-08-2020'
taxonomy:
    category:
        - Audio
        - Video
        - Multimedia
---

![](img/shotcut_2.png)

===

# Shotcut - Videos schneiden für Fortgeschrittene

- Du willst ein Video schneiden?
- [OpenShot](../openshot/openshot.md) ist dir etwas *zu* einfach? 
- Du bezeichnest dich als "Fortgeschnitten"?

Wie wäre es mit Shotcut!? 

Shotcut ist ein Videoschnitt-Programm für Fortgeschrittene, dass etwas mehr Funktionen bietet.
Es ist ebenfalls freie Software und damit unter allen Betriebssystemen kostenlos und ohne Einschränkungen nutzbar.

Wie in jedem Videoschnitt-Programm kannst du Videos, Audio- und Bilddateien beliebig in dein Projekt einfügen.
Viele Effekte und Übergänge bringt Shotcut ebenfalls mit, so dass du auch komplexere Filme und Beiträge damit schneiden kannst. 

Wie bei allen Schnittprogrammen üblich, fügst du für den Schnitt alle Materialien in unterschiedlichen Spuren ein und belegst sie mit den gewünschten Effekten und Übergängen. 

Die Oberfläche und Anordnung aller Optionen lassen sich in Shotcut sehr flexibel anpassen.
So kannst du dir deinen Schnittplatz passend für deine Bedürfnisse einrichten.

Als Export bietet Shotcut viele vordefinierte Profile an, überlässt aber im erweiterten Modus alle Einstellungen dir.
Auch Hardware-Beschleunigtes Kodieren bzw. Rendering wird von Shotcut unterstützt. 

---

!!!! <center><a href=https://www.shotcut.org/>https://www.shotcut.org/</a></center>

## Hilfe

- Erste Schritte mit Shotcut (PDF auf Deutsch):
  * <https://www.shotcut.org/howtos/getting-started/Erste%20Schritte%20mit%20Shotcut.pdf>
- "Getting Started"
  * <https://www.shotcut.org/howtos/getting-started/>
- Video-Tutorials:
  - <https://www.shotcut.org/tutorials/>
