---
title: 'Mixxx – DJ-Software für Live-Mix bis Radio-Show'
date: '26-11-2020 00:00'
publish_date: '26-11-2020 00:00'
taxonomy:
    category:
        - Audio
        - Video
        - Multimedia
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

![](img/mixxx.png)

===

# Mixxx - DJ-Software für Live-Mix bis Radio-Show

- Die eigene Karriere als DJane / DJ während Corona pushen?
- Live-Mixe mit passender Musik für deinen Podcast erstellen?

Mixxx ist ein freies DJing-Programm für viele Anwendungszwecke.
Mit Mixxx kannst du nicht nur Musik wie an einem richtigen Deck bzw. DJ-Pult 
mischen, sondern auch aufnehmen oder sogar live über das Internet streamen. So 
kannst du z.B. einen Mix in deiner eigenen Radio-Show unterbringen und auch 
gleich aufnehmen. 

!!! **Tipp:**  
!!! *Wenn du noch ein Programm fürs Schneiden und Abmischen deiner Audiodateien suchst, probiere doch mal [Audacity](../audacity/) aus oder das Tonstudio [Ardour](../ardour/).*

Die Oberfläche ist ziemlich bunt und selbsterklärend. Du kannst zwischen 2 und 
4 Audiospuren bzw. Tracks in deinem Projekt haben. Dabei werden dir sowohl 
Lautstärken angezeigt als auch die BPM ("Beats per Minute"). So kannst du die 
Schnelligkeit und Taktrate deiner Stücke einschätzen und automatisch 
synchronisieren lassen. Mit Schiebereglern und Drehknöpfen stellst du die 
Überblendung deiner Tracks ein, die Hoch- oder Tiefpassfilter und die 
Lautstärke. Zusätzlich gibt es auch Knöpfe für Samples und Effekte, die du frei 
belegen kannst.

Ein weiteres Highlight ist natürlich, was du dich mit keiner deiner teuren 
Schallplatten traust: scratchen!
Einfach mit den Scratch-Knöpfen rumwackeln oder per Mausklick die Tonspuren 
durch die Gegend wischen und Scrrraaattchwhwhwhwwbwbwb!

!!! **Tipp:**  
!!! *Du kannst natürlich alles per Maus oder Tastatur steuern - aber komfortabler ist es natürlich mit einem richtigen DJ-Pult: über MIDI-Schnittstellen kannst du dann die Knöpfe und Funktionen in Mixxx mit deinem Mischpult, Keyboard oder anderen Geräten verbinden und steuern.*

---

!!!! <center><a href=https://mixxx.org/>https://mixxx.org/</a></center>

## Hilfe

- Offizielles Handbuch (Sprache: DE / Multilingual):
  * <https://mixxx.org/manual/latest/de/>
- Offizielles Wiki von Mixxx (Sprache: EN):
  * <https://github.com/mixxxdj/mixxx/wiki>

- Video-Tutorials:
  - *"Mixxx Tutorial - Beginner's Guide"* (*CurveRadio*; YouTube)
    * <https://www.youtube.com/watch?v=LykvuXuq0iA>
  - *"Mixxx is an incredible FOSS DJ-ing program"* (*unfa*; YouTube)
    * <https://www.youtube.com/watch?v=rt5Ed5GZ1U8>
  - *"Broadcast live on your Radio Station with Mixxx (Mac, Windows & Linux)"* (*RadioKing*; YouTube)
    * <https://www.youtube.com/watch?v=YbK2mw16QQ4>
