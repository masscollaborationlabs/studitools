---
title: 'Audacity - Audio aufnehmen und schneiden'
date: '29-08-2020'
publish_date: '29-08-2020'
taxonomy:
    category:
        - Audio
        - Video
        - Multimedia
---

![](img/audacity.png)

===

# Audio aufnehmen und schneiden mit Audacity

- Du willst ein Interview oder einen Podcast aufnehmen?
- Diese eine Stelle aus deinem Lieblings-Set möchtest du als einzelnes Lied speichern?
- Du musst eine einzelne Tonspur für ein Video anfertigen? 

Dafür gibt es Audacity - eine freie Software um Audio aufzunehmen und zu schneiden.

Mit Audacity könntest du z.B. ein Mikrofon zur Aufnahme eines Interviews nutzen oder die Ton-Ausgabe deines Browsers oder Videoplayers mitschneiden. 
Natürlich kannst du auch die Lieblingsstelle aus diesem einen Lied ausschneiden, damit du es für immer auf Repeat hören kannst…

Anschließend lassen sich alle Tonspuren frei anordnen und mit Effekten oder Übergängen versehen. 
Mit "Normalisieren" kannst du z.B. zu laute oder zu leise Aufnahmen nachträglich korrigieren. 
Außerdem kannst du auch die Aufnahmen verbessern, indem du z.B. Rauschen aus deiner Aufnahme herausrechnest.

!!! **Tipp:**  
!!! *Wenn du höhere Ansprüche hast noch mehr Möglichkeiten für Audio-Bearbeitung, Produktion und Mixing sucht, solltest du vielleicht lieber [Ardour](../ardour/) verwenden.*

Audacity unterstützt viele Dateiformate - von AAC bis OGG. 
So hast du immer das nötige Dateiformat griffbereit und kannst auch Dateien in verschiedene Formate umwandeln. 
Audacity ist für alle Betriebssysteme frei erhältlich.

!!! **Tipp:**  
!!! *Wenn du einen guten Audio- / Video-Player suchst, schau dir mal VLC an.  
!!! Mit diesem Allround-Talent lassen sich Dateien nicht nur abspielen, sondern auch ganz einfach Audio- und sogar Video-Aufnahmen erstellen und in vielen Formaten speichern.*

---

!!!! https://www.audacityteam.org/

## Hilfe

- FAQ:
  * <https://www.audacityteam.org/help/faq/>
- Dokumentation:
  * <https://www.audacityteam.org/help/documentation/>
- Tutorials:
  * <https://manual.audacityteam.org/#tutorials>
- "Getting Started":
  * <https://manual.audacityteam.org/quick_help.html>

- Tutorial: "Einstieg in die Audio-Produktion mit der kostenlosen Software Audacity"
  * <https://www.podcampus.de/nodes/RrBEm>
