---
title: 'OpenShot – Filme schneiden leicht gemacht'
date: '29-08-2020'
publish_date: '29-08-2020'
taxonomy:
    category:
        - Audio
        - Video
        - Multimedia
---

![](img/openshot.png)

===

# OpenShot - Filme schneiden leicht gemacht

* Mal schnell ein Video schneiden?
* Dein Video muss in ein anderes Format umgewandelt werden?
* [Shotcut](../shotcut/) ist dir… irgendwie zu viel?

Wie wäre es mit OpenShot? 

OpenShot ist ein einfaches aber robustes Videoschnitt-Programm, mit dem du Video- und Audio-Dateien schneiden, mit Effekten versehen und umwandeln kannst. Dafür hast du verschiedene Spuren zur Verfügung, die du mit Übergängen und Effekten verknüpfen kannst. Außerdem gibt es Werkzeuge mit denen du z.B. Logos, Banner und Formen über deine Videos legen kannst. Audiospuren, Bilder und Videos lassen sich dabei beliebig mischen.

Fertige Videos kannst du ganz einfach mit Voreinstellungen direkt für gängige Videoplattformen exportieren - oder doch jedes Detail selbst einstellen. So musst du dich aber nicht um die technischen Details kümmern, sondern kannst dich auch ganz auf den Videoschnitt konzentrieren.

OpenShot ist freie Software und steht daher für alle Betriebssysteme kostenlos zur Verfügung.

---

!!!! <center><a href=https://www.openshot.org/>https://www.openshot.org/</a></center>

## Hilfe

- Handbuch / Dokumentation von OpenShot:
  * <https://www.openshot.org/user-guide/>
- offizielle Video-Tutorials:
  - <https://www.youtube.com/c/JonathanThomasOSS/videos>

- Tutorial: "Einen Film mit OpenShot erstellen" (YouTube; "Tutonaut")
  * <https://www.youtube.com/watch?v=74pIsM1vBZ8>
