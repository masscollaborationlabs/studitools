---
title: VDO.ninja
date: '05-07-2021 21:52'
taxonomy:
    category:
        - Audio
        - Video
        - Multimedia
        - Quick-Tip
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

===

# VDO.ninja


* Oh nein, die Webcam ist Kaputt - wie sollst du jetzt in die Konferenz?
* Gemeinsam einen Video-Podcast aufnehmen… okay. Aber dafür brauchen doch alle  entsprechendes Equipment!?

Kopf hoch! Klar kommst du in die Konferenz - und den Podcast - du hast doch ein 
Smartphone!?

Schau dir mal [OBS Studio](obs_studio) an. Damit hast du ja schon ein Studio, mit dem du 
Aufnahmen anfertigen und streamen kannst.

Was du jetzt noch brauchst ist: VDO.ninja!

VDO.ninja ist einfach eine Webseite. Dort gibst du deine Kamera oder das 
Mikrofon eines Geräts frei. Diese lassen sich dann in OBS Studio als Quelle 
einfügen. Damit kannst du also z.B. deine Smartphone-Kamera in OBS Studio z.B. 
Webcam nutzen und Streamen.
Du könntest damit auch beides als "virtuelle Kamera" mit OBS Studio für eine 
Videokonferenz nutzen - sogar mit Greenscreen und Effekten.

**Hinweis:**
*VDO.ninja basiert auf dem offenen Standard "WebRTC" ("Web Realtime Communication").
WebRTC ist ein gemeinsamer Standard für Messenging im Browser. 
Dabei ist es egal ob es Audio-, Video- und Textnachrichten sind. WebRTC wird 
von allen gängigen Browsern unterstützt. Alle Daten werden außerdem nur direkt 
zwischen den Geräten und Browsern übertragen. Anmelden oder Registrieren musst 
du dich daher nirgendwo.
Auch Videokonferenz-Software wie [Jitsi-Meet]() basiert beispielsweise auf 
WebRTC und benötigt daher keine zusätzliche Software auf deinem Gerät.*

## deine Smartphone-Kamera als Kamera für OBS Studio nutzen

Hier ein kleines Rezept, um deine Handy-Kamera und Mikrofon mit VDO.ninja in 
OBS Studio einzufügen:

1. stelle sicher, dass du `Browser` als Quelle in OBS Studio eingefügen 
   kannst
2. gehe mit dem Browser im Smartphone auf `https://vdo.ninja`
3. klicke auf `Add you Camera to OBS`
4. Stelle alles so ein, wie du es brauchst (Audio / Video, Qualität des Videos 
   etc.). Hier musst du außerdem möglicherweise deinem Browser Berechtigungen 
   für Audio- und Video-Aufnahmen geben.
5. setze ein Passwort, damit niemand fremdes dein Video als Quelle nutzen kann
6. starte die Video-Freigabe mit eine Klick auf `Start`

Über dem Videos erscheint nun ein Link. Diesen musst du in OBS Studio einfügen. 

Erstelle in OBS Studio eine neue Quelle mit dem Typ `Browser`.
Dort gibst du einfach die URL an (Komplett, mit `https://` und allem) und das 
(optionale) Passwort.

Fertig!
Deine Handy-Kamera ist jetzt in OBS-Studio als Videobild verfügbar - als 
Webcam-Ersatz für Live-Streaming, Konferenzen, Gespräche oder zusätzliche 
Videokamera!

**Hinweis:**
*VDO.ninja kann noch jede Menge mehr. So könntest du z.B. auch mehrere Menschen 
so mit ihren Handy-Kameras in deinem "Studio" versammeln, um z.B. einen 
Video-Podcast aufzunehmen, gemeinsam Filme zu sehen, eine Art Video-Konferenz 
zu veranstalten oder gemeinsam jede Art von Webseite oder Screen-Sharing von 
unterschiedlichen Geräten anzusehen.* 

!!!! <center><a href=https://vdo.ninja>https://vdo.ninja</a></center>

## Hilfe

- VDO.ninja-Guides (Sprache: EN):
  * <https://docs.vdo.ninja/>
