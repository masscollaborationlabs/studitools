---
title: 'OBS Studio – dein Studio für Live-Streaming und Videoaufnahmen'
date: '04-07-2021 00:00'
publish_date: '07-04-2021 00:00'
taxonomy:
    category:
        - Audio
        - Video
        - Multimedia
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

![](img/obs_2.png)

===

# OBS Studio - dein Studio für Live-Streaming und Videoaufnahmen

Ob Live-Streaming oder Tutorial-Video - irgendwie musst du das ja aufnehmen. 
Mit dem freien *Open Broadcaster Software Studio* ("OBS Studio") kannst du so ziemlich alles aufnehmen und zusammenschneiden, was du willst. 
Die Software ist kostenlos und funktioniert unter allen gängigen Betriebssystemen. 
Sie wird standardmäßig bei den größten Videoplattformen von Streamer*innen eingesetzt.

! **Hinweis:**  
! _Du kannst auswählen, ob du OBS Studio zur Aufnahme eines Videos nutzen willst oder zum Live-Streaming. 
! Fürs Streamen benötigst du einen entsprechenden Streaming-Code für die Plattform bzw. den Server, auf dem du deinen Live-Stream zeigen willst._

Für die Aufnahme kannst du unterschiedliche Geräte, Aufnahmen und Medien als Quellen in unterschiedlichen Szenen bündeln. 
Im Studio-Modus kannst du dann schnell zwischen den verschiedenen Szenen wechseln - etwa zwischen dem gezeigten Programm und deiner Webcam. 
Als Quellen kannst du dabei auch Bilder, verschiedene Mikrofone, Bildschirme oder einzelne Fenster wählen. 
Außerdem lassen sich alle Quellen auch mit Filtern bearbeiten, damit z.B. dein Webcam-Bild etwas schärfer angezeigt wird.

!!! **Tip:**  
!!! *Oftmals ist in Aufnahmen die Kamera nicht so sehr das Problem - sondern die Tonqualität.  
!!! Sieh in den Einstellungen des Audio-Mixers nach Filtern für z.B. die Rauschunterdrückung oder einen Filter namens "Gate" nach. 
!!! Damit stellst du einen Schwellwert für die Lautstärke ein:  
!!! Das (leise) Rauschen deines Mikrofons und das Echo in deinem Zimmer wird dann herausgefiltert und nur deine (lautere) Stimme kommt bei der Aufnahme durch.*

Du kannst sogar virtuelle Hintergründe mit einem Greenscreen (oder anderen Farben) einstellen. 
Die Aufnahme aus OBS Studio lässt sich auch als "virtuelle Kamera" umleiten, so dass du die Ausgabe direkt in Videokonferenzen nutzen kannst.

Falls du mal nicht weiterkommst und spezielle Fragen zu OBS Studio hast, stehen dir extrem viele Tutorial-Videos und Beiträge zur Verfügung. 
OBS Studio ist schließlich wirklich *das* Programm für Live-Streaming und Videoaufnahmen am PC.

! **Hinweis:**  
! *Wenn du später Videos schneiden willst, dann schau dir mal [OpenShot](../openshot) oder [Shotcut](../shotcut) an.
! Wenn du stattdessen den Ton nachträglich verbessern willst, kann dir z.B. [Ardour](../ardour/) helfen.*

---

!!!! <center><a href=https://obsproject.com/de>https://obsproject.com/de</a></center>

## Hilfe

- Offizielle Hilfe-Seite von OBS Studio
  * <https://obsproject.com/de/help>

### Video-Tutorials

- Video-Kurs: "OBS Studio Komplettkurs 2020" von Nilson1489_TV (YouTube; Sprache: DE)
  * <https://www.youtube.com/watch?v=X1sCnkTX2TQ&list=PLXNUbE8XKh9pojt1OIGQHulB0APGYZAee>
- *BitBastelei #420b Greenscreen und Einblendungen in Videokonferenzen: Virtuelle Webcam mit OBS* von BitBastelei (YouTube; Sprache: DE)
  * <https://www.youtube.com/watch?v=P3TkxdRTZc0>
