---
title: 'Instant-Messaging mit XMMP („Jabber")'
date: '01-04-2020'
publish_date: '01-04-2020'
taxonomy:
    category:
        - Kommunizieren
        - Koordinieren
---

![](img/gajim_CC-BY-SA_4.0_Gajim_Developers.png)

===

# Instant-Messaging mit XMMP („Jabber")

Dein Account-Name der Uni Bremen ist nicht nur deine E-Mail-Adresse, sondern 
gleichzeitig ein Account für den den Chat-Dienst XMPP („Jabber"). Mit XMPP 
kannst du beliebige Einzel- oder Gruppenchats erstellen, die auch 
Passwortgeschützt sein können.

Das XMPP-Protokoll ist ein offener Standard, für den es auf allen 
Betriebssystemen Programme gibt - sowohl für den Desktop-Rechner, als auch für 
Mobilgeräte (auch WhatsApp und andere Messenger nutzen eigene Versionen von 
XMPP).

XMPP funktioniert ähnlich wie E-Mail (dezentral):

* Du und andere haben die freie Auswahl, auf welchem Server du dein Konto 
  einrichten willst (hier: die Uni Bremen).
* Es gibt viele unterschiedliche Programme (Clients). Damit kannst du mehrere 
  Accounts gleichzeitig nutzen - z.B. in der Uni und für dich privat.
* Du kannst alle Nutzer\*innen anderer Server ebenfalls anschreiben und musst 
  dich nicht nur auf deine Kontakte an der Uni beschränken.
* die (optionale) starke Verschlüsselung hält vertrauliche Gespräche auch 
  wirklich vertraulich.

XMPP bietet sich also besonders an, wenn du mit Personen an anderen Unis 
schreiben möchtest - oder als Alternative zu anderen Messengern.

!! **Achtung** 
!! *Wenn du jemanden an der Uni über XMPP anschreibst, solltest du die Person auch 
!! darüber informieren, dass du es nutzen möchtest. Es gibt keine automatische 
!! Weiterleitung oder Benachrichtigung über XMPP-Chats!*

---

!!!! <center><a href=https://jabber.uni-bremen.de/start.html>https://jabber.uni-bremen.de/start.html</a></center>

## Hilfe

* Web-Client des ZfN für XMPP:
  * <https://jabber.uni-bremen.de/start.html>
* Informationen des ZfN zum XMPP-Dienst:
  * <https://www.uni-bremen.de/zfn/weitere-it-dienste/chat-konferenzsysteme/>
* Mobil-Client "Conversations":
  + <https://conversations.im/>
* Eine Liste von möglichen Client-Programmen für ihre Geräte gibt es auf folgender Seite:
  * <https://xmpp.org/software/clients.html>


