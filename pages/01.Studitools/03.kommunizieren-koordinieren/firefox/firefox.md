---
title: 'Firefox - ein gemeinnütziger Browser'
date: '07-09-2021'
publish_date: '07-09-2021'
taxonomy:
    category:
        - Kommunizieren
        - Koordinieren
---

![](img/firefox.png)

===

# Firefox - ein gemeinnütziger Browser

* Warst du schonmal Surfen?
* Surfst du nackt?
* Suchst du vielleicht Sonnenbrillen oder Potenzpillen? 

> *"Äh, sorry… falls das jetzt irgendwie zu persönlich für dich war, aber wir 
> sind hier schließlich im Internet!"*

So ähnlich kannst du dir heute leider "das Internet" vorstellen:  
der Strand kostet Eintritt oder du brauchst einen Mitgliedsausweis. Einzige 
Alternative: der Daten-FKK-Bereich. Da kannst du zwar so hin, darfst aber 
nichtmal ein Handtuch haben. Außerdem brüllen dir windige Geschäftsmenschen 
ständig per Megafon Angebote entgegen und verteilen eklig schmeckende Kekse, die 
du AUF JEDEN FALL(!!!11) essen musst… \*_würg_\*!  
Ein "dickes Fell" reicht da längst nicht mehr.

> Oh dog! Das klingt ja ätzend! "Einfach am Strand chillen" haben sie gesagt… 
> von wegen!

Von Firefox hast du bestimmt schon einmal gehört. Firefox ist ein Webbrowser, 
mit dessen Nutzung du jedes Mal mitentscheidest, wie der Interstrand mit 
Datensand sein soll, wenn du auf den www.wellen.surfen willst.  
Ja, äh… du ahnst, was gemeint ist…

Firefox wird nämlich von der Mozilla Foundation entwickelt - einer
gemeinnützigen und nicht gewinnorientierten Stiftung, die sich auch durch 
Spenden finanziert (deine zum Beispiel). Das ist ein ziemlicher Gegensatz zu 
Browsern von gewinnorientierten Firmen, die ihr Geld mit Internet-Werbung 
verdienen. Daraus ergibt sich der größte Unterschied zu anderen Browsern: denn 
wer Geld mit Werbung verdient, wird kaum einen privatsphärefreundlichen Browser 
bauen, der diese kommerzielle Überwachung verhindert. Das Entwicklungsmodell von 
Software hat am Ende einen enormen Einfluss auf deine eigenen Möglichkeiten und 
Spielräume - und in wessen Interesse Entscheidungen getroffen und akzeptiert 
werden.  
Die Zielgruppe bei Firefox sind _Nutzer*innen_ im Internet - *nicht* 
_Kund*innen_ anderer Firmen, von denen Provisionen locken.

Daher setzt Firefox vor allem auf Funktionen für Privatsphäre und Datenschutz. 
Aber es geht nicht nur einfach darum, Geld zu verdienen, sondern um 
*Mitgestaltung*. Und da kannst auch du eine Menge mitbewegen - sogar ohne 
Technik-Kenntnisse oder einer enormen Kaufkraft.

! **Hinweis:**  
! *Die Mozilla Foundation entwickelt auch viele weitere Projekte, z.B. [Common 
! Voice](https://commonvoice.mozilla.org/de), bei dem eine 
! Spracherkennungs-Software entsteht. Und bei diesen Projekten kannst du 
! eigentlich immer helfen - z.B. indem du programmierst, Fehler meldest und 
! andere Rückmeldungen gibst.  
! Bei Mozillas [Common Voice](https://commonvoice.mozilla.org/de) kannst du z.B. 
! Sätze vorlesen oder bewerten, um damit eine frei verfügbare Spracherkennung 
! mitzuentwickeln.  
! Alternativen zu digitalen Assistenten gewinnorientierter Firmen sind nämlich 
! bitter nötig - oder gute, kostenlose Screenreader (Stichwort: Barrierefreiheit 
! und Inklusion)!*

Firefox wird außerdem als freie Software von vielen Menschen auf aller Welt 
entwickelt. Sie programmieren auch Erweiterungen, die Firefox für viele Zwecke 
noch besser anpassen. Und auch hier stehen oft die Interessen und die 
Privatsphäre der Nutzer*innen im Vordergrund - damit *alle* etwas vom Stran… 
äh, vom Internet haben.

Firefox gibt es außerdem für jedes Betriebssystem. Egal ob Linux, Windows, 
MacOS, iOS, Android, BSD, UNIX… es gibt ihn.  
Und ob du im Anzug, Shorts, Bikini, Burkini, mit Flip-Flops oder Regenstiefeln 
an den Strand und Surfen willst, ohne belästigt zu werden, möchtest du ja auch 
selbst entscheiden, nicht wahr?

---

!!!! <center><a href=https://www.mozilla.org/de/firefox/>https://www.mozilla.org/de/firefox/</a></center>

## Hilfe

- Support-Seite für Mozilla Firefox (Sprache: DE):
  * <https://support.mozilla.org/de/products/firefox>
- Webseite der Mozilla Foundation:
  * <https://foundation.mozilla.org/de/>
