---
title: 'DFN-Terminplaner - Umfragen und Abstimmungen'
date: '12-03-2020'
publish_date: '12-03-2020'
taxonomy:
    category:
        - Kommunizieren
        - Koordinieren
---

![](img/terminplaner_dfn_webseite.png)

===

# DFN-Terminplaner - Umfragen und Abstimmungen

* Einfach kurz eine Umfrage erstellen?
* Klar, das geht geht doch bei D… dem Deutschen Forschungsnetz (DFN)!

Mit dem _Terminplaner_ kannst du Umfragen oder Termin-Abfragen erstellen und anderen Personen per Link schicken.
Nach der Erstellung einer Umfrage hast du noch mehr Möglichkeiten: die Sichtbarkeit der Ergebnisse könnten z.B. geheim bleiben, oder du erhältst eine E-Mail-Mitteilungen sobald jemand abgestimmt hat. Außerdem kann eine Abstimmung zu einem bestimmten Datum enden.

Das beste daran? Es funktioniert _und_ ist Privatsphäre-freundlich, ohne Werbung - nicht wie D… das andere.

---

!!!! <center><a href=https://terminplaner6.dfn.de>https://terminplaner6.dfn.de</a></center>
