---
title: 'Jitsi-Meet - planen, plaudern, präsentieren'
date: '04-06-2020'
publish_date: '04-06-2020'
taxonomy:
    category:
        - Kommunizieren
        - Koordinieren
---

![](img/jitsi.png)

===

# Jitsi-Meet - planen, plaudern, präsentieren

- Einfach mal wieder deine Freunde aus der Uni sehen?
- Kurz vor Knapp noch einmal die Präsentation gemeinsam durchgehen?
- Mit der Projektgruppe aus der anderen Uni treffen?

Jitsi-Meet vom ZfN ist das Videokonferenz-Tool, mit dem du in kleinen bis 
mittelgroßen Gruppen einfach drauflos plaudern kannst.
Mehr als deinen normalen Uni-Login und einen Browser brauchst du dafür nicht.

!!! **Hinweis:**  
!!! *Es gibt auch eine Jitsi-Meet App für iOS und Android. Die Android-Version ist 
!!! auch im freien App-Store [F-Droid](https://f-droid.org/de/) zu finden.*

Jitsi-Meet ist eine Open-Source-Software und wird von vielen Organisationen 
(teilweise ebenfalls öffentlich und kostenlos) eingesetzt. Auch wenn du es 
privat verwenden willst, gibt es daher viele kostenlose Anbieter für 
Videokonferenzen. 

Mit Jitsi-Meet stehen dir die gängigen Funktionen für Audio- und 
Videokonferenzen, wie etwa ein Chat oder Bildschirmfreigabe zur Verfügung. Die 
Bildschirmfreigabe lässt sich auch auf ein bestimmtes Fenster beschränken. 
Außerdem kannst du YouTube-Videos direkt in der Konferenz zeigen. So könnt ihr 
euch in einer Gruppe nicht nur Dokumente, Präsentationen oder ein anderes 
Programm zeigen, sondern auch z.B. Videos besprechen. Über den Chat könnt ihr 
auch private Nachrichten direkt an eine einzelne Person aus der Gruppe 
schicken.  
Und falls dein Zimmer mal wieder zu unordentlich ist oder ihr mehr Privatsphäre 
für euer Treffen braucht: den Hintergrund kannst du einfach auf "unscharf" 
stellen und die Konferenz lässt sich mit einem Passwort schützen.

**Treffen über Jitsi-Meet beim ZfN einrichten:**

1. auf die Jitsi-Meet Seite des ZfN gehen
2. einen Namen für euer Meeting eingeben (der Namensgenerator kann dir dabei 
   helfen)
3. wenn ihr die erste Person im Raum seid ("ich bin Organisator*in"), müsst ihr 
   eure Uni-Account-Daten angeben (Name und Passwort wie bei Stud.IP)
4. wenn nötig: ein Passwort vergeben 
5. *fertig*

!!! **Tipp:**  
!!! *Für Gruppenarbeiten und Protokolle, die ihr gemeinsam schreiben müsst, könnt 
!!! ihr in einem zweiten Browser-Fenster die Stud.IPads in Stud.IP verwenden oder 
!!! [OnlyOffice in 
!!! Seafile](https://blogs.uni-bremen.de/studytools/2020/07/16/online-bearbeitung-von-dokumenten-in-seafile/).*

---

!!!! <center><a href=https://jitsi.zfn.uni-bremen.de/>https://jitsi.zfn.uni-bremen.de/</a></center>

! **Hinweis:**  
! *Videokonferenzen funktionieren oft unterschiedlich gut. Das kann an vielem 
! liegen: Internetverbindung, Router, Browser-Version, Betriebssystem, der 
! Konferenz-Software…   
! Außerdem ändert sich das ständig, da die Software-Entwicklung sehr schnell 
! geht. Möglicherweise funktioniert daher eine andere Software in eurer Gruppe 
! besser, ist angemessener für euren Zweck oder die Gruppengröße.*  

!! *Achte immer darauf, dass du die aktuellste Version deines Browsers 
!! installiert hast.*

- ZMML Info-Portal zu digitaler Lehre:
  - <https://www.uni-bremen.de/zmml/lehre-digital/digitale-werkzeuge/kommunikation-und-information>

## Hilfe

- Informationen zu Jitsi vom ZfN:
  - <https://www.uni-bremen.de/zfn/weitere-it-dienste/chat-konferenzsysteme/jitsi-meet-videokonferenzen>
- Die vom ZfN empfohlenen Browser sind *[Mozilla 
  Firefox](https://www.mozilla.org/de/firefox/new/)* und 
  *[Chromium](https://www.chromium.org/getting-involved/download-chromium)*-basierte 
  Browser. Du musst also nicht Google Chrome benutzen, wenn du lieber einen 
  anderen Browser zum Schutz deiner Privatsphäre benutzen willst. 
