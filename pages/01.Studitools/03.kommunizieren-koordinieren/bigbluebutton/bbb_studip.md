---
title: 'Meetings in Studiengruppen mit BigBlueButton'
date: '04-06-2020'
publish_date: '04-06-2020'
taxonomy:
    category:
        - Kommunizieren
        - Koordinieren
---

![](img/csm_DW-Anleitung-BBB.png)

===

# Meetings in Studiengruppen mit BigBlueButton

Konferenzen (Video / Audio) kannst du auch selbst in Studiengruppen einrichten:  
unter "Meetings" aktivierst du *BigBlueButton*, das auch in Seminaren genutzt werden kann.

Mit Stud.IP-Meetings könnt ihr euch Präsentationen oder euren Bildschirm zeigen, gemeinsam Texte schreiben ("geteilte Notizen", oder besser noch: mit Stud.IPads) oder auf einem Whiteboard gemeinsam zeichnen.  
Unter-Räume für die Arbeit in Kleingruppen ("Break-Out-Rooms") gibt es ebenfalls.

Das Plug-In "Meetings" musst du für eine Studiengruppe aktivieren:

1. entweder beim Erstellen der Gruppe
2. nachträglich in den Optionen der Gruppe

Sobald du das "Meetings"-Plug-In für die Studiengruppe aktiviert hast, kannst du im entsprechenden Reiter der Gruppe ein Meeting eröffnen. 
Je nach Einstellung ist das Meeting entweder *immer sichtbar* oder nur, *wenn du es für andere sichtbar schaltest*. Verändern lässt sich das mit dem kleinen Zahnrad-Icon bei "Meetings".

! **Anmerkung:**  
! *Für den Fall, dass ihr mit Menschen außerhalb der Uni zusammen eine Konferenz nutzen wollt: Ihr könnt in den Einstellungen für ein Meeting einen Link erstellen, mit dem ihr Gäste zu der Konferenz einladen könnt.  
! Eine weitere Möglichkeit bietet ist [BigBlueButton auf der Webseite des ZfN](https://www.uni-bremen.de/zfn/weitere-it-dienste/chat-konferenzsysteme/bigbluebutton-videokonferenzen). Dort muss nur der / die Organisatorin eines Meetings einen Uni-Account haben, alle anderen können auch ohne Uni-Account teilnehmen. Auch hier muss der / die Organisator *in einen Link an Gäste verschicken.*

---

Ok, dann [bring mich zu Stud.IP](https://elearning.uni-bremen.de)!

! **Anmerkung zu Videokonferenzen:**  
! *Videokonferenzen funktionieren generell unterschiedlich gut. Das kann an vielem liegen: Internetverbindung, Router, Browser-Version, Betriebssystem, der Konferenz-Software…   
! Außerdem ändert sich das ständig, da die Software-Entwicklung sehr schnell geht. Möglicherweise funktioniert daher eine andere Software in eurer Gruppe besser, ist angemessener für euren Zweck oder die Gruppengröße.*  

!! *Achte immer darauf, dass du die aktuellste Version deines Browsers installiert hast und überprüfe die Einstellungen zu Raumgröße, Übertragungs-Qualität und Webcams.*

- ZMML Info-Portal zu digitaler Lehre:
  - <https://www.uni-bremen.de/zmml/lehre-digital/digitale-werkzeuge/kommunikation-und-information>

## Hilfe

- Informationen des ZMML, um BigBlueButton einzurichten und zu nutzen:
  - <https://www.uni-bremen.de/zmml/lehre-digital/digitale-werkzeuge/videokonferenzen-mit-bigbluebutton>
- Informationen zu BigBlueButton vom ZfN:
  - <https://www.uni-bremen.de/zfn/weitere-it-dienste/chat-konferenzsysteme/bigbluebutton-videokonferenzen> 
- Dokumentation und Handbuch zu BigBlueButton:
  - <https://docs.bigbluebutton.org/>
- offizielle Video-Tutorials für BigBlueButton:
  - <https://bigbluebutton.org/html5/>

