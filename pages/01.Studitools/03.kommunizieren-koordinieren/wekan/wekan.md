---
title: 'Wekan - ein Kanban für deine Projekte'
date: '13-07-2021'
publish_date: '13-07-2021'
taxonomy:
    category:
        - Kommunizieren
        - Koordinieren
---

![](img/wekan.png)

===

# Wekan - ein Kanban für deine Projekte

* Aaaaah! So viele Aufgaben und kein Überblick?
* Wer aus der Projektgruppe sollte eigentlich nochmal dieses eine Dings 
  fertigmachen?
* Hast du heute schon deine Pflanzen auf dem Balkon gegossen?

Wekan ist ein digitales Kanban. Du ka… was? Nein, das hat nichts mit Pokemons 
zu tun.

## Was ist Kanban?

Ein Kanban ist sowas wie… Post-its. Nur besser. Next-Level-Post-its sozusagen!

Kanban-Systeme sind Methoden zur Produktionssteuerung bzw. Projektsteuerung und 
kommen ursprünglich aus Japan. Auf kleinen Karteikarten werden dabei Aufgaben 
und Zustände notiert z.B. der Lagerbestand von Produkten und Bauteilen. Jedes 
Bauteil und jedes Produkt bekommen eine Karte. Damit ist immer ersichtlich, 
wenn ein Teil fehlt (weil es verarbeitet wurde) oder wie viele Produkte gerade 
einen Produktionsprozess durchlaufen oder verlassen haben. Damit lassen sich 
also Input- und Output-Schleifen aufeinander abstimmen: es ist nämlich sofort 
klar, wie die Auslastung ist, wann Teile fehlen oder wann Teile neu angeschafft 
werden müssen.  
Aber das kannst du auch alles bei Wikipedia und Fachbüchern nachlesen…

Weil Kanban-Systeme vielseitig und leicht umzusetzen sind, haben sie sich z.B. 
auch in der Softwareentwicklung und im Projektmanagement etabliert - und als 
privates Tool für alle Aufgaben.

Im Prinzip ist das bestechend einfach: stell dir eine Tabelle mit Spalten vor, 
wie beispielsweise "Todo", "in Arbeit" und "Fertig". Jede Aufgabe schreibst du 
auf einen Post-it und klebst sie in die entsprechende Spalte (je nachdem, wie 
gerade der Stand bei der Aufgabe ist).  
Ergebnis: du hast immer einen Überblick darüber, *wie viel* du eigentlich tust, 
*was dein Stand ist* bei Aufgaben oder Projekten ist und (wenn du in einem Team 
arbeitest) *wer gerade was tut*.  
Wenn du jetzt noch ein Maximum gleichzeitiger Dinge in deiner "Todo"-Spalte 
festlegst… voila! Schon hast du weniger Stress!

## Wekan

Wekan ist so ein Kanban - nur eben digital. Außerdem ist es natürlich freie 
Software - und kann wahnsinnig viel!

Jedes Projekt bekommt in Wekan ein eigenes "Board", das du öffentlich oder 
privat schalten kannst (je nachdem, ob du es im Team oder für dich allein 
nutzt).  
In einem Board gibt es dann die Listen - also etwas wie "Idee", "Todo", "in 
Arbeit", "Fertig"… und in diesen Listen legst du deine Aufgaben-"Karten" an.

Karten kannst du dann einer Person aus dem Team zuordnen und auch Start- oder 
Fälligkeitsdatum angeben. Außerdem kannst du sie verschlagworten und farbig 
markieren oder kommentieren, um etwa den Prozess bei der Aufgabe festzuhalten. 
Außerdem lassen sich Dateien zu einer Aufgabe hochladen. Für Routineaufgaben 
legst du Checklisten an, die du genüsslich abhaken darfst, wenn du sie 
"abgefrühstückt" hast. Mjam!

! **Hinweis:**  
! *Für solche Checklisten kannst du Vorlagen verwenden. Wenn du regelmäßig die 
! gleichen Schritte auf einer Karte aufschreibst, solltest du unbedingt eine 
! Vorlage dafür erstellen!*

Natürlich kannst du auch alle Aufgaben filtern und sogar exportieren.  
Mit einem Klick auf deinen Avatar aktivierst du Standard-Filter wie "zeige nur 
meine Karten an" oder "zeige fällige Karten an" (natürlich nur, wenn du Karten 
Personen zugewiesen oder ein Datum hinterlegt hast).

Wenn dein Projekt z.B. mehrere Phasen oder Teilbereiche hat, solltest du 
"Swimlanes" nutzen. Stell dir "Swimlanes" als Zeile vor, die alle deine Spalten 
umfasst:  
alle bei euch im Team haben jeweils einen Bereich, in dem sie jede "Idee" 
festhalten, ihre "Todo"-Aufgaben aufschreiben, mitteilen, dass die Aufgabe "in 
Arbeit" ist oder sogar "fertig". In Swimlanes könntest du z.B. auch die 
Orga-Aufgaben deiner Abschlussarbeit von deinen eigentlichen Meilensteinen beim 
Schreiben getrennt festhalten. 

|                     | **Idee**   | **Todo**  | **in Arbeit** | **Fertig!** |
|---------------------|------------|-----------|---------------|-------------|
| "Swimlane" Person 1 | fixe Idee  | Work      | chillen       |             |
| "Swimlane" Person 2 | mehr Ideen | mehr Work | chillivanilli |             |

Mit den unterschiedlichen Ansichten bekommst du einen Überblick über dein 
Projekt:

- mit `Liste` siehst du nur noch *eine* Tabelle mit *allen* Aufgaben
- unter `Swimlane` siehst du die Listen unterteilt in die einzelnen Bereiche

!!! **Tip:**  
!!! *Es gibt auch eine Kalender- oder Terminübersicht, in dem du Zeit und Datum 
!!! deiner Aufgaben in einem Kalender oder einer Liste siehst. Dort hast du dann 
!!! auch deine Daueraufgaben, Startzeiten und Deadlines im Blick.*

Und schwupps - siehst du sofort, wer wie weit mit welcher Aufgabe ist oder ob 
es in einem Bereich noch wahnsinnig viel zu tun gibt.  
Und vor allem siehst du, wie viel schon geschafft ist! Wow… Cheers!

!!! **Fun-Fact:**  
!!! *Alle Artikel für Studytools werden mit so einem Kanban 
!!! ([Taskell](https://taskell.app/)) geplant und in einzelnen Schritten 
!!! festgehalten. Läuft!*

---

!!!! <center><a href=https://onlinetools.zfn.uni-bremen.de>https://onlinetools.zfn.uni-bremen.de</a></center>

## Hilfe

- Offizielle Hilfe zu Wekan (Sprache: EN)
  * <https://github.com/wekan/wekan/wiki>
- Hilfe zu Wekan der Hochschule für Wirtschaft und Recht Berlin 
  * <https://www.it.hwr-berlin.de/anleitungen/kanban-board-wekan/>
- weiterführende Artikel zu Kanban-Systemen bei "die Computermaler":
  * <https://die-computermaler.de/page/1/?s=kanban>
- Video: "Wie "Personal Kanban" hilft, sich selbst zu managen und Stress zu vermeiden" (*Projekt Akademie Bonn*; YouTube)
  * <https://www.youtube.com/watch?v=8XEDnnzbvIM>
- Video "Wie funktioniert Software-Kanban?" (*it-agile*; YouTube)
  * <https://www.youtube.com/watch?v=ndWPFk7GR8k>
- Video "Quick Look - Wekan for Project Management" (*Derek Caelin*; YouTube; Sprache: EN)
  * <https://www.youtube.com/watch?v=X3ejVUgR6TU>
