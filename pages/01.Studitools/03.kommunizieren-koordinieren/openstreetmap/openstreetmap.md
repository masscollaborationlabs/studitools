---
title: 'OpenStreetMap - freie Weltkarte für jeden Zweck'
---

===

# OpenStreetMap - freie Weltkarte für jeden Zweck

Die OpenStreetMap ("OSM") ist ein Gemeinschaftsprojekt, dass seit 2004 eine 
Weltkarte erstellt - Stück für Stück mit viel ehrenamtlicher Handarbeit. 
Unterstützt wird das Projekt aber auch von teilweise kommerziellen Diensten.

## Hä? Warum machen sich Leute den diese Arbeit!? Es gibt doch schon Kartendienste…!?

Naja, OpenStreetMap ist nicht einfach nur eine Karte, sondern eine 
*Geodatenbank*.\
Dort ist nicht nur ein Weg oder ein Gebäude eingezeichnet, sondern Details zum 
Gebäude, die Geschäfte innen, die Höhe, das Denkmal und der Mülleimer davor, 
ein Link zu Wikipedia beim Denkmal, die Öffnungszeiten der Geschäfte, die 
Beschaffenheit des Bodens, ob der Weg beleuchtet ist oder der Ort barrierefrei, 
das Restaurant vegane Speisen anbietet, welche Buslinien an der Haltestelle 
davor fahren…

All das braucht oft das Wissen von Menschen vor Ort - nicht eines Unternehmens, 
das einfach irgendwelche Daten zusammensammelt.\
Und: es ist *weit* mehr als eine Straßenkarte!

## Ja, gut. Aber wozu das Ganze?

Mit diesen Informationen können Menschen überall auf der Welt Karten für jeden 
Zweck erstellen:

- Ehrenamtliche und Hilfsorganisationen nutzen z.B. das [Humanitarian 
  OpenStreetMap Team](https://www.hotosm.org/) (kurz: "HOT") um bei 
  Naturkatastrophen Satellitenbilder auszuwerten und hochaktuelle Karten zu 
  erstellen, damit Helfer*innen vor Ort Menschenleben retten können.\
  Als beispielsweise 2010 ein Wirbelsturm das bitterarme Haiti verwüstete gab 
  es kaum Karten die geholfen hätten einzuschätzen, wo Helfer*innen gebraucht 
  würden. Es bestand vorher kein ökonomischer Anreiz, eine so arme Nation zu 
  kartieren. Freiwillige aus der OSM-Community erstellten über HOT in kurzer 
  Zeit genaue Kartendaten, um die Bergungsarbeiten zu unterstützen.
- die [OpenCycleMap](https://www.opencyclemap.org/) zeigt dir alle Radrouten 
  auf der Welt an.
- die [Veggiekarte](https://veggiekarte.de/) hilft dir, Restaurants mit 
  vegetarischen oder veganen Gerichten zu finden.
- Karten wie [Wheelmap](https://wheelmap.org/) zeigen (nicht) barrierefreie 
  Orte und Geschäfte an.
- Routenplaner wie [openroute service](https://maps.openrouteservice.org/) 
  zeigen dir nicht nur die schnellste Route, sondern berücksichtigen auch dein 
  Verkehrsmittel, deine Geschwindigkeit und die Höhenunterschiede.
- die [ÖPNVkarte](https://www.öpnvkarte.de/) zeigt mit diesen Daten alle 
  öffentlichen Verkehrsmittel auf der Welt an.
- das Projekt [Surveillance under Surveillance](https://sunders.uber.space/) 
  kartiert öffentliche und private Videoüberwachung.
- …sogar Details wie "Wo kann ich eigentlich Club-Mate kaufen?" decken Karten 
  wie die [Mate-Karte](https://matekarte.strubbl.de/) ab.

OpenStreetMap ist also mehr, als "nur eine Karte". Die OpenStreetMap ist von 
Menschen für Menschen - unkommerziell, ohne von Werbung oder Tracking und frei 
verwendbar für jeden Zweck an jedem Ort.\
Privatsphärefreundlicher als viele andere Karten ist OSM damit ebenfalls.\
Die Karte kannst du daher mit gutem Gewissen auf einer Webseite einbetten oder 
anderen eine Karte mit Markern zu senden, denn hier steht keine kommerzielle 
Datenkrake dahinter. Und du musst keine Lizenzkosten bezahlen.

Es ist ganz einfach *deine* Karte.

## Wie jetzt - "meine Karte"?

Du kannst alle Kartendaten von OpenStreetMap auch offline auf deinem Smartphone 
oder GPS-Gerät nutzen und herunterladen. Oder ausdrucken. Oder sie selbst 
verbessern:\
Wenn du an der Karte mitarbeiten willst, brauchst du keine spezielle Software, 
denn Hinweise kannst du anonym und direkt über die Webseite eingeben.

So trägst du dazu bei, die Karte für dich und andere aktuell zu halten und 
wichtige Informationen zu ergänzen.

Besonders wichtig ist das überall außerhalb Europas - denn eine solche 
Gemeinschaft ist leider nicht überall auf der Welt gleichmäßig verteilt und hat 
nicht überall die nötige Infrastruktur. Europa und insbesondere Deutschland 
sind sehr detailliert, während andere Länder manchmal eher schlecht kartiert 
sind.

### Die Karte editieren

Um die Karte wirklich zu bearbeiten brauchst du ein Nutzer*innen-Konto bei 
OpenStreetMap und schon kannst du Informationen ergänzen, aktualisieren (oder 
löschen, wenn du weißt, was du tust).\
Das geht direkt im Browser über einen eingebauten Editor ("iD") - oder über 
umfangreiche Programme wie [JOSM](https://josm.openstreetmap.de/) oder 
[QGIS](https://qgis.org/) (ein freies geographisches Informationssystem, dass 
für professionelle Zwecke gedacht ist und ebenfalls kostenlos auf allen 
Plattformen funktioniert). Natürlich gibt es QGIS auch für dein Smartphone. 
Oder du benutzt [OSMAnd](https://osmand.net/) ([auch im freien 
F-Droid-Store](https://f-droid.org/de/packages/net.osmand.plus/) zu haben). 
Damit kannst du nicht nur kartieren, sondern besitzt gleich eine 
Offline-nutzbare Karte, Routenplanung, GPS-Tracker und Editor in einem.

Und, auf welche Details achtest du so, wenn du durch die Stadt oder Landschaft 
gehst?

**Hinweis:**\
*In Deutschland betreibt der gemeinnützige Verein [FOSSGIS 
e.V.](https://www.fossgis.de/) die deutsche OSM-Karte und setzt sich als "local 
Chapter" der internationalen [Open Source Geospatial 
Foundation](https://www.osgeo.org/) für die Förderung von freien Geodaten und 
freier Gedoaten-Software ein.*

- OpenStreetMap (International):
  - <https://www.openstreetmap.org/>
- OpenStreetMap (Deutschland):
  *  <https://www.openstreetmap.de/>

## Hilfe

- OpenStreetMap-Wiki (Sprache: Multilingual):
  * <https://wiki.openstreetmap.org/wiki/Main_Page>
- Anleitung für Einsteiger*innen (Sprache: DE)
  * <https://wiki.openstreetmap.org/wiki/DE:Beginners%27_guide>

## Editoren

- JOSM:
  * <https://josm.openstreetmap.de/>
- OSMAnd:
  * <https://osmand.net/>
  * App im freien F-Droid-Store für Android:
    + <https://f-droid.org/de/packages/net.osmand.plus/>
- QGIS:
  * <https://qgis.org/>

## weiterführende Links

- WeeklyOSM - Wochennotizen und Aktuelles aus der deutschen OSM-Community:
  * <https://weeklyosm.eu/de/>
- FOSSGIS e.V.:
  * <https://www.fossgis.de/>
- OSGeo:
  * <https://www.osgeo.org/>
- Overpass-Turbo (eigene Such-Anfragen zu Karten-Merkmalen per Script starten):
  * <https://overpass-turbo.eu/>
