---
title: 'Instant-Messaging mit Rocket.Chat'
date: '02-04-2020'
publish_date: '02-04-2020'
taxonomy:
    category:
        - Kommunizieren
        - Koordinieren
---

![](img/rocketchat_web_client_2.png)

===

# Instant-Messaging mit Rocket.Chat

Das ZfN bietet euch mit [Rocket.Chat](https://rocket.chat/) eine weitere Instant-Messaging-Plattform (neben [XMPP / "Jabber"](https://blogs.uni-bremen.de/studytools/2020/04/01/instant-messaging-an-der-uni-bremen-mit-xmmp-jabber/)).
Ähnlich wie bei anderen Diensten sind Gruppenchats dabei in sogenannten *Channels* gruppiert, die ihr jederzeit für euch selbst oder eine Gruppe anlegen könnt. In Diskussionen können Diskussionsstränge (sog. *Threads*) entstehen, die angesprochene Themen vertiefen und weiterführen. Zusammen mit Favoriten oder *angepinnten* Nachrichten lassen sich damit Diskussionen gut strukturieren und für später aufbereiten.

Rocket.Chat verbindet die Funktionen von klassischem Instant-Messaging, Foren und Gruppenchats. Audio- und Video-Nachrichten sind ebenfalls möglich (*Achtung! Derzeit noch im Testbetrieb!*).

*Rocket.Chat ist Open-Source und ist neben der Webseite des ZfN auch auf allen gängigen Plattformen als Programm bzw. App verfügbar. Um es zu benutzen braucht ihr lediglich euren Uni-Account und Passwort.*

---

<https://rocket.uni-bremen.de/>

## Hilfe

-  Web-Client des ZfN für Rocket.Chat:
    - [https://rocket.uni-bremen.de/](https://rocket.uni-bremen.de/)
-  Rocket.Chat-Apps für verschiedene Geräte:
    - [https://rocket.chat/install](https://rocket.chat/install)
-  Hilfe für Rocket.Chat:
    - [https://rocket.chat/docs/]()
-  Anleitung für Rocket.Chat der Universität Köln:
    - [https://rrzk.uni-koeln.de/sites/rrzk/E-Mail\_Accounts/anleitung\_rocket\_chat.pdf](https://rrzk.uni-koeln.de/sites/rrzk/E-Mail_Accounts/anleitung_rocket_chat.pdf)
