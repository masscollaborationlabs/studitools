---
title: 'Thunderbird - Ein Programm, sie alle zu verwalten… E-Mails, Kalender und Aufgaben'
date: '16-01-2021'
publish_date: '16-01-2021'
taxonomy:
    category:
        - Kommunizieren
        - Koordinieren
---

![](img/thunderbird_emails.png)

===

# Thunderbird - Ein Programm, sie alle zu verwalten… E-Mails, Kalender und Aufgaben

Sehr geehrte Studierende, sehr geehrte Interessent*innen,

Thunderbird ist ein freies E-Mail-Programm für alle Betriebssysteme. Damit 
können Sie z.B. mehrere E-Mail-Postfächer auf einmal verwalten und abrufen. 
Außerdem gibt es viele Funktionen, um E-Mails zu sortieren, filtern, Aufgaben 
zuzuordnen und damit einen Überblick über E-Mails zu behalten. Auch Ihre 
Kontakte können Sie dort mittels Adressbuch speichern und synchronisieren, so 
dass Sie von überall Zugriff auf Ihre Kontakte haben.

Darüber hinaus bietet Thunderbird zusätzlich einen Terminkalender und eine 
Aufgabenverwaltung. Beide können Sie mit dem Webmail-Portal des ZfN verbinden 
und synchronisieren, sodass Sie auf all Ihren Geräten jeweils ihre Termine und 
Aufgaben im Blick haben. Auch der in Stud.IP integrierte Terminplaner kann in 
Thunderbird als Kalender integriert werden. E-Mails können Sie so z.B. einer 
bestimmten Aufgabe zuordnen und können sie am Smartphone als auch Laptop und 
Webmail bearbeiten. 

! **Hinweis:**  
! *Auch private Kalender Ihres Mailanbieters lassen sich in Thunderbird anzeigen 
! und bearbeiten. Schauen Sie in den Informationen des Mailanbieters nach der 
! Option "WebDAV" oder "iCal" und legen sie entsprechende Kalender in Thunderbird 
! an.*

!! **Wichtig:**  
!! *In Thunderbird müssen Sie jeden Kalender aus ZfN-Webmail als einen einzelnen 
!! Kalender anlegen. Beim Erstellen von Aufgaben und Terminen müssen Sie jeweils 
!! wählen, welchem Kalender der Termin / die Aufgabe zugeordnet ist.*

Für Ihre Sicherheit ist ebenfalls gesorgt, denn Ihre E-Mails lassen sich in 
Thunderbird mit OpenPGP/GPG oder S/MIME verschlüsseln.  
Sollten Sie ständig ähnliche E-Mails schreiben müssen, dann können Sie außerdem 
Vorlagen für E-Mails anlegen.

Sollten Sie schnellere Kommunikation (wie Instant-Messaging) bevorzugen, dann 
kann Thunderbird auch das. Mit dem integrierten Chat-Client können Sie über 
XMPP ("Jabber") z.B. der Uni-Bremen schreiben. Einen [Beitrag zu 
XMPP](https://blogs.uni-bremen.de/studytools/2020/04/01/instant-messaging-an-der-uni-bremen-mit-xmmp-jabber/) 
finden Sie ebenfalls hier auf Studytools. 

Mit freundlichen Grüßen,
Ihr Studitools Blog

---

!!!! <center><a href=https://www.thunderbird.net/de/>https://www.thunderbird.net/de/</a></center>

## Hilfe

- Offizielle Hilfe zu Thunderbird
  * <https://support.mozilla.org/de/products/thunderbird>

- Hilfe-Seite des ZfN zu E-Mail:
  - <https://www.uni-bremen.de/zfn/weitere-it-dienste/e-mail>
- Hilfe-Seite des ZfN zu Kalendern in Webmail:
  * <https://webmail.uni-bremen.de/horde3/services/help/?module=kronolith>
  * <https://www.uni-bremen.de/zfn/weitere-it-dienste/e-mail/webmailer>
