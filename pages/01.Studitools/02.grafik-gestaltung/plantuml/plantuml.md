---
title: 'PlantUML - Diagramme von Projektplanung bis Programmcode'
date: '07-09-2021'
publish_date: '07-09-2021'
taxonomy:
    category:
        - Grafik
        - Gestaltung
---

![](img/plantuml_beispiel_wordpress.png)

===

# PlantUML - Diagramme von Projektplanung bis Programmcode

* Du bist "Tech-Wizard"… äh Programmierer*in und musst… Muggeln die 
  automagischen Funktionen und Abläufe deiner Software erklären?
* Euer Projekt braucht ein Gantt-Diagramm zur Projektplanung?

Mit der "Programmiersprache" PlantUML erstellst du im Handumdrehen UML-Diagramme 
("UML" steht für "Unified Modeling Language"). Damit kannst du für technische 
Abläufe so ziemlich alle Diagramme erstellen, die du jemals brauchen solltest:\
Fluss-, Ablauf- und Sequenzdiagramme, Klassen, Zeit- bzw. Signalverläufe… . Zur 
Projektplanung erstellst du damit auch Gantt-Diagramme, Work-Breakdown-Structure 
Diagramme oder auch Entwürfe deiner GUIs (grafische Oberflächen und Menüs). Oder 
auch Mindmaps.

PlantUML ist insbesondere für Programmierer*innen gut, um schnell Diagramme zu 
den Prozessen und Funktionen von Software zu erstellen. Das hilft Software zu 
planen, umzusetzen und später auch bei der Wartung und Dokumentation… denn weißt 
du in 3 Jahren noch, wie dein Quellcode im Einzelnen zusammenhängt?

Aber auch wenn du kein Techie bist, ist PlantUML gut für dich:\
Projektplanung solltest du nämlich auch manchmal machen -- z.B. für deine 
Abschlussarbeit. Oder du erstellst ein paar hübsche Diagramme, die den Kern 
deiner Hausarbeit visualisieren. Wäre doch nett, oder?

## Aber warum… !? Ist das nicht total umständlich?

Grafiken zu "programmieren" hat neben vielen Anderen Vorteilen zunächst einen 
Zeitvorteil:\
wunderschöne Diagramme zu zeichnen ist nämlich wahnsinnig zeitaufwändig!\ 
Mit PlantUML schreibst du ein paar Zeilen Text in eine Datei, statt in einem 
umfangreichen Grafikprogramm hunderte Formen, Linien und Gruppierungen 
anzuordnen. Klein sind die "Grafiken" auch, denn es ist ja nur ein kurzer Text 
-- und der lässt sich ebenfalls schnell überall ansehen und verändern.

Für dich Programmierer*in ist außerdem vorteilhaft, dass du die Diagramme mit 
einem Versionsverwaltungssystem (etwa [Git](https://git-scm.com/)) verwalten 
kannst. Diagramme und Quellcode der Software kannst du damit im Team erstellen 
und teilen. Für Änderungen haben dann alle einfachen Zugriff auf den Quelltext 
der Grafik -- und kaputtgehen können Formatierungen und das Layout ebenfalls 
nicht.

## Was brauchst du dafür?

**Achtung!**\
*Um PlantUML auszuführen benötigst du die Programmiersprache Java, die du vorher 
installieren musst.*\
*Für Sequenz- und Aktivitätsdiagramme benötigst du außerdem 
[Graphviz](https://graphviz.org/). Mit diesem Programm lassen sich ebenfalls 
einige Arten von Diagrammen erstellen. Daher greift PlantUML für bestimmte 
Diagramme auf Graphviz zurück.*

Was du dafür brauchst? Nur die (Java-)Datei von PlantUML und einen beliebigen 
Texteditor!

Lade die PlantUML Java-Datei (mit der Endung `.jar`) herunter. Diese kannst du 
über die Kommandozeile / Terminal bzw. Eingabeaufforderung aufrufen.

**Tipp:**\
*Auf der Webseite von PlantUML findest du eine Übersicht von vielen Diensten und 
Programmen, die PlantUML-Diagramme bearbeiten und anzeigen können. Außerdem 
findest du sehr viele Beispiele zu den einzelnen Diagrammtypen.*
*Es gibt auch mehrere gute Online-Editoren für PlantUML, bei denen du deine 
Diagramme schreiben und als Bilddatei herunterladen kannst.*

## Beispiel: ein Diagramm als PNG-Bild speichern

In diesem Beispiel bekommst du aus 3 Zeilen Text eine Grafik (Sequenz-Diagramm) 
im `.png`-Bildformat:

Du legst eine Textdatei mit dem nötigen Code an:

```plantuml
@startuml
Alice -> Bob: test
@enduml
```

Speichere diese Datei als `Beispiel.txt` ab. 

Wenn du die `plantuml.jar`-Datei im gleichen Ordner gespeichert hast, musst du 
im Terminal bzw. der Eingabeaufforderung im gleichen Ordner sein und folgenden 
Befehl angeben:

`java -jar plantuml.jar Beispiel.txt`

Erklärung:
: `java` ruft dabei die Programmiersprache Java auf, mit `-jar` gibst du an, 
dass nun eine ausführbare Java-Datei aufgerufen werden soll -- nämlich die 
`plantuml.jar`, die du heruntergeladen hast. Die Datei zum Umwandeln ist dann 
`Beispiel.txt`. PlantUML erstellt automatisch eine Bilddatei im `.png`-Format. 

Wenn du dein Diagramm in einem anderen Format brauchst, kannst du die nötigen 
[Befehle für die Erstellung in unterschiedlichen Dateiformaten 
ansehen](https://plantuml.com/de/command-line#458de91d76a8569c). Mit `-svg` z.B. 
erstellst du eine SVG-Datei oder mit `-pdf` eine PDF-Datei.

**Hinweis:**\\
*Willst du deine Diagramme interaktiver erstellen, kannst du PlantUML auch 
folgendermaßen aufrufen: `java -jar plantuml.jar -gui`. Im folgenden 
Dialogfenster wählst du einen Ordner aus. PlantUML ließt dann automatisch die 
darin enthaltenen Dateien ein und erstellt bei jedem Speichervorgang eine 
Vorschau deines Diagramms. Damit siehst du *sofort* alle Änderungen und ob alles 
geklappt hat.*

---

<https://plantuml.com/de/>

# Hilfe

- Schnelleinstieg in PlantUML (FAQ; Sprache: DE):
  * <https://plantuml.com/de/starting>
- *Wie Modellierung mit UML zu besserem Code führt*
  * <https://blog.nevercodealone.de/modellierung-mit-uml-besserer-code/>
- *Docs-as-Code - Die Grundlagen*
  * <https://www.informatik-aktuell.de/entwicklung/methoden/docs-as-code-die-grundlagen.html>
- Video-Tutorial *Von 0 auf 100 - PlantUML - UML Diagramme "schreiben"* (*coding BOTT*; YouTube; Sprache:DE)
  * <https://www.youtube.com/watch?v=a18wPd5jYmQ>
- "Markdown native diagrams with PlantUML" (Andreas Offenhaeuser, Sprache: EN)
  * <https://blog.anoff.io/2018-07-31-diagrams-with-plantuml/>
