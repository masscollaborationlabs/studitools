---
title: ' Scribus - Printmedien gestalten'
date: '29-10-2020'
publish_date: '29-10-2020'
taxonomy:
    category:
        - Grafik
        - Gestaltung
---

![](img/scribus2.png)

===

# Scribus

* Du träumst von deinem eigenen Magazin?
* Du hast noch gehört wie du "ja" gesagt hast, als es um das Layout dieses 
  Flyers ging?

Das Zauberwort ist "DTP" - *Desktop Publishing*. ;)

Scribus ist so eine (frei verfügbare) DTP-Software, mit dem du Layouts für 
einzelne Seiten, Flyer, Plakate, Magazine oder ganze Bücher anlegen kannst.

"Aber mein Office kann das doch auch!?" - äh, ja klar…\
Hast du schon mal verzweifelt versucht die Seitenzahlen auf deinem Deckblatt zu 
entfernen?\
Ist dir mal so ein Bild durch den Absatz geflutscht und hat deinen Fließtext in 
ein Haiku verwandelt!?\
Na?

Mit Scribus legst du ein Layout fest, in dem Text-Rahmen, Platzhalter für 
Abbildungen, Beschriftungen und grafische Elemente fest platziert werden.\
So kannst du z.B. in der nächsten Ausgabe eines Magazins schnell zwischen 
verschiedenen Seiten-Einteilungen und Rastern wechseln.

Deinen Text darfst du aber trotzdem in einem ganz normalen Office-Programm wie 
[LibreOffice](https://blogs.uni-bremen.de/studytools/2020/03/17/libreoffice-dein-office-fuer-alle/) 
schreiben.\
Scribus nimmt später einfach den Text und die Formatvorlagen deines Dokuments 
und ändert das Aussehen und den Textsatz entsprechend der Einstellungen in 
Scribus.

Außerdem unterstützt Scribus gängige Standards für professionellen Druck wie 
z.B. bestimmte Farbräume oder Schnittmarken für die Druckseiten.

---

<https://www.scribus.net/>

## Hilfe

- Offizielles Scribus-Wiki:
  * <https://wiki.scribus.net/canvas/Scribus>
- Artikelserie *"Magazin in Scribus"* (*Nora Just*; dienonprofitkiste): 
  (Sprache: DE)
  * <https://dienonprofitkiste.de/was-hast-du-von-dieser-artikelserie/>
- Videotutorial *"Printmedien mit Scribus layouten"* (Lettman-Web; YouTube)
  * <https://www.youtube.com/watch?v=J4p8uSVPoWw&list=PLUQjjQ4ViickechnqkXFkbQFG_caJJ8Y2>
