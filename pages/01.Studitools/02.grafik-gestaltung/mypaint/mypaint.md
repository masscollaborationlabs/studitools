---
title: 'MyPaint - dein digitales Kunst-Atelier'
date: '26-11-2020'
publish_date: '26-11-2020'
taxonomy:
    category:
        - Grafik
        - Gestaltung
---

![](img/mypaint.png)

===

# MyPaint - dein digitales Kunst-Atelier

* Erinnerst du dich noch an das Malprogramm "Paint"?
* Willst du auch diese Zeit wieder aufleben lassen?
* Nein? Gut, wir auch nicht!

Wohl nicht umsonst hieß es "PAINt"…

Also lass dich nicht vom Namen täuschen - "MyPaint" hingegen ist nämlich die 
positivste Überraschung seit diesem traumatischen "Malprogramm".\

MyPaint ist dein wahr gewordener Traum für *richtige* Malerei!\
Denn damit kannst du wie mit echten Farben malen: ob Bleistift, Kreide, Acryl, 
Aquarell, Öl oder doch dein vertrauter Textmarker… dir steht einiges an Pinseln 
zur Verfügung.\
Das Besondere an MyPaint: das Programm unterstützt Farben, die sich fast wie 
echte Farben verhalten. Sie lassen sich mischen, verdünnen, überlagern oder 
abtragen. Auch deinen Hintergrund - der übrigens unendlich groß ist - kannst du 
wie Papier oder Leinwand einstellen und beim Malen frei in alle Richtungen 
drehen.

Die Oberfläche des Programms ist sehr einfach gehalten und 
Einsteiger*innenfreundlich - aber denk bloß nicht, dass es deswegen nur wenig 
bietet. Du findest alle üblichen Funktionen eines Grafikprogramms: Ebenen, 
Formen, Pinselstriche als Pfade zeichnen… du weißt schon.

Wenn dir 
[Krita](https://blogs.uni-bremen.de/studytools/2020/10/29/krita-malerei-in-digital/) 
also vielleicht ein bisschen zu einschüchternd ist und PAINt deine Alpträume 
wieder wachruft… mach' ne Kunsttherapie. Mit MyPaint.

**Hinweis:**\
*Standardmäßig speichert MyPaint Bilder als OpenRaster-Format, das von einigen 
anderen Programmen wie z.B. 
[Krita](https://blogs.uni-bremen.de/studytools/2020/10/29/krita-malerei-in-digital/) 
ebenfalls genutzt werden kann. Darin speichert MyPaint auch deine 
Pinsel-Einstellungen, so dass du beim nächsten Öffnen gleich weitermachen 
kannst.*\
*Du kannst deine Bilder aber auch jederzeit als JPG, PNG oder in einem anderen 
Format speichern.*

---

<http://mypaint.org/>

## Hilfe

- Offizielles Wiki von MyPaint (Sprache: EN):
  * <https://github.com/mypaint/mypaint/wiki>
- Pakete mit zusätzlichen Pinseln:
  * <https://github.com/mypaint/mypaint/wiki/Brush-Packages>
- Video-Überblick: *"MyPaint 2.0.0 -- Shockingly Awesome Free Painting App!"* (*Gamesfromscratch*; YouTube)
  * <https://www.youtube.com/watch?v=qo3grRU_kM4>
