---
title: 'Krita – Malerei in digital'
date: '29-10-2020'
publish_date: '29-10-2020'
taxonomy:
    category:
        - Grafik
        - Gestaltung
---

![](img/krita.png)

===

# Krita – Malerei in digital

* Du hast deine Wachsmalstifte vergessen und nur dein Notebook dabei?
* Dein erstes Semester mitten im Corona-Chaos -- perfekter Stoff für ein 
  Comic…!?

Krita ist ein freies Programm zum Malen, Zeichnen und Kolorieren -- egal ob 
Schaubild mit Marker, Superheldin in Acryl oder Landschaft als Aquarell… sogar 
Animationen kannst du erstellen.

Malerei kann Krita sehr viel besser als z.B. das ebenfalls freie Grafikprogramm 
[GIMP](https://www.gimp.org/), das oft für Foto-Bearbeitung eingesetzt wird. 
Trotzdem sind beide Programme wohl ähnlich gut auch dafür geeignet.

Als Werkzeuge hast du alle gängigen Funktionen zur Verfügung, die auch andere 
Programme bieten:Farben, Layer bzw. Ebenen, Masken, Effekte, Pinsel…

Aber in Krita findest du nicht nur jede Menge voreingestellter Pinsel, sondern 
du kannst auch eigene erstellen. Eine große Besonderheit sind dabei spezielle 
Pinsel, wie beispielsweise die für 
[Wasserfarben](https://youtu.be/drYOrnG0Ui0?t=516), deren "Flüssigkeit" sich 
verdünnt, mischt oder eine bestimmte Textur nachahmt. Eine solche Vielfalt 
bietet dir kaum ein anderes Programm und viele Künstler*innen bieten 
Pinsel-Pakete zum Download an.

Solltest du dich auch für Animationen begeistern können -- das kann Krita auch: 
Frames festlegen, Animations-Modus an und los geht es!

**Tipp:**\
*Für Mal- und Zeichenprogramme solltest du eher nicht deine Maus benutzen, 
sondern ein Grafiktablett mit einem druckempfindlichen Stift. Damit kannst du 
wie mit einem richtigen Stift malen, denn je nach Druck ändert sich dann auch 
die Breite oder Stärke deiner Farben.*

---

<https://krita.org>

## Hilfe

- Offizielle Hilfe zu Krita (Sprache: EN)
  * <https://docs.krita.org/en/>
- Offizieller YouTube-Kanal von Krita (Sprache: EN)
  * <https://www.youtube.com/c/KritaOrgPainting/videos>

- Video-Tutorials:
  - "How to paint in Krita" (YouTube; *Jenna Drawing*; Sprache: EN)
    * <https://www.youtube.com/watch?v=Z06RRp81iDM>
  - "Complete Krita Animation Demo / Tutorial in 30 Mins!" (YouTube; *Tahnee Gehm*; Sprache: EN)
    * <https://www.youtube.com/watch?v=Pp0DvgZ2k6k>
- Tutorials und HowTos von *David Revoy* (Spache: EN)
  * <https://www.davidrevoy.com/categorie3/tutorials-brushes-extras>

