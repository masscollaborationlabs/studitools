---
title: 'Fotos entwickeln - mit RAWTherapee'
date: '26-11-2020'
publish_date: '26-11-2020'
taxonomy:
    category:
        - Grafik
        - Gestaltung
---

![](img/rawtherapee.png)

===

# Fotos entwickeln - mit RAWTherapee

RAWTherapee ist ein freies Programm um Fotos zu entwickeln. Wie der Name schon 
verrät, geht es um die RAW-Bilder deiner Kamera - also die "rohen" Sensor-Daten 
deiner Kamera. Das ist wie die Entwicklung von einem Analog-Film in der 
Dunkelkammer, nur eben am Computer.

**Hinweis:**\
*Wenn du Fotos bearbeiten, schneiden und mit Effekten versehen willst, solltest 
du dir andere Programme wie 
[GIMP](https://blogs.uni-bremen.de/studytools/2020/11/05/gimp-bildbearbeitung-fuer-jeden-zweck/) 
oder 
[Krita](https://blogs.uni-bremen.de/studytools/2020/10/29/krita-malerei-in-digital/) 
ansehen.*

Was diese RAW-Bilder von JPG-Bildern unterscheidet?\
Die Rohdaten deiner Kamera können beispielsweise über 1000 unterschiedliche 
Abstufungen von Farben erfassen - ein JPG-Bild kann aber nur 255 Abstufungen 
speichern. Wenn du in greller Sonne fotografierst, ist da manchmal der Himmel 
zu Hell oder die Landschaft zu dunkel. Bei einem RAW-Bild kannst du 
nachträglich die Belichtung deines Bildes verändern, sodass du beides gut 
erkennen kannst. Auch orange Party-Fotos kannst du damit nachträglich viel 
natürlicher aussehen lassen.

Mit RAWTherapee du fast alles an deinen Bildern verändern: die Belichtung, 
Farben (verschiedene Farbräume), Objektiv-Verzerrung, Kontrast und Schärfe sind 
dabei nur die üblichen Optionen. Du kannst mit Helligkeits-Verläufen und 
Vignetten arbeiten, das Bild von Farb- und Helligkeitsrauschen befreien, oder 
nachschärfen. Sogar Rauch oder Nebel lassen sich zum Teil entfernen.

Natürlich kann RAWTherapee deine Bilder auch verwalten, filtern und Ordnen. 
Dazu bewertest du sie mit Sternen, gibst ihnen Farbcodes und speicherst deine 
Bearbeitungs-Profile, um sie auf andere Bilder zu übertragen.

Am Ende speicherst du die Bilder entweder einzeln oder lässt sie in einem 
ganzen Stapel "entwickeln" (Batch-Processing).

**Hinweis:**\
*Du willst die Farben wie bei einem analogen Film aussehen lassen? Klar! Dafür 
gibt es über zusätzliche Profile zum herunterladen viele Vorlagen, die 
verschiedenen analogen Filmen nachempfunden sind. Diese kannst du auf ein Bild 
und hast sofort das Aussehen eines beliebigen Films. Mit dieser Technik kannst 
du sogar eigene Farbeinstellungen erstellen. Diese Filter kannst du übrigens 
auch in 
[GIMP](https://blogs.uni-bremen.de/studytools/2020/11/05/gimp-bildbearbeitung-fuer-jeden-zweck/) 
oder 
[Krita](https://blogs.uni-bremen.de/studytools/2020/10/29/krita-malerei-in-digital/) 
mit dem PlugIn "GMIC" nutzen. Wie das alles funktioniert, steht im Handbuch 
unter "Film Simulation".*

---

<http://rawtherapee.com/>

## Hilfe

- Offizielle Hilfe "RawPedia" (Mehrsprachig)
  * <http://rawpedia.rawtherapee.com/Main_Page>
- Film Simulation (analoge Filme; Sprache: EN)
  * <http://rawpedia.rawtherapee.com/Film_Simulation>
    + Blog-Einträge zur Film-Simulation von *Pat David* (Sprache: EN)
      + <https://patdavid.net/2013/08/film-emulation-presets-in-gmic-gimp.html>
      + <https://patdavid.net/2013/09/film-emulation-presets-in-gmic-gimp.html>

- Video-Tutorials:
  * "Urlaubsbilder verbessern oder retten - Einführung im RAWtherapee" (*Lettmann-Web*; YouTube)
    + <https://www.youtube.com/watch?v=M7CTmxykw7M>
  * "Processing Photographs in Rawtherapee" (*jacob swing*; YouTube)
    + <https://www.youtube.com/watch?v=WnUpCoOcRLE>
