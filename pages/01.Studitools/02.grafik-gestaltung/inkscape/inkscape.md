---
title: 'Inkscape - Vektorgrafiken für Logos, Layouts, Laser-Cutter'
date: '08-10-2020'
publish_date: '08-10-2020'
taxonomy:
    category:
        - Grafik
        - Gestaltung
---

![](img/inkscape_2.png)

===

# Inkscape - Vektorgrafiken für Logos, Layouts, Laser-Cutter

Inkscape ist ein freies Programm um sogenannte *Vektorgrafiken* zu bearbeiten. 

Im Gegensatz zu *Pixel-Grafiken* sind Bilder dabei nicht aus kleinen Blöcken (den "Pixeln") aufgebaut, sondern mathematisch berechnet. Dadurch kannst du sie beliebig vergrößern oder verkleinern, ohne dass sie dabei unscharf oder "pixelig" werden.

Besonders gut geeignet ist Inkscape um Logos oder Banner zu erstellen. Aber auch ein Layout für ein Handout oder einen Flyer sind mit Inkscape schnell gemacht. Sogar technische Zeichnungen oder Grundrisse kannst du damit zeichnen. So kannst du z.B. auch Vorlagen für einen Laser-Cutter zeichnen oder Schnittmuster für Stoffe. Der Vorteil liegt wie gesagt darin, dass jede Größe vom Bild ohne Verlust möglich ist.

Inkscape ist sehr einfach zu bedienen, hat dabei aber sehr viele Funktionen.  
Alle Striche und jede Form sind in Inkscape eigene Objekte, die du frei verschieben kannst. Das macht es dir besonders einfach, damit Schaubilder oder Anordnung von Objekten vorzunehmen.

Ist dein Werk fertig, kannst du es als eine gängige SVG-Vektorgrafik abspeichern, oder in jedem anderen gängigen Bildformat und als PDF.

Achso, falls du dein Bild mal mit einem anderen Programm erstellt hattest: klar kann Inkscape alle gängigen Formate lesen! ;)

---

!!!! <center><a href=https://inkscape.org>https://inkscape.org</a></center>

## Hilfe

- Inkscape kennenlernen (Sprache: DE)
  - <https://inkscape.org/de/lernen/>
- Buch *Inkscape: Guide to a Vector Drawing Program* (Sprache: EN):
  * <http://tavmjong.free.fr/INKSCAPE/MANUAL/html/index.html>

### Video-Tutorials

- Video-Tutorials von *Logos by Nick* (YouTube; Sprache: EN):
  * <https://www.youtube.com/c/LogosByNick/videos>
- Reihe: *"Inkscape Tutorials"* (YouTube; Sprache: EN):
  * <https://www.youtube.com/playlist?list=PLqazFFzUAPc5lOQwDoZ4Dw2YSXtO7lWNv>
