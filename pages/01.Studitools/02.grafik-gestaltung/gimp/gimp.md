---
title: 'GIMP - Bildbearbeitung für jeden Zweck'
date: '05-11-2020'
publish_date: '05-11-2020'
taxonomy:
    category:
        - Grafik
        - Gestaltung
---

![](img/gimp.png)

===

# GIMP - Bildbearbeitung für jeden Zweck

GIMP steht für *GNU Image Manipulation Programm* und ist seit vielen Jahren 
eines *der* Programme für Grafikbearbeitung. GIMP ist freie Software und damit 
kostenlos und für quasi alle Plattformen verfügbar.

Als ausgewachsenes Grafikprogramm bietet dir GIMP alles, was du für 
unterschiedliche Anwendungsbereiche brauchst:\
ob Foto-Bearbeitung, Malerei, Retusche oder Design - GIMP kann eigentlich 
alles. Falls dir die Funktionen alle noch nicht genug sind, warten auch noch 
mächtige Plugins wie [GMIC](http://gmic.eu/) auf dich.\
Pinsel-Spitzen kannst du sogar sowohl in 
[Krita](https://blogs.uni-bremen.de/studytools/2020/10/29/krita-malerei-in-digital/), 
MyPaint und GIMP verwenden. So kannst du deine liebsten Pinsel gleich in 
anderen Programmen weiter nutzen.

Ansonsten stehen dir die üblichen Werkzeuge für Bildbearbeitung zur Verfügung:\
Ebenen, Masken, Farbkurven, Filter und verschiedene Manipulations-Werkzeuge um 
Inhalte zu verändern. \
GIMP unterstützt darüber hinaus auch unterschiedliche Farbprofile für Monitore 
und professionellen Druck. Und natürlich kannst du auch dein Grafiktablett in 
GIMP benutzen.

Als Dateiformate liest und schreibt GIMP außerdem alle gängigen Formate, so 
dass du zwischen verschiedenen Programmen wechseln kannst, wenn es für dich 
wichtig wird… und natürlich kannst du deine Werke auch gleich als PDF 
speichern.\
Wenn du allerdings Zeichnungen anfertigen willst, die sich beliebig vergrößern 
lassen, dann solltest du eher zu einem Programm für Vektorgrafiken wie 
[Inkscape](https://blogs.uni-bremen.de/studytools/2020/10/08/inkscape-vektorgrafiken-fuer-logos-layouts-laser-cutter/) 
greifen.

**Tipp:**  
*Bei "Speichern" und "Speichern unter…" verwendet GIMP das eigene 
XCF-Bildformat, in dem Ebenen und GIMP-eigene Einstellungen gespeichert werden. 
Bei "Exportieren" kannst du andere Dateiformate wie JPG, PNG oder PDF 
auswählen.*

---

<https://www.gimp.org/>

## Hilfe

- Offizielles Handbuch:
  - <https://www.gimp.org/docs/>
- Tutorials auf gimp.org
  * <https://www.gimp.org/tutorials/>

- Video-Tutorial *"Gimp 2.10 Crashkurs - In 1h zum halben Gimp-Profi! - Für 
  absolute Anfänger"* (YouTube; *Linux Guides*)
  * <https://www.youtube.com/watch?v=IDY5OuMS_6E>
