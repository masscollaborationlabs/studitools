---
title: 'Grafik und Gestaltung'
content:
    items:
        - '@self.children'
    limit: 50
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
show_sidebar: false
---

