---
title: 'Darktable - Foto-Entwicklung in deiner digitalen Dunkelkammer'
date: '12-12-2020'
publish_date: '12-12-2020'
taxonomy:
    category:
        - Grafik
        - Gestaltung
---

![](img/darktable.png)

===

# Darktable - Foto-Entwicklung in deiner digitalen Dunkelkammer

Darktable ist ein freies Programm um Fotos zu entwickeln. Ähnlich wie 
[Rawtherapee](../rawtherapee/) 
entwickelst du damit die RAW-Bilder (die "rohen" Sensor-Daten) deiner Kamera.  
Der Name "Darktable" spielt übrigens auf den "Leuchttisch" und die 
"Dunkelkammer" eines Fotolabors für analoge Filme an.

! **Hinweis:**  
! *Wenn du Fotos bearbeiten, schneiden und mit Effekten versehen willst, solltest du dir eher Programme wie [ GIMP](../gimp/) oder [Krita](../krita/) ansehen.*

RAW-Bilder enthalten als "rohe" Daten deines Kamera-Sensors wesentlich mehr 
Farben als die JPG-Bilder, die du vermutlich viel öfter siehst.  
Bei einem RAW-Bild kannst du nachträglich beispielsweise noch die Helligkeit 
deines Bildes stark verändern oder einen Farbstich entfernen.

In Darktable hast du zwei Bereiche: einen "Leuchttisch" und eine 
"Dunkelkammer":  
- Am *Leuchttisch* verwaltest du deine Bilder, ordnest sie und bewertest sie 
  z.B. mit Sternen. Außerdem lassen sich Sammlungen anlegen.  
- In der *Dunkelkammer* hast du alle Werkzeuge, dein Bild zu entwickeln. Dort 
  kannst du die Helligkeit bzw. die Belichtung verändern, Farben einstellen, 
  Kontrast und Schärfe verändern.  

Ähnlich wie bei 
[RAWTherapee](../rawtherapee/) 
kannst du auch in Darktable mit Verläufen und Vignetten arbeiten. Allerdings 
hast du hier mehr Kontrolle: mit "Masken" kannst du die Veränderungen auf 
bestimmte Bereiche deines Bildes beschränken.

Darktable kann außerdem viele Funktionen von deiner Grafikkarte berechnen 
lassen und ist dadurch sehr schnell bei der Benutzung.

---

!!!! <center><a href=https://www.darktable.org/>https://www.darktable.org/</a></center>

## Hilfe

- Offizielle Hilfe-Seite (EN / Multilingual)
  * <https://www.darktable.org/resources/>
- Video-Tutorial: *"NATURAL LIGHT PORTRAIT EDITING IN DARKTABLE . Beginner 
  Photo Editing . FREE Lightroom Alternative"* (*Amber McIntire Photography*, 
  YouTube)
  * <https://www.youtube.com/watch?v=grPKZbgwQcI>

