---
title: 'Zim - das Desktop-Wiki für deine Notizen'
---

![](img/zim.png)

===

# Zim - das Desktop-Wiki für deine Notizen

- Du willst ein Forschungsjournal haben, in dem du deine Gedanken und Notizen zu einem Projekt sammeln kannst?
- Zur Projektplanung brauchst du mehr Informationen über deine einzelnen Todo-Punkte?
- Deine Hausarbeit wird zur Doktorarbeit?

Dann ist *Zim* das richtige Programm für dich!

*Zim* ist ein für alle Betriebssysteme frei verfügbares Desktop-Wiki, in dem du 
so etwas wie Wikipedia anlegen kannst:  
ein Forschungs-Journal mit Notizen für eine Arbeit, dein ganzes Studium oder 
einfach nur ein Tagebuch für dich.

Deine Notizen schreibst und sortierst du in einem Zim-Notizbuch auf Seiten und 
Unterseiten. Diese verknüpfst du dann durch Links mit anderen Einträgen. Wie 
bei einem richtigen Wiki unterstützt Zim dabei Rücklinks: damit siehst du, 
welche Seiten auf die gerade aktuelle Seite verweisen. So kannst du z.B. 
schnell sehen, welche Notizen du zu einem bestimmten Thema verlinkt hast.

In deine Notizen kannst du Tabellen oder Bilder einfügen, Dateien als Anhänge 
speichern oder auf Webseiten und lokale Dateien verlinken. Du kannst auch 
Schlagworte bzw. Tags nutzen, damit du nicht den Überblick verlierst.  
Es gibt außerdem viele Erweiterungen, die gleich dabei sind: ein Journal, 
mathematische Formeln, Todo-Listen und vieles mehr. 

Mit Zim hast du deine Notizen immer griffbereit und nichts geht mehr verloren. 
Und falls du mehrere Projekte hast, kannst du sogar für jedes ein eigenes 
Notizbuch anlegen. Die kannst du sogar an unterschiedlichen Ordnern bzw. Orten 
speichern. 

!!! **Tipp:**  
!!! *Zim speichert deine Notizen in einfachen Text-Dateien und Ordnern. So kannst 
!!! du immer darauf zugreifen (selbst, wenn du mal kein Zim zur Hand hast). 
!!! Außerdem kannst du es z.B. über die Campus-Cloud 
!!! "Seafile" der Uni Bremen 
!!! oder mit 
!!! [Syncthing](../../sammeln-speichern-ordnen/syncthing/) 
!!! auch zwischen mehreren Geräten synchronisieren.*  

!!!! <center><a href=https://zim-wiki.org>https://zim-wiki.org/</a></center>

## Hilfe

- Handbuch zu Zim (EN):
    - <https://zim-wiki.org/manual/Start.html>
- Plug-Ins und andere Anleitungen (EN):
    - <https://github.com/jaap-karssenberg/zim-wiki/wiki>

### Videos

- kurzes Einführungs-Video zum Zim von *Achim Halfmann* (YouTube; Sprache: DE)
  * <https://www.youtube.com/watch?v=1345U-8gVGg>
- Zim Done On Linux *DONE ON LINUX* (YouTube; Sprache: EN)
  * <https://www.youtube.com/watch?v=FJVmLF_Z7qQ>


