---
title: 'zoterobib – Literaturverzeichnisse schnell erstellen'
---

![](img/zbib.png)

===

# zoterobib – Literaturverzeichnisse schnell erstellen

„zoterobib“ ist ein Dienst, mit dem du bequem und schnell ein Literaturverzeichnis erstellen kannst.
In der Suche gibst du jeweils eine ISBN, DOI oder URL deiner Quelle an und wählst einen Zitierstil aus.
Anschließend kannst du deine Literaturliste in die eigene Zwischenablage kopieren (wie mit `STRG+C`) oder die Liste als HTML-, RTF- oder BibTeX-Datei herunterladen. Wenn du sie für später in der Literaturverwaltung Zotero brauchst, kannst du sie auch direkt dort speichern.

!!!! <center><a href=https://zbib.org/>https://zbib.org/</a></center>

*zoterobib ist ein Angebot der Corporation for Digital Scholarship, die auch 
das freie Literaturverwaltungsprogramm 
[Zotero](../zotero/) 
entwickelt.*

