---
title: 'Syncthing - deine Cloud ohne Cloud'
---

![](img/syncthing_2.png)

===

# Syncthing - deine Cloud ohne Cloud

*"The Cloud ist just someone else's computer"* heißt es - du solltest also besser dieser anderen Person Vertrauen, bei der du deine Daten ablegst!

Deine Daten könntest du schützen…

- …indem du sie verschlüsselt dort speicherst (z.B. mit [VeraCrypt](../veracrypt/))
- …einen Vertrauenswürdigen Anbieter wählst (z.B. deine Uni-Cloud *Seafile*)
- …oder einfach deine eigene "Cloud" nutzt!

Was du dafür brauchst?  
Naja, ein Ding, alles zu speichern, zu synchronisieren - ein *Syncthing* eben!

Syncthing synchronisiert deine Dateien zwischen verschiedenen Geräten. Dazu braucht es keinen Server oder muss den "Computer von jemand anderem" benutzen: die Daten werden direkt zwischen deinen Geräten übertragen. So behälst du hast die volle Kontrolle über deine Daten. 

!! **Achtung!**  
!! Syncthing funktioniert je nach Betriebssystem (Windows, MacOS, Linux) ein wenig anders. Es starten sich nicht unbedingt beim Systemstart oder hat je nach System unterschiedliche Installations-Routinen. Suche also in der offiziellen Hilfe oder in einem aktuellen Video-Tutorial nach einer Anleitung, die für dein Gerät möglichst passend ist.

Grundsätzlich funktioniert Syncthing aber so:

- Jedes Gerät bekommt eine eindeutige Nummer zugewiesen.
- Um Ordner zwischen Geräten zu synchronisieren, musst du jeweils einmal diese Nummer angeben und den Ort, an dem der synchronisierte Order auf jedem einzelnen Gerät liegen soll. 
- Ordner können bei den unterschiedlichen Geräten in unterschiedlichen Verzeichnis-Pfaden liegen (z.B. kann die Musik bei einem Gerät auf dem Desktop unter `Musik` liegen, bei dem anderen Gerät unter `Downloads/Musik`).
- Syncthing hält anschließend alle Ordner auf den Geräten Synchron: ändert sich eine Datei, wird sie sofort mit dem anderen Gerät synchronisiert.

!! **Achtung!**  
!! Da Syncthing Dateien *direkt* zwischen Geräten synchronisiert, *müssen beide Geräte angeschaltet und online sein*!  
!! (Das gilt natürlich nicht, wenn du deinen eigenen Server betreibst, der immer an ist.)

Wenn du Syncthing nach der Installation startest, kannst du alle Einstellungen über deinen Browser vornehmen. Dazu musst du auf die Web-Adresse `http://localhost:8384/` gehen. Dort bekommst du alle Einstellungen angezeigt, sobald Syncthing gestartet ist.

Einstellen kannst du beispielsweise:

- ob Syncthing alte Versionen der Dateien löscht oder behält 
- du ein Versionswerwaltungssystem wie `git` benutzten willst
- ein Gerät nur Dateien senden oder nur empfangen darf

Syncthing ist freie Software und für alle Plattformen verfügbar. Auch für Mobilgeräte gibt es Syncthing in den jeweiligen App-Stores.

!!!! <center><a href=https://syncthing.net>https://syncthing.net</a></center>

## Hilfe

- "Getting started" (Sprache: Englisch)
  * <https://docs.syncthing.net/intro/getting-started.html#getting-started>
- Dokumentation / Handbuch (Sprache: Englisch)
  * <https://docs.syncthing.net/>

- Video-Anleitung: "Dateien kostenlos, sicher & OpenSource synchronisieren - Syncthing" (YouTube; "PXL-Tech")
  * <https://www.youtube.com/watch?v=9loj-czAWF4>
