---
title: 'Taguette - Texte und Interviews Kodieren'
---

![](img/taguette.png)

===

# Taguette - Texte und Interviews Kodieren

* Dein Prof so: "Damals, '78, Großrechner und Kopierer… !"  
  und du so: "Aber mein Laptop kann doch `Strg+x + Strg+v`!?"  
  Und Prof so: "…und die 40 Interviews für meine Dissertation habe ich noch 
  ausgedruckt und mit farbigen Markern und Deckweiß ausgewertet! Da müssen Sie 
  jetzt halt auch durch!"

So ein Quark!

Für quantitative Methoden gibt es Software an jeder Ecke: sogar 
[LibreOffice](../../schreiben/libreoffice/) 
kann statistische Verfahren wie ANOVA-Tests!  
Doch ausgerechnet *qualitative Methoden* sind "Quäli-tative Methoden" mit 
enormen Quantitäten Papier, Klebstoff und Marker - wenn du nicht gerade ein 
paar hundert Euro für teure Spezialsoftware übrig hast… 

Aber für alle Forscher\*innen, die ihre Brötchen mit qualitativen Methoden 
verdienen wollen, gibt es Abhilfe: *Taguette*!  
(Mit dem passenden Spruch: *"Feeding Your Qualitative Needs"*)

Wer hat's erfunden!?  
Einige Wissenschaftler\*innen, deren Stimmungsringe durch solche Missstände zu 
lange "Signalgrau" waren:

> *"It's not right or fair that qualitative researchers without massive research 
> funds cannot afford the basic software to do their research."*

*Taguette* ist eine frei verfügbare Software, mit der du beliebige Texte mit 
farbigen Markern am Computer(!!!) markieren kannst. Taguette kann dabei sowohl 
auf einem Server online laufen oder lokal auf deinem Computer.

**Wichtig!**  
*Bei Interviews ist die Anonymität deiner Interviewpartner\*innen besonders 
wichtig! Diese sensiblen Daten verarbeitest du besser nur auf vertrauenswürdigen 
Diensten: von deiner Uni, oder lokal bei dir.  
Cloud-Anbieter oder Dienste im Ausland haben oftmals nur mangelhaften 
Datenschutz oder werten Inhalte oft sogar für Werbung aus.*

Für eine qualitative Auswertung weist du einzelnen Textstellen Tags bzw. Codes 
zu, die so stufenweise ein Kategoriensystem für z.B. eine Inhaltsanalyse 
ergeben. Die Codes kannst du auch später noch in Taguette zusammenführen, damit 
dein Kategoriensystem auch wirklich stimmig ist.

Alle Codes bekommen bei der Erstellung nicht nur einen Namen, sondern auch eine 
Beschreibung. Diese ist wichtig für dein Codebuch - also die Auflistung aller 
Codes und deren Kriterien; denn die musst du ja ebenfalls nachvollziehbar 
darlegen.

**Hinweis:**  
*In einem anderen Artikel stellen wir dir 
[oTranscribe](../../schreiben/otranscribe/) 
vor, mit dem du deine Interviews transkribieren kannst. oTranscribe verarbeitet 
und speichert ebenfalls alle Daten lokal auf deinem Computer.*

Wenn du fertig bist, kannst du dir den gesamten Text mit allen Markierungen 
darin anzeigen lassen. Die Codes stehen dann jeweils am Ende einer Markierung. 
Diese Ansicht speicherst du und kannst dann alles mit einer Schere und Papier… 
weißt du ja…

Oder du exportierst ganz einfach die Datei!  
Denn am Ende deines nun qualitativ hochwertigen Tages kann Taguette dir auch all 
deine Codes, Beschreibungen und Textstellen und das Codebuch ausgeben. Bei den 
Markierungen in Taguette lassen sich einzelne Codes anwählen, sodass du nur noch 
alle Textstellen mit diesem Code siehst.  
Und diese gefilterte Ansicht lässt sich in vielen gängigen Dateiformaten 
speichern: als Tabellendokument für Office, als HTML-Datei oder einfache 
Textdatei.

Und, welche Farbe hat *dein* Stimmungsring jetzt!?

**Fun-Fact:**  
*"Signalgrau" ist - ehrlich! - eine offizielle RAL-Farbe: Nr. 7004*

!!!! <center><a href=https://www.taguette.org/>https://www.taguette.org/</a></center>

## Hilfe

- Webseite von Taguette (Sprache: EN):
  * <https://www.taguette.org/>
- Installationsanleitung (Sprache: EN):
  * <https://www.taguette.org/install.htm>
- "Getting Started" Anleitung (Sprache: EN):
  * <https://www.taguette.org/getting-started.html>

### Videos

- "Free Qualitative Data Analysis with Taguette and qcoder! an IASSIST webinar" (*iassistdata*; YouTube; Sprache: EN)
  * <https://www.youtube.com/watch?v=OIB_xLlM8Fw>
- "Qualitative Research Using Open Source Tools - Beth Duckles & Vicky Steeves" (*csvconf*; YouTube; Sprache: EN)
  * <https://www.youtube.com/watch?v=DwCunW19wcQ>
- "CAQDAS webinar 016 Open Tools for Qualitative Analysis" (*Christina Silver, PhD.*; YouTube, Sprache: EN)
  * <https://www.youtube.com/watch?v=9teZO-KVwqk>
- "Taguette Basic Data analysis with Excel" (*Rich McCue*; YouTube; Sprache: EN)
  * <https://www.youtube.com/watch?v=GvqVeZPoEvs>
