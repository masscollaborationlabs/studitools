---
title: 'Dateien verschicken -- mit croc'
date: '24-09-2021'
publish_date: '24-09-2021'
taxonomy:
    category:
        - Sammeln
        - Speichern
        - Ordnen
---

![](img/croc.png)

===

# Dateien verschicken - mit croc

* Aaaaarghh! DIESE DATEI!!!!  
Für 'ne E-Mail zu groß, *Seafile* hast du nicht, deine Dingsbox ist voll, der 
Chat kann das nicht, im selben Netzwerk seid ihr nicht, einen FTP-Server… Was 
bitte!?
* … und deine Brieftaube ist gerade frühstücken…

Willkommen!  
Es ist 2021 - und immer noch so ätzend umständlich "nur eine Datei" zu 
übertragen, wie eh und je!

Oh, hey - darf ich dir *croc* vorstellen!?

*croc* ist das nette Krokodil von nebenan, dass deine Dateien durchs Internet 
bringt. Ohne viel Klimbim. Und das nicht nur auf allen Betriebssystemen, sondern 
auch als App für dein Smartphone (natürlich im freien App-Store 
[F-Droid](https://f-droid.org/de/)).

Deine Dateien werden nicht nur sicher (Ende-zu-Ende) verschlüsselt übertragen, 
sondern du kannst auch gleich mehrere Dateien versenden und auch abgebrochene 
Downloads weiterführen.

!! **Hinweis:**  
!! *Klingt zu schön, um wahr zu sein?  
!! Okay, hier das Häkchen: das Programm croc hat (außer auf dem Smartphone) (noch) 
!! keine grafische Oberfläche, sondern funktioniert über die Kommandozeile 
!! (Texteingabe).  
!! Aber hey - es ist einfacher als Brieftaubenzucht und was hast du jetzt noch zu 
!! verlieren, wo du schon all deine Nerven los bist?)*

Nachdem du das Programm installiert hast, kannst du croc über die Kommandozeile 
aufrufen - natürlich mit dem Befehl `croc`. Wenn du es ohne zusätzliche Angaben 
aufrufst, möchte croc einen Download-Code haben - dazu gleich mehr.

Wenn du eine Datei verschicken willst, gibst du ein: `croc send DATEINAME`, also 
in etwa so:

`croc send TESTDATEI.jpg`

Oder bei zwei Dateien also entsprechend:

`croc send TESTDATEI1.jpg TESTDATEI2.zip`

!!! **Tipp:**  
!!! *Du musst dafür im gleichen Verzeichnis wie die Dateien sein oder den Dateipfad 
!!! angeben. Auf allen Betriebssystemen stehen dir ein paar Befehle zur Verfügung, 
!!! um deinen Aktuellen "Standort" herauszufinden:  
!!! `pwd` (Linux / MacOS) zeigt das "present working directory" an (das Verzeichnis, 
!!! in dem du aktuell bist).  
!!! Mit `cd` und einem Verzeichnisnamen wechselst du zwischen Verzeichnissen.  
!!! Mit `ls` (Linux / MacOS) oder `dir` (Windows) listest du alle Dateien und Ordner 
!!! in einem Verzeichnis auf.*

croc generiert dann einen einfach zu merkenden, einzigartigen Download-Code, der 
in etwa so aussieht:

`1234-pepper-camel-water`

Eine andere Person, die diese Datei empfangen soll, ruft croc bei sich mit `croc 
1234-pepper-camel-water` auf - und schon startet der Versand!

!!! **Tipp:**  
!!! *Statt einem zufälligen Code könnt ihr auch einen eigenen festlegen. Das hat den 
!!! Vorteil, dass ihr euch z.B. vor dem Versand nicht mehr unbedingt erneut 
!!! absprechen müsst, wie euer Download-Code lautet.*

!!!! <center><a href=https://github.com/schollz/croc>https://github.com/schollz/croc</a></center>

## Hilfe

- Blog-Eintrag zu croc vom Entwickler:
  * <https://schollz.com/blog/croc6/>
