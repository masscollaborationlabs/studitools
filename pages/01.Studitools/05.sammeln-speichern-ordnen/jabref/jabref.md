---
title: 'JabRef - Literaturverwaltung für LaTeX'
date: '16-06-2020'
publish_date: '16-06-2020'
taxonomy:
    category:
        - Sammeln
        - Speichern
        - Ordnen
---

![](img/jabref.png)

===

# JabRef - Literaturverwaltung für LaTeX

JabRef ist ein weiteres Programm zur Literaturverwaltung, ähnlich wie Zotero. 

Die Besonderheit von JabRef ist, dass es stärker auf das Textsatz-Programm 
[LaTeX](../../06.schreiben/latex/) 
ausgerichtet ist (LaTeX wird oft in Naturwissenschaftlichen Fächern genutzt, 
u.a. wegen der guten Unterstützung für Formeln). Es unterstützt besser 
LaTeX-Editoren wie LyX, Texmaker oder TeXstudio.

! **Hinweis:**  
! *Auch Zotero unterstützt dich beim Schreiben mit LaTeX. Dort gibt es 
! Erweiterungen, die speziell den Workflow zwischen Zotero und LaTeX 
! unterstützen.*

Mit einer Literaturverwaltung kannst du Quellen aus unterschiedlichen 
Bibliothekskatalogen, Online-Shops und Webseiten direkt aus deinem Browser 
speichern. In JabRef werden dann alle Daten der Literatur automatisch 
hinterlegt. Außerdem kannst du dort Sammlungen von Quellen (z.B. zu einem 
bestimmten Thema oder einem Projekt) erstellen, Notizen, Exzerpte und 
Schlagworte speichern. 

Einzelne Quellen lassen sich Bewerten, um z.B. besonders wichtige Quellen 
leichter zu finden. Außerdem lassen sich die Quellenangaben zwischen mehreren 
Katalogen abgleichen, sodass z.B. eine fehlende ISBN-Nummer aus einem anderen 
Katalog ergänzt werden kann. 

Auch JabRef verbindet sich wie Zotero mit deinem Browser und deinem 
Schreibprogramm. So kannst du direkt beim Schreiben Quellenangaben in deinen 
Text einfügen. Solltest du die Quellenangabe in JabRef später verändern, werden 
diese Änderungen direkt auch in deinem Dokument übernommen.

Dir stehen ebenfalls die über 9000 Zitierstile wie bei Zotero zur Verfügung. 
Damit steuerst du das Aussehen deiner Quellen und deines Literaturverzeichnis. 
Das lässt sich mit JabRef (bzw. LaTeX) ebenfalls automatisch erstellen, ohne 
dass du dich um die alphabetische Sortierung oder die einzelnen Einträge 
kümmern musst.

*JabRef ist Open-Source-Software und steht für alle Betriebssysteme und viele 
Editoren und Schreibprogramme zur Verfügung.*

!!!! <center><a href=https://www.jabref.org/>https://www.jabref.org/</a></center>

## Hilfe

- Handbuch und Dokumentation (Sprache: Englisch)
  * <https://docs.jabref.org/>
- Zitierstile:
  - <https://www.zotero.org/styles>
