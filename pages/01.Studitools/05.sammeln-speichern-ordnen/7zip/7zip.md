---
title: '7-Zip - Dateien komprimieren'
date: '24-08-2021'
publish_date: '24-08-2021'
taxonomy:
    category:
        - Sammeln
        - Speichern
        - Ordnen
---

![](img/7zip.png)

===

# 7-Zip - Dateien komprimieren 

* Die Datei ist einfach viel zu groß um sie als E-Mail zu verschicken?
* Du hast diese Testversion vom Packprogramm… aber seit 30 Jahren, nicht
  30 Tagen?

7-Zip ist ein Programm, mit dem du deine Dateien in sogenannte "Archive" 
komprimieren ("packen" bzw. verkleinern) oder extrahieren ("entpacken") kannst. 
7-Zip unterstützt eine ganze Reihe an verschiedenen Formaten (neben dem 
`.7z`-Dateiformat):  
das bekannte `.zip` sowie freie Kompressionsformate wie `.tar.gz`,
`.bz2` oder `.tar`. Entpacken kann 7-Zip aber wirklich fast alles - auch Formate 
wie `.rar`, `.iso`.

Falls dir all die Abkürzungen nichts sagen: die brauchst du auch nicht kennen.  
Wichtig ist nur, dass 7-Zip quasi alles packen und entpacken kann, was du jemals
brauchen wirst. Und dass frei zur Verfügung steht - egal ob unter Windows, MacOS
oder Linux.

!! **Hinweis:**  
!! *Das Verfahren für `.7z`-Dateien komprimiert Daten meist noch besser als das 
!! bekannte `.zip`-Format. Welches das richtige Format für deine Dateien ist, kommt 
!! allerdings sehr auf die Dateitypen und den Zweck an. `JPG`-Dateien 
!! beispielsweise bekommst du so nicht kleiner, denn JPG ist schon ein Verfahren, 
!! um Bilddateien zu komprimieren.*

Unter Windows findest du nach der Installation im Kontextmenü (Rechtsklick auf 
eine Datei) Optionen, um Dateien mit 7-Zip zu (ent)packen. Unter Linux und MacOS 
gibt es 7-Zip als Programm für die Kommandozeile. Auch dort findest du aber 
Optionen für das Menü deines Dateimanagers.

!!! **Tipp:**  
!!! *7-Zip kann in einigen Formaten auch deine komprimierten Dateien verschlüsseln (mit dem Standard AES256).
!!! Beim Dialog zum Packen kannst du ein Passwort dafür eingeben, und die Datei wird als verschlüsseltes Archiv angelegt.*

!!!! <center><a href=https://www.7-zip.org>https://www.7-zip.org</a></center>

## Hilfe

- FAQ von 7-Zip (Sprache: EN):
  * <https://www.7-zip.org/faq.html>
