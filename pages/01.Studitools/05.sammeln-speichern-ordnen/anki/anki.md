---
title: 'Anki - Lernen mit deinen eigenen digitalen Karteikarten'
date: '06-11-2020'
publish_date: '06-11-2020'
taxonomy:
    category:
        - Sammeln
        - Speichern
        - Ordnen
---

![](img/anki.png)

===

# Anki - Lernen mit deinen eigenen digitalen Karteikarten

Anki ist ein Programm mit dem du digitale Karteikarten zu lernen erstellen 
kannst ("Anki" bedeutet "Auswendiglernen" auf Japanisch). 

Mit Anki kannst du unterschiedliche Arten von Karten erstellen: von einfachen 
Listen mit Begriffen, Lückentexten, Bildern mit Beschriftung, Video- und 
Audio-Aufnahmen oder sogar mathematischen Formeln (mit LaTeX).  
Diese Karten kannst du anschließend in dein Fragen-Deck laden und dich 
regelmäßig abfragen lassen.

Anki wiederholt dabei die Karten nach einem bestimmten System: alles, was du 
schon als "das kenne ich gut" bewertet hast wird dir seltener angezeigt, 
während schwerere Karten häufiger abgefragt werden. So passt sich die Abfrage 
nach und nach deinem Kenntnisstand an. Außerdem bekommst du nie *alle* Karten 
auf einmal angezeigt - so einen Lernmarathon erspart dir Anki.

Und überhaupt: *du* entscheidest bei Anki, wie viel, wie oft und wie lange du 
wiederholen möchtest. Fast alles lässt sich dabei in Anki deinen Bedürfnissen 
und deinem Lerntempo anpassen.

Die Decks mit Fragen kannst du auch über "AnkiWeb" mit anderen Teilen oder 
einfach als Datei weitergeben. So könntest du auch mit mehreren Personen z.B. 
Inhalte einer Veranstaltung aufarbeiten oder für euren Studiengang ein 
Kartendeck erstellen.  
Außerdem gibt es für Anki jede Menge Erweiterungen (Addons), die viele neue 
Funktionen bereithalten.

!! **Hinweis:**  
!! *Wenn du eine Möglichkeit suchst, dein Wissen interaktiv mit anderen an der Uni Bremen zu messen, schau dir am besten das [Lernduell](https://blogs.uni-bremen.de/studytools/2021/03/07/lernduell-in-stud-ip-fordert-euer-wissen-heraus/) auf Stud.IP an.*

Anki gibt es dabei für alle Plattformen, so dass du mit den Apps für dein 
Smartphone auch unterwegs lernen kannst.

! **Achtung:**  
! *In Googles Playstore und und im freien [F-Droid-Store](https://f-droid.org/) für Android sind kostenlose Apps verfügbar. Im Apple-Store kostet die App Geld. Außerdem gibt es einige Apps, die den Namen "Anki" haben, aber nicht von den offiziellen Entwickler*innen stammen.  
! Informationen dazu gibt es auf den Hilfe-Seiten von Anki.*

!!!! <center><a href=https://apps.ankiweb.net>https://apps.ankiweb.net</a></center>

## Hilfe

- "Getting Started" (Sprache: EN):
  - <https://docs.ankiweb.net/#/getting-started>
- Dokumentation:
  * <https://docs.ankiweb.net/#/> (Sprache: EN)
  * <http://www.dennisproksch.de/anki> (Sprache: DE)

## Karten-Decks und Addons 

- Karten-Decks:
  * <https://ankiweb.net/shared/decks/>
- Addons (Sprache: EN)
  * <https://ankiweb.net/shared/addons/>

### Tutorials

- *"So merkt man sich ALLES - Anki digitale Karteikarten für's Medizinstudium - Spaced Repetition"* (esther liobas; YouTube; Sprache: DE)
  * <https://www.youtube.com/watch?v=JyWc_UJMPlI>
- *"HOW TO USE ANKI. The beginners tutorial I wish I'd had!"* (Clara Elizabeth; YouTube; Sprache: EN)
  - <https://www.youtube.com/watch?v=g8kNH27Pj1c>
- *"Anki Tutorial"* (Rachel Southard; YouTube; Sprache: EN)
  - <https://www.youtube.com/watch?v=e9GtFLAI2RU>
- *"Using LaTeX to Create Anki Flashcards"* (Michelle Krummel; YouTube; Sprache: EN)
  * <https://www.youtube.com/watch?v=bwcr25334J8>

- *"A Quick Guide on Using Anki (effectively) (in an academic context)"* (Stevie Poppe; Sprache: EN)
  * <https://steviepoppe.net/blog/2016/09/a-quick-guide-on-using-anki-effectively-in-an-academic-context/>
