---
title: 'Zotero - Literaturverwaltung für alle'
---

![](img/zotero.png)

===

# Zotero - Literaturverwaltung für alle

Zotero ist ein freies Programm zur Literaturverwaltung und enthält eine Liste 
all deiner Quellen, der Notizen, Exzerpte und Schlagworte dazu.

Zotero verbindet sich mit deinem Browser und deinem Schreibprogramm. Ein Buch 
oder Artikel aus der Uni-Bibliothek kannst du daher einfach aus dem Browser 
direkt in Zotero übertragen. Die Quelle kannst du dann sofort im Text zitieren, 
denn alle Literaturangaben werden von der Webseite übernommen.  
Da Zotero und dein Schreibprogramm miteinander verknüpft sind, übertragen sich 
alle Änderungen auch sofort auf die Quellenangaben im Text und dem 
Literaturverzeichnis.

!!! **Tipp**  
!!! *Falls du schon viele Artikel gespeichert hast aber noch keine Literaturverwaltung verwendest:
!!! Zotero liest beim Import alle Angaben aus den Metadaten oder den DOIs der Artikel ein - und deine Sammlung von PDFs ist ratzfatz in Zotero geordnet.*

Dein Literaturverzeichnis benötigt dank Zotero nur einen Klick:  
"Bibliografie hinzufügen" anklicken und alle zitierten Quellen sind alphabetisch 
sortiert in einem Verzeichnis. Mehr als 9000 unterschiedliche Zitierstile stehen 
dir dabei zur Verfügung. Unter anderem gibt es einen Stil der 
Politikwissenschaften an der Uni Bremen.

*Zotero steht für alle Betriebssysteme und viele Schreibprogramme (z.B. auch [LaTeX](../../schreiben/latex/)) 
zur Verfügung. So kannst du z.B. [einfach zwischen Office und 
Online-Office-Programmen 
wechseln](https://www.zotero.org/blog/move-zotero-citations-between-google-docs-word-and-libreoffice/) 
oder [unter Apples iOS sogar einfach Strichcodes per Foto 
einscannen](https://www.zotero.org/blog/scan-books-into-zotero-from-your-iphone-or-ipad/). 
Plugins erweitern die Funktion von Zotero noch zusätzlich - je nach dem, womit 
du arbeitest und was du brauchst.*

!!! **Tipp:**  
!!! *Du willst nur ganz schnell eine Liste deiner Literatur anlegen? Dazu brauchst du nur deinen Browser und [zoterobib](../zbib/).*

!!!! <center><a href=https://www.zotero.org>https://www.zotero.org</a></center>


## Hilfe

- Hilfe und Dokumentation: 
  - <https://www.zotero.org/support/>
- Zitierstile:
  - <https://www.zotero.org/styles>
- Zotero-Plugins:
  - <https://www.zotero.org/support/plugins>

