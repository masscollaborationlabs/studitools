# Quick-Tip: Markierungen aus PDFs in Zotero-Notizen umwandeln

* Du hast jetzt in der PDF das halbe Buch markiert - aber wie bekommst du die 
  Textstellen in deine Literaturverwaltung 
  [Zotero](https://blogs.uni-bremen.de/studytools/2020/03/17/zotero-literaturverwaltung-fuer-alle/)? 

Mit der Zotero-Erweiterung "ZotFile" kannst du nicht nur Anhänge (also Dateien, 
die du einer Quelle angefügt hast) verwalten, sondern auch Markierungen 
automatisch in Zotero übertragen. Und ganz vieles mehr…

Eine Installationsanleitung findest du auf der Webseite von ZotFile. Danach 
kannst du auf eine PDF-Datei in deiner Literatursammlung klicken und unter 
`Anhänge verwalten > Annotationen herauskopieren` wählen. Nach kurzer Zeit sind 
alle markierten Stellen deiner PDF-Datei als eine Notiz zu der Quelle 
gespeichert.

<http://zotfile.com/>
