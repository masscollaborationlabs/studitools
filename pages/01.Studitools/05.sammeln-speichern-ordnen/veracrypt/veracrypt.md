---
title: 'Veracrypt - Datenträger verschlüsseln'
---

![](img/veracrypt_wizard.jpg)

===

# Veracrypt - Datenträger verschlüsseln

- Du hast Daten eines großen Unternehmens für deine Abschlussarbeit bekommen und musst besonders gut darauf aufpassen?
- Deine Forschungsdaten sind auf deinem USB-Stick… aber wo ist der eigentlich hin? 

Du musst kein\*e Regierungs-Agent\*in sein: auch eine sozialwissenschaftliche Befragung von Personen kann Personen gefährden, wenn sie z.B. Nachteile durch ihre Aussagen fürchten müssen.  

*Auch als wissenschaftlich Arbeitende\*r musst du nicht nur deine Quellen zitieren können, sondern vor allem schützen.  
Damit solche Daten nicht in falsche Hände gelangen, solltest du sie unbedingt verschlüsseln!*

*VeraCrypt* ist ein Programm, mit dem du Datenträger einfach und sicher verschlüsseln kannst: Ordner, USB-Sticks oder ganze Festplatten. Es steht als Open-Source-Programm auf allen Betriebssystemen frei zur Verfügung. Wie VeraCrypt funktioniert, ist also ebenfalls transparent und überprüfbar.

Die Standardeinstellungen von VeraCrypt sind ausreichend gewählt. Eigentlich musst du nur wählen, was du verschlüsseln willst:

- einen "Container" (Teil deines Speicherplatzes), in dem du Daten hinterlegst 
- einen Teil eines USB-Sticks oder einer Festplatte ("Partition")
- den gesamten USB-Stick bzw. Festplatte. 

Der verschlüsselte Bereich lässt sich mit VeraCrypt wie ein USB-Stick bei Bedarf aktivieren und du kannst Daten in deinem gesicherten Bereich speichern oder öffnen.  
Wenn du den verschlüsselten Bereich "auswirfst" (wie einen USB-Stick), dann sind die Daten sicher verschlüsselt. Nur wer das Passwort kennt, kann darauf zugreifen.

! **Anmerkung:**
! *Eine Verschlüsselung ist nur so gut wie das Passwort. Du solltest daher darauf achten, ein möglichst gutes Passwort zu verwenden.*

!! **Achtung:**  
!! *Um auf deine mit VeraCrypt verschlüsselten Daten (z.B. auf einem USB-Stick) mit einem anderen Gerät zuzugreifen, muss dort ebenfalls VeraCrypt installiert sein.*

!!!! <center><a href=https://www.veracrypt.fr/en/Downloads.html>https://www.veracrypt.fr/en/Downloads.html</a></center>

## Hilfe

- Dokumentation von VeraCrypt:
  - <https://www.veracrypt.fr/en/Documentation.html>
- Schnelleinstieg für Anfänger_innen (Sprache: Englisch):
  - <https://www.veracrypt.fr/en/Beginner%27s%20Tutorial.html>

- Videotutorials zu VeraCrypt:
  - <https://www.youtube.com/watch?v=_5c4aJantY0>
  - zweiteiliges Video zur Verschlüsselung mit VeraCrypt:
    - <https://www.youtube.com/watch?v=vpFdngxkCK0>
    - <https://www.youtube.com/watch?v=-rK7GmK-JGg>

- ausführliches Tutorial zu VeraCrypt:
  - <https://www.privacytutor.de/blog/datenschutzhelden/dateiverschluesselung-mit-veracrypt/>
 
### sichere Passwörter

- Video *"Passwörter Einfach Erklärt"* (Alexander Lehmann):
  * <https://vimeo.com/138839266>
- Comic *"Password Strength"* von XKCD:
  * <https://xkcd.com/936/>
- Empfehlungen zu sicheren Passwörter des Bundesamts für Sicherheit in der Informationstechnik (BSI)
  * <https://www.bsi-fuer-buerger.de/BSIFB/DE/Empfehlungen/Passwoerter/passwoerter_node.html>

### Generell: Warum sollten Dateien auf dem eigenen Gerät verschlüsselt werden?

- *"Protect the sensitive files on your computer"* (Tactical Technology Collective and Front Line Defenders; Sprache: Englisch)
  - <https://securityinabox.org/en/guide/secure-file-storage>
