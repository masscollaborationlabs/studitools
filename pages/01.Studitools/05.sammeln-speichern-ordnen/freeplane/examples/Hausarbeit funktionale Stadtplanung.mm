<map version="freeplane 1.8.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Hausarbeit funktionale Stadtplanung" FOLDED="false" ID="ID_1302109022" CREATED="1584223425088" MODIFIED="1607731304676" STYLE="oval" VGAP_QUANTITY="0.0 pt">
<font SIZE="18"/>
<hook NAME="MapStyle" zoom="0.826">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans" SIZE="24"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
<stylenode LOCALIZED_TEXT="default" COLOR="#000000" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans" SIZE="10" BOLD="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans" SIZE="9"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" TEXT_ALIGN="LEFT" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans"/>
<edge STYLE="hide_edge" COLOR="#909090" WIDTH="2"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans" SIZE="10" BOLD="true"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans" SIZE="10" BOLD="true"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans" SIZE="10" BOLD="true"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<icon BUILTIN="yes"/>
<font NAME="IBM Plex Sans"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans" SIZE="18"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans" SIZE="16"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans" SIZE="14"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans" SIZE="12"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans" SIZE="10"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="6.0 pt" SHAPE_VERTICAL_MARGIN="3.0 pt" VGAP_QUANTITY="5.0 pt" BORDER_WIDTH="2.0 px">
<font NAME="IBM Plex Sans"/>
<edge STYLE="bezier" COLOR="#909090" WIDTH="2"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Jane Jacobs" LOCALIZED_STYLE_REF="defaultstyle.floating" POSITION="right" ID="ID_421848659" CREATED="1589394617869" MODIFIED="1607731265547" HGAP_QUANTITY="572.1702011560845 pt" VSHIFT_QUANTITY="-59.74467963878795 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="IBM Plex Sans" DESTINATION="ID_934682968" MIDDLE_LABEL="kritisiert" STARTINCLINATION="9;171;" ENDINCLINATION="-45;-72;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<hook NAME="FreeNode"/>
<node TEXT="Wer war Jane Jacobs?" ID="ID_1927437730" CREATED="1589395014786" MODIFIED="1589395026363"/>
<node TEXT="Wo hat sie gelebt?" ID="ID_1426380804" CREATED="1589395026890" MODIFIED="1589395035795"/>
<node TEXT="Was genau hat sie untersucht?" ID="ID_51241949" CREATED="1589395036225" MODIFIED="1589395040867"/>
</node>
<node TEXT="Gliederung" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="right" ID="ID_861090604" CREATED="1589395192388" MODIFIED="1607731301316" HGAP_QUANTITY="39.27659523179488 pt" VSHIFT_QUANTITY="42.127658719658164 pt">
<node TEXT="Einleitung" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_265766307" CREATED="1589395134611" MODIFIED="1589395357080">
<node TEXT="Gegenstand" ID="ID_958518458" CREATED="1589395160139" MODIFIED="1589395166004">
<node TEXT="Was ist funktionale Stadtplanung?" ID="ID_1206578890" CREATED="1589395167027" MODIFIED="1589395174284"/>
</node>
</node>
<node TEXT="Hauptteil" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_958023122" CREATED="1589395138051" MODIFIED="1589395357081"/>
<node TEXT="Schluss" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_789548889" CREATED="1589395140795" MODIFIED="1589395357082"/>
<node TEXT="Literatur" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1888253057" CREATED="1589395142978" MODIFIED="1589395357078">
<node TEXT="Jane Jacobs" ID="ID_1728606039" CREATED="1589395146867" MODIFIED="1589395149428"/>
<node TEXT="Charta von Athen" ID="ID_367987675" CREATED="1589395234291" MODIFIED="1589395237718"/>
</node>
</node>
<node TEXT="funktionale Stadtplanung" LOCALIZED_STYLE_REF="defaultstyle.floating" POSITION="right" ID="ID_934682968" CREATED="1589394702573" MODIFIED="1607731304675" HGAP_QUANTITY="737.6170063096512 pt" VSHIFT_QUANTITY="128.68084845277406 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="IBM Plex Sans" DESTINATION="ID_1037180203" MIDDLE_LABEL="Rückwirkung auf?" STARTINCLINATION="-597;83;" ENDINCLINATION="-202;-30;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<hook NAME="FreeNode"/>
<node TEXT="Was ist das?" ID="ID_401365460" CREATED="1589394708623" MODIFIED="1589394712705"/>
<node TEXT="Wer proklamiert das?" ID="ID_168163561" CREATED="1589394713399" MODIFIED="1589394718521"/>
<node TEXT="Wann war das aktuell?" ID="ID_1633925099" CREATED="1589394719519" MODIFIED="1589394723914"/>
<node TEXT="Vorhaben und Umsetzungen" ID="ID_413610347" CREATED="1589394846176" MODIFIED="1589394852882">
<node TEXT="Positiv-Beispiele (aus heutiger Sicht?)" ID="ID_946535727" CREATED="1589394832089" MODIFIED="1589394844314"/>
<node TEXT="negativ-Beispiele (aus heutiger Sicht?)" ID="ID_1888104391" CREATED="1589394858353" MODIFIED="1589394868138"/>
</node>
</node>
<node TEXT="Stadtplanung heute" LOCALIZED_STYLE_REF="defaultstyle.floating" POSITION="right" ID="ID_1037180203" CREATED="1589394727870" MODIFIED="1589395221467" HGAP_QUANTITY="593.617009231547 pt" VSHIFT_QUANTITY="359.99999269526074 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="3 3" FONT_SIZE="9" FONT_FAMILY="IBM Plex Sans" DESTINATION="ID_934682968" MIDDLE_LABEL="bezieht sich heutige Stadtplanung &#xa;noch auf funktionale Stadtplanung?" STARTINCLINATION="10;-101;" ENDINCLINATION="-138;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<hook NAME="FreeNode"/>
<node TEXT="Ist funktionale  Stadtplanung noch Aktuell?" ID="ID_146790930" CREATED="1589394819744" MODIFIED="1589394929403"/>
<node TEXT="Gibt es bestimmte Bereich, in denen funktionale Stadtplanung umgesetzt wird?" ID="ID_1014063209" CREATED="1589394934673" MODIFIED="1589394950227"/>
<node TEXT="Sind neue Städt in China nach Konzepten funktionaler Stadtplanung erstellt?" ID="ID_1717351668" CREATED="1589394951193" MODIFIED="1589394977243"/>
</node>
</node>
</map>
