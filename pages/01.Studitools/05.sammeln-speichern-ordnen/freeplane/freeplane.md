---
title: 'Freeplane - mit Mind-Maps Gedanken kartieren'
date: '14-05-2020'
publish_date: '14-05-2020'
taxonomy:
    category:
        - Sammeln
        - Speichern
        - Ordnen
---

![](img/freeplane_2.png)

===

# Freeplane - mit Mind-Maps Gedanken kartieren

- So viele Begriffe in dem Buch… dir raucht der Kopf?
- Wie hängen all diese Dinge eigentlich zusammen?
- Du hast mal eine Mind-Map auf Papier gemalt und willst daraus deine Hausarbeit erstellen? 

Freeplane ist ein frei verfügbares Programm, mit dem du Mind-Maps und Concept-Maps erstellen kannst - aber es kann noch sehr viel mehr!

Mind-Maps und Concept-Maps sind assoziativ erstellte "Karten" aller Dinge, Zusammenhänge und Notizen, die dir gerade zu einem Thema einfallen. Mit diesem Überblick ist es viel leichter, sich die Zusammenhänge vorzustellen und ein Thema zu strukturieren. So kommst du vom Brainstorming schnell zu einer Struktur.

Mit all diesen Begriffen und Zusammenhängen hast du die nötigen Zutaten für deine nächste Hausarbeit schon in einer Gliederung, denn:  Freeplane kann deine Mind- und Concept-Maps als Office-Datei speichern - zum Beispiel für [LibreOffice](../../schreiben/libreoffice/).

Und schwupps, sind deine losen Gedanken zur fertigen Gliederung mitsamt Notizen und Quellen geworden!

*Damit du beim speichern als Office-Datei auch Überschriften mit den Absatz-Vorlagen hast, musst du in Freeplane die jeweiligen "Knoten" mit der Vorlage für Überschriften ("Level 1", "Level 2" etc.) einstellen.*

!!!! <center><a href=https://www.freeplane.org/wiki/index.php/Home>https://www.freeplane.org/wiki/index.php/Home</a></center>

!! **Achtung**\
!! *Um Freeplane zu nutzen, musst du vorher [Java](https://www.java.com/de/) installieren.*

! **Hinweis:**\
! *Freeplane ist das Nachfolge-Projekt von Freemind, einem bekannten und 
! ebenfalls bekannten Mind-Mapping-Programm. Beide Programme stammen vom selben 
! Entwickler, sind zueinander kompatibel und werden auch von vielen anderen 
! Mind-Mapping-Programmen unterstützt.*

## Hilfe

- eine Hilfe ist in Freeplane als Mind-Map integriert
- [*"Mindmapping mit Freeplane - Zusammenhänge schnell sichtbar machen"*](https://www.youtube.com/watch?v=hgnhWwR3qyM) von *Markus Oppitz* (CC-BY-NC-SA 4.0)
- Zu Mind-Maps und Concept-Maps findest du etwas bei den [Materialien der Studierwerkstatt](https://blogs.uni-bremen.de/studytools/2020/03/12/materialien-der-studierwerkstatt-uni-bremen-zum-wissenschafltichen-arbeiten/)
- "Mindmaps erstellen mit Freeplane" (Zentrale Studienberatung an der Uni Paderborn; YouTube)
  * <https://www.youtube.com/watch?v=F7K5xT8s-uQ>
- Videos zu Mind- und Concept-Maps generell:
  - "Strukturieren mit Mind Maps" (Studierwerkstatt Uni Bremen; YouTube)
    - <https://www.youtube.com/watch?v=ieXahxhoIsI> 
  - "Concept Map als Lese- Lern- und Schreibtechnik" (Studierwerkstatt Uni Bremen; YouTube)
    - <https://www.youtube.com/watch?v=qytZgdHqDlw> 


