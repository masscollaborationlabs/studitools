---
title: Liste aller Tools
---

# Liste aller Tools

## 0-9

* 7-Zip – Dateien komprimieren

## A

* Untertitel erstellen – mit Aegisub
* Anki – Lernen mit deinen eigenen digitalen Karteikarten
* Ardour – dein Tonstudio
* Projekte dokumentieren — mit AsciiDoc
* Audio aufnehmen und schneiden mit Audacity

## B

* Blender – 3D-Modelle und Animationen
* Meetings mit BigBlueButton – teilnehmen auch ohne Uni-Account
* Meetings in Studiengruppen mit BigBlueButton
* Der Bremer Schreibcoach – Ratgeber für wissenschaftliches Schreiben im Studium und darüber hinaus

## C

* Dateien verschicken – mit croc

## D

* Darktable – Foto-Entwicklung in deiner digitalen Dunkelkammer
* DFN-Terminplaner – Umfragen und Abstimmungen

## E

## F

* Firefox – ein gemeinnütziger Browser
* FreeCAD – technische Zeichnungen anfertigen, Bauteile und Maschinen planen

* Freeplane – mit Mind-Maps Gedanken kartieren

## G

* GIMP – Bildbearbeitung für jeden Zweck

## H

## I

* Inkscape – Vektorgrafiken für Logos, Layouts, Laser-Cutter

## J

* planen, plaudern, präsentieren – mit Jitsi-Meet

* Jupyter – Entwicklungsumgebung und interaktives Notizbuch für viele Programmiersprachen

## K

* Kanopy – Filme streamen über die SUUB
* Krita – Malerei in digital

## L

* LanguageTool – Rechtschreib- und Grammatik-Prüfung
* LaTeX – Textsatz für Wissenschaft, Buchdruck und Typograf*innen
* Lernräume an der Uni Bremen
* Lernvideos des ZMML zum wissenschaftlichen Arbeiten
* LibreOffice – (d)ein Office für alle
* LyX – LaTeX-Editor für Einsteiger*innen

## M

* Mailinglisten für Gruppen und Projekte
* Markdown – Einfach. Gut. Schreiben.
* Materialien der Studierwerkstatt zum wissenschaftlichen Arbeiten
* Mixxx – DJ-Software für Live-Mix bis Radio-Show
* MyPaint – dein digitales Kunst-Atelier

## N

## O

* Online-Selbstlernkurs der Universität Halle zum wissenschaftlichen Arbeiten
* OnlyOffice – gemeinsam Office-Dokumente in Seafile bearbeiten
* OBS Studio – dein Studio für Live-Streaming und Videoaufnahmen
* OpenShot – Filme schneiden leicht gemacht

* OpenStreetMap – freie Weltkarte für jeden Zweck
* Transkribieren mit oTranscribe
* Abschlussarbeit publizieren – Open Access über die SUUB, nicht “für die Tonne”!

## P

* PlantUML – Diagramme von Projektplanung bis Programmcode

## Q

## R

* Fotos entwickeln – mit RAWTherapee
* Instant-Messaging mit Rocket.Chat
* Statistik und Daten-Analyse mit R und Rstudio
* Sprachkurse mit „Rosetta Stone“

## S

* Schreibportal für die Geistes-, Kultur- und Sprachwissenschaften (FB 09 / 10)
* Printmedien gestalten – mit Scribus
* Campus-Cloud: Seafile an der Uni Bremen
* Shotcut – Videos schneiden für Fortgeschrittene
* Gruppenarbeit? Studiengruppen auf Stud.IP nutzen!
* StudIPads – gemeinsam texten in Stud.IP
* Syncthing – deine Cloud ohne Cloud

## T

* Taguette – Texte und Interviews Kodieren
* Große Dateien versenden – mit dem Temporären Dateicontainer vom ZfN
* TeXstudio – umfangreicher Editor für LateX
* Thunderbird – Ein Programm, sie alle zu verwalten… E-Mails, Kalender und Aufgaben

## U

* Ublogs – einfach bloggen an der Uni

## V

* Datenträger verschlüsseln mit VeraCrypt
* VLC – für alle(s) mit Medien

## W

* ZfN-Webmail – E-Mails, Kalender, Aufgaben und Dateien zu deinem Uni-Account
* Wekan – ein Kanban für deine Projekte
* Wilmas Tutorials

## X

* Instant-Messaging mit XMMP („Jabber“)
* Der digitale College-Block: Xournal++

## Y

## Z

* Zettlr – der digitale Zettelkasten
* Zim – das Desktop-Wiki für deine Notizen
* Zotero – Literaturverwaltung für alle
* zoterobib – Literaturverzeichnisse schnell erstellen

