# Aufgabenliste - Todos in Zim verwalten

* Heute dies, morgen das… aaaah!

Hey, ganz ruhig! Eine Hektik kommt immer noch *nach* der Anderen!\
Und damit du nicht durchdrehst, sondern dir lieber Notizen schreibst, gibt es ja 
[Zim](https://blogs.uni-bremen.de/studytools/2020/05/14/zim-das-desktop-wiki-fuer-deine-notizen/)!
Zim bringt (neben anderen Erweiterungen) eine Aufgabenliste mit. Und mit der 
kannst du Checklisten und die Daten für deine Projekte festlegen. 

So ein Quark!

## Erweiterung aktivieren

Die Erweiterung musst du zunächst unter ` Bearbeiten > Einstellungen…` im Reiter 
`Erweiterungen` aktivieren. Dort findest du auch eine Schaltfläche, um die 
Erweiterung zu konfigurieren -- etwa, in welchem Bereich von Zim deine 
Aufgabenliste zu sehen sein soll.\

**Hinweis:**\
*Die Hilfe zur jeweiligen Erweiterung findest du ebenfalls bei den Einstellungen 
im gleichen Reiter.*

## Aufgaben anlegen und abhaken

Um Aufgaben anzulegen, schreibst du zwei eckige Klammern: `[]`. Mit einem 
Leerzeichen hinter den Klammern wandelt Zim diese nun in ein Kästchen zum 
Abhaken (Checkbox) um.\
Klickst du mehrmals auf eine Checkbox, gibt es unterschiedliche Markierungen 
("abgehakt", "durchgekreuzt", "verschoben" (Pfeil rechts) und "verschoben" 
(Pfeil links)).

## Unter-Aufgaben anlegen und priorisieren

Mit Einrückungen (`Tab`) strukturierst du deine Aufgabenliste und legst 
unter-Aufgaben an. Dabei kannst du auch jederzeit zwischen Checkboxen und 
normalen Listen (die mit einem `*` als Punkt anfangen) wechseln. So kannst du 
z.B. Details zu einer Aufgabe notieren.

Allen Aufgaben kannst du mit 1 bis 3 Ausrufungszeichen ("!") Prioritäten 
zuweisen (es können auch mehr verwendet werden; "!!!!!" zählt mehr als "!!!", 
wird in der Aufgabenliste aber nicht anders markiert.).

**Tipp:**\
*Du kannst auch nur bestimmte Teile deines Wikis als Aufgaben indizieren lassen. 
So kannst du z.B. nur einen bestimmten Projektbereich in der Aufgabenliste als 
Aufgaben anzeigen lassen, während deine normalen Checklisten an anderer Stelle 
ignoriert werden.*

## Startzeiten und Deadlines setzen

Brauchen deine Aufgaben Anfangszeiten und Deadlines, legst du diese mit einem 
Datum fest. Dabei benutzt Zim vor allem das englische Datumsformat: 
`JAHR-MONAT-TAG`, also z.B. 2021-09-25.

* mit einem `<` davor wird dein Datum zu einer Deadline: `<2021-09-30`
* mit einem `>` davor wird das Datum als Startzeitpunkt gesetzt: `>2021-10-01`

## Aufgaben wiederfinden

Ist deine Aufgabenliste unendlich riesig? Filter sie doch!\
Ganz unten im Fensterbereich der Aufgabenliste gibt es ein Eingabefeld, in dem 
du nach beliebigen Wörtern suchen kannst. Das ist besonders praktisch, wenn du 
mit eigenen Bezeichnungen oder den Tags von Zim arbeitest, um die Aufgaben 
bestimmten Projekten oder Personen zuzuordnen.

**Tipp:**\
*Zim hat auch eine Erweiterung für Tags, die du unbedingt nutzen solltest! Damit 
kannst du nicht nur deine Aufgabenliste strukturieren, sondern auch alle Seiten 
in Zim verschlagworten und dadurch einfacher finden.*
