# Lesezeichen im Browser

* Du hast viel recherchiert und brauchst später alle Webseiten wieder - aber 
  jetzt musst du ganz schnell los?
* Du suchst ständig nach denselben Seiten in einer Suchmaschine?
* Du hast 79 Tabs auf… und alle sind irgendwie wichtig! Aber dein Laptop ist 
  jetttzzzzztttt ssooooooo llllaaaaaannnnnnnngggsssssaaaammmmmm??????

Setz' dir die Webseiten doch als Lesezeichen in deinem Browser! 

Du fragst ja auch deine Freunde nicht jedes Mal auf dem Weg zur Mensa, wo denn 
die Mensa ist, oder? ;)

*  **Es spart dir Zeit:**
  + Du weißt wo es hingehen soll. Die ersten paar Buchstaben eingeben, `Enter` 
    drücken - und du bist da.
*  **Es ist praktisch:**
  + …denn deine Lesezeichen kannst du in Ordnern organisieren und mit 
    Schlagworten thematisch ordnen. Lesezeichen in Ordnern kannst mit einem 
    Klick alle wieder aufrufen.
*  **Es ist umweltfreundlich:**
  + Wenn du eine Webseite erst über eine Suchmaschine suchst, kostet dich das 
    deine Zeit und im Rechenzentrum der Suchmaschine Strom. So verschwendest du 
    unnötig Ressourcen. 
*  **Es schützt deine Privatsphäre:**
  + …denn eine Suchmaschine erfährt sonst bei jeder Suche, für was du dich 
    interessierst, welches Gerät du verwendest oder wo du dich befindest.

Wie du deine Lesezeichen (manchmal auch "Favoriten" genannt) verwaltest und die 
Lesezeichen-Leiste im Browser aktivierst, erfährst du auf der Hilfe-Seite 
deines jeweiligen Browsers.

**Tip:**\
*Auch auf deinem Smartphone kannst du in deinem Browser Lesezeichen anlegen 
oder Webseiten sogar als Starter auf deinem Startbildschirm bei den Apps 
speichern. Damit kannst du dir manchmal sogar die ein oder andere App sparen - 
schließlich kannst du manchmal auch einfach auf der Webseite die Informationen 
finden.*
