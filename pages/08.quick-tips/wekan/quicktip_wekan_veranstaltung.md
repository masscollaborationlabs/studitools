# Veranstaltungen planen und Dokumentieren - mit Wekan

Mit dem digitalen Kanban-Board 
[Wekan](https://blogs.uni-bremen.de/studytools/2021/07/13/wekan-ein-kanban-fuer-deine-projekte/) 
lassen sich nicht nur Aufgaben und Projekte managen - auch Veranstaltungen 
kannst du damit planen!

Zugegeben: dafür ist Wekan eigentlich ein bisschen zweckentfremdet…\
Aber wer gesehen hat, wie andere Board-Dienste verwendet werden, wird sich hier 
kaum erschrecken. Vor allem bietet Wekan wirklich nützliche Features dafür!

Wir haben dazu mal ein Testboard für eine fiktive mehrtägige Veranstaltung 
angelegt:

- <https://wekan.zfn.uni-bremen.de/b/uM8hMcPsXqE68Zzqs/veranstaltungsboard>

**Wichtig:**\
*Damit die Veranstaltungsplanung öffentlich sichtbar ist, muss das Board 
ebenfalls öffentlich sichtbar sind!*

## Umsetzung

Tagesplan mit Listen:
: Pro Tag der Veranstaltung nutzen wir hier eine Liste von "Aufgaben". Die 
einzelnen Listen sind nach den Tagen benannt bzw. können mit Datum beschriftet 
werden.

Unterschiedliche Räume / Orte mit Swimlanes:
: Um bei mehreren Veranstaltungs-Räumen den Überblick zu behalten, lassen sich 
die Swimlanes nutzen. Jede Swimlane steht dabei für einen Raum, in dem an den 
verschiedenen Tagen (Liste) Veranstaltungen eingetragen werden können.

Workshops beschreiben mit Markdown:
: Jeder Workshop ist eine "Karte" (bzw. "Aufgabe"). Für Beschreibungstexte 
lässt sich Markdown-Syntax nutzen. Damit lassen sich die Texte mit einfachen 
Formatierungen, wie `**fett**` und `*kursiv*` versehen. Außerdem lassen sich 
Links (`[Bezeichnung](https://www.adresse.de)`) setzen oder Listen erstellen.

Bilder:
: Um einzelne Veranstaltungen zu bebildern, kann ein Screenshot erstellt 
werden, der beim Datei-Upload zu einer Karte `aus der Zwischenablage` eingefügt 
werden kann (`Strg+v`). Damit erscheint das Bild direkt über der Beschriftung 
von Workshops.

Zeiten für den Ablaufplan / Kalender einstellen:
: Start- und Endzeiten der Workshops sind als Start- und Fälligkeitsdatum 
eingestellt. Damit tauchen die Zeiten der Workshops und ihr Titel (eigentlich 
also die Aufgaben und deren Daten) später in der Kalender-Ansicht auf. Einziger 
Nachteil: in der Listen-Ansicht des Kalenders steht leider "Fällig" hinter den 
Terminen.

Themen und Schwerpunkte labeln:
: Für unterschiedliche "Tracks" oder Themenschwerpunkte bei Veranstaltungen 
lassen sich die Labels von Wekan nutzen. Jedes Label kann mit einer Farbe 
versehen werden. Das erleichtert die Navigation während der einzelnen Tage und 
die Workshops lassen sich in der Ansicht "Filter" nach Themen filtern.

Materialien:
: einzelne Dateien und Dokumente lassen sich als Dateien einem Workshop (also 
einer Karte) hinzufügen. 

**Tipp:**\
*Für größere Dateien kann dabei auch z.B. ein Link via 
[Seafile](https://blogs.uni-bremen.de/studytools/2020/04/01/datei-austausch-seafile-an-der-uni-bremen/) 
genutzt werden. Links können hier auch für einen bestimmten Zeitraum 
freigegeben werden.*

Suchen & Finden:
: Nach einem entsprechenden Label oder einer Person lässt sich in den Filtern 
oder in der Suche stöbern. Alternativ kann über die Suche auch einfach normaler 
Text eingegeben werden. 

## Pro & Contra

- **Pro:**
  * tolle Filter-Funktionen erleichtern die Übersicht über eine Veranstaltung
  * Inhalte können gut aufbereitet präsentiert werden
  * Zusammenarbeit an einem Board möglich (sofern ein Zugriff auf Wekan über 
    die Uni besteht)
  * Kalender-Ansicht lässt sich als Programmübersicht nutzen: entweder als 
    Kalender-Ansicht für Tage oder Tabelle aller Termine 
  * Swimlanes könnten z.B. für unterschiedliche Räume an den Tagen genutzt 
    werden 
  * Dateien können hochgeladen werden (bleiben aber bei der Uni / innerhalb der 
    EU); große Dateien lassen sich über Uni-interne Infrastruktur (Seafile) 
    teilen
- **Contra:**
  * für die gemeinsame Bearbeitung des Boards ist ein Account bei der Uni 
    Bremen nötig
  * kein eigenes Hintergrundbild möglich
