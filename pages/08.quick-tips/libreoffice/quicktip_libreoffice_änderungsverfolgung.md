# Effizient Texte überarbeiten: Änderungen aufzeichnen, kommentieren und vergleichen

- Diese ganzen Korrekturen aus den einzelnen Dokumenten deiner Freunde… wie 
  sollst du da nur den Überblick behalten?
- Der geniale Satz von letzter Woche… für immer gelöscht?
- Die Notiz, wie doof deine Dozentin ist, im Fließtext vergessen?

In 
[LibreOffice](https://blogs.uni-bremen.de/studytools/2021/03/05/effizient-texte-ueberarbeiten-aenderungen-aufzeichnen-kommentieren-und-vergleichen/) 
kannst du alle Änderungen am Dokument aufzeichnen. Damit siehst du, wie du 
(oder andere) den Text verändert hast. Wenn du Texte von anderen Korrekturlesen 
lässt, ist diese Funktion enorm praktisch.

Jede veränderte Zeile wird mit einem kleinen Strich markiert. So hast du immer 
den Überblick, welche Zeilen verändert wurden. Alle Änderungen in Buchstaben, 
Wörtern oder Format werden dir mit farbigen Zeichen angezeigt. Du kannst dir 
die Änderungen außerdem anzeigen lassen oder sie verstecken - je nachdem, in 
welchem Arbeitsschritt du gerade bist.

**Hinweis:**\
*Wichtiger Unterschied: `Änderungen aufzeichnen` aktiviert die Funktion, alle 
Änderungen aufzuzeichnen. `Änderungen anzeigen` bedeutet nur, die Änderungen 
(nicht mehr) sichtbar zu machen, während du z.B. den Text überarbeitest.* 

## Änderungen annehmen… oder auch nicht

Alle Änderungen kannst du danach im Dokument anklicken (mit einem Rechtsklick) 
und jeweils einzeln annehmen oder verwerfen.\
Falls dir das zu mühsam ist: alternativ kannst du das auch mit einer Liste 
machen, die du nach Merkmalen der Änderungen sortieren kannst - z.B. nur 
Änderungen einer bestimmten Person oder eines Datums. Dort kannst du sogar 
Kommentare zu jeder Änderungen hinterlegen. Dadurch kannst du anderen z.B. jede 
Änderung begründen.

## Kommentare zu Textstellen hinterlassen

Außerdem kannst du Kommentare einfügen, die sich auf einzelne Zeichen, Wörter 
oder Absätze beziehen. Das ist besonders hilfreich, wenn du dir eine Notiz beim 
Bearbeiten hinterlegen willst. Am Ende kannst du sie auch einfach wieder 
löschen und sie sind garantiert nicht in deinem Fließtext sichtbar.

## Text korrigiert zurückbekommen… aber was hat sich geändert?

Deine Arbeit hast du einer Person gegeben, die sie dir nun korrigiert 
zurückgeschickt hat?\
Du kannst auch einfach ganze Dokumente vergleichen lassen. Einfach deine Datei 
öffnen, `Dokument vergleichen…` anklicken - und schon siehst du alles, was sich 
am Text verändert hat (auch, wenn du die Änderungsverfolgung mal vergessen 
haben solltest).

##  Versionen: dem eigenen Text beim Wachsen zusehen

Und wenn du selbst ab und an einen Zwischenstand deiner Arbeit speichern 
willst, den du auch später wieder aufrufen kannst: auch das geht!\
Unter `Datei > Versionen` kannst du Versionen deines Textes (mit einem 
Kommentar dazu) speichern. So kannst du sehen, wie dein Text nach und nach 
wächst - und notfalls eine Version wiederherstellen.\

**Hinweis:**\
*Die Funktion `Versionen` steht nur bei OpenDocument-Dateien (ODT) zur 
Verfügung. Das DOCX-Dateiformat unterstützt diese Funktion nicht.\
Die Versionen werden alle innerhalb der Datei gespeichert. Die Funktion lässt 
bei viel Inhalt die Dateigröße also möglicherweise schnell sehr groß werden. 
Das kann im Extremfall zu längeren Ladezeiten oder Abstürzen führen.*

## Und wo finde ich jetzt die Funktionen?

Änderungen aufzeichnen:
: Die Funktion `Änderungen aufzeichnen` gibt es als eigene Toolbar unter 
`Ansicht > Symbolleisten > Änderungen`) oder unter `Bearbeiten > Änderungen`. 

Kommentare:
: Die Kommentar-Funktion findest du unter `Einfügen > Kommentar` (vorher den 
gewünschten Text-Teil markieren).

Dokumente vergleichen:
: Um Dokumente zu vergleichen klicke auf `Einfügen > Änderungen > Dokument 
vergleichen` 

Versionen:
: Versionen kannst du unter `Datei > Versionen` speichern und ansehen. 

**Hinweis:**\
*Nicht verzweifeln, wenn du nicht mit LibreOffice arbeitest! Fast alle 
Funktionen sollte es auch in jedem anderen Office-Programm geben.*

## Hilfe

- Änderungen aufzeichnen:
  * <https://help.libreoffice.org/latest/de/text/shared/guide/redlining_enter.html>
- Kommentare:
  * <https://help.libreoffice.org/latest/de/text/shared/01/04050000.html>
- Vergleichen verschiedener Dokumentversionen:
  * <https://help.libreoffice.org/latest/de/text/shared/guide/redlining_doccompare.html>
- Versionen:
  * <https://help.libreoffice.org/latest/de/text/shared/01/01190000.html>
