# Formatvorlagen in Office-Programmen - "Office! Bei Fußnote! Such die Überschrift!"

* Ist das eigentlich Arial 11 oder Arial 12?
* Dieses Inhaltsverzeichnis ist das Schlimmste! Alles immer einrücken - und es 
  passt nie!
* Aaarrrrgh! Diese Seitennummern!!
* Das DECKBLATTTTT!!!! F\*\*\*\*\*!!!!!

Kennste?\
Muss nicht sein.

Damit deine Hausarbeiten, Blog-Einträge, Magazintexte, Briefe, Bücher, 
Abschlussarbeiten etc. dich nicht schon mental in Frührente gehen lassen gibt 
es Formatvorlagen. Was das ist?

Dein Computer ist leider sehr… dumm. Aber er ist sehr schnell.\
Wenn du etwas möchtest, musst du also leider *sehr genau* sagen, *was* du 
möchtest. Aber dann… geht's ab!

Formatvorlagen sind ein bisschen wie Hunde erziehen, nur eben in Office: drauf 
zeigen, bedeutungsvoll "Überschrift" sagen und ein Klickerli geben.\
Wenn dein Office das gelernt hat, kann es Überschriften erkennen.

Du hast bestimmt schonmal diese Menüs oder Schaltflächen in deinem 
Office-Programm mit "Standard", "Überschrift 1", "hervorgehoben" gesehen. 
Vielleicht hast du sie auch noch nie wirklich beachtet… das sind 
Formatvorlagen. Wenn du darauf klickst, sieht dein Text plötzlich anders aus. 

## "Achsoo! Aber die sehen hässlich aus. Die Schriftart ist falsch und… wozu überhaupt die Mühe!?"

### Vorteil 1: Einstellungen an einer Stelle, statt immer alles neu Einstellen

Mit einem Rechtsklick auf eine Vorlage bekommst du ein Menü, in dem du die 
Einstellungen einer Vorlage ändern kannst. Das Tolle ist: Änderungen übertragen 
sich sofort auf alle Teile deines Dokuments, die ebenfalls diese Vorlage 
nutzen. Alle Absätze mit "Überschrift 1" übernehmen also automatisch das 
Aussehen, Schriftart, Abstände….\
Nachträglich deine Doktorarbeit von "Times New Roman" auf "Comic Sans" 
umstellen? Ein Klacks!

### Vorteil 2: ein einheitlicher Look

Du hast bestimmt schon den Fehler gehabt, unterschiedliche Schriftarten oder 
Schriftgrößen mitten in deinem Fließtext zu sehen. Das ist mit Formatvorlagen 
vorbei. Jeder Fließtext bekommt die entsprechende Vorlage und sieht dann exakt 
gleich aus.

**Wichtig:**\
*Dein Office nimmt an, dass du schon wissen wirst, was du tust. Deshalb 
"überschreibt" jede Änderung, die du selbst an einem Wort oder Absatz 
vorgenommen hast die Einstellungen einer Formatvorlage!\
Du solltest also bei z.B. kopierten Texten unbedingt darauf achten, alle 
"manuellen Formatierungen" zu entfernen!*

### Vorteil 3: Auf einmal ändert sich jede Kleinigkeit Automagisch! 

Dein Office kann unheimlich viele Tricks, wenn es gut erzogen ist:

Kapitel-Nummerierung:
: Abschnitte und Kapitel sollen ja meist nummeriert sein. Statt das selbst 
einzugeben, kannst du einfach die automatische Nummerierung deiner Kapitel 
aktivieren (bei 
[LibreOffice](https://blogs.uni-bremen.de/studytools/2020/03/17/libreoffice-dein-office-fuer-alle/): 
`Extras > Kapitelnummerierung`). So ändert sich die Nummer automatisch, sobald 
du Kapitel einfügst oder löschst. Dein Office kann ja jetzt einfach alle 
"Überschrift 1" zählen…

Kapitel neu Strukturieren:
: …und weil es alle Überschriften als solche erkennt, weiß es auch welcher Text 
alles zu welcher Überschrift gehört. So kannst du ganz schnell alles 
umstrukturieren - bis zur nächsten Überschrift. Ein Unterkapitel kannst du mit 
einem Klick ein Kapitel höher rücken, eine Überschrift einfach als Unterkapitel 
festlegen. Dazu gibt es meist eine spezielle Gliederungs-Ansicht (bei 
[LibreOffice](https://blogs.uni-bremen.de/studytools/2020/03/17/libreoffice-dein-office-fuer-alle/)im 
"Navigator" bzw. in der Seitenleiste).

Verzeichnisse:
: Das Inhaltsverzeichnis - ein leidiges Thema? Not anymore!\
  Office kennt ja jetzt deine Überschriften. `Einfügen > Verzeichnis > 
  Verzeichnis`, `Inhaltsverzeichnis` auswählen. Fertig.\
  Geht auch mit Tabellen- und Abbildungsverzeichnissen.

Literaturverzeichnis:
: Das solltest du 
[Zotero](https://blogs.uni-bremen.de/studytools/2020/03/17/zotero-literaturverwaltung-fuer-alle/) 
überlassen. Quellen, die du mit Zotero in deinem LibreOffice im Texte zitierst 
kannst du dort mit einem Klick als Literaturverzeichnis einfügen. Wenn du dann 
in Zotero z.B. einen Autor*innennamen änderst, ändert der sich ebenfalls im 
Text.

Querverweise:
: In Büchern wird oft auf ein anderes Kapitel, Seite oder Abbildung verwiesen, 
z.B. "(siehe Kapitel 1.2. wichtige Grundlagen)", "auf Seite XY" oder "siehe 
unten". Sobald dein Office Überschriften erkennt ist, kannst du darauf einen 
Querverweis setzen. Das ist ein Link, der dich direkt zu dem Kapitel springen 
lässt. Und wenn sich die Nummer des Kapitels oder der Name ändert, geschieht 
das überall dort, wo du einen Querverweis gesetzt hast. Das geht für Seiten, 
Abbildungen, Tabellen… alles was mit einer Vorlage einstellbar ist.

Alles soll am Ende doch anders sein:
: Kurz vor Abgabe siehst du, dass die Schriftart *nicht* "Comic Sans" sein 
soll, sondern "Arial". Wer hätte es gedacht!?\
  In die Einstellungen der Formatvorlagen und bei fünf Vorlagen "Arial" 
  eingestellt… statt 2000 mal alles zu markieren und etwas zu vergessen.

### Vorlagen sogar für Wörter und einzelne Buchstaben…

All diese Vorteile bisher gibt es bei *Absatzvorlagen*. Daneben gibt es aber 
auch noch *Zeichenvorlagen*.\
Mit Zeichenvorlagen kannst du ähnlich komfortable Dinge tun aber sie beziehen 
sich eben auf einzelne Zeichen, Wörter oder Sätze.

Stell dir vor, du machst eine Anleitung wie diese. Dafür willst du spezielle 
Wörter aus Menü-Einstellungen immer wie Schreibmaschinenschrift mit einem 
Rahmen und einem leichten Schatten aussehen lassen.\
Schreibe also deinen Text, erstelle eine entsprechende Zeichenvorlage und 
übertrage sie auf alle Wörter, die so aussehen sollen.\
Wenn du das Aussehen aller Textstellen nachträglich ändern willst: einfach die 
Zeichenvorlage anpassen!

### …und für die nächste Hausarbeit

Hast du alle Einstellungen richtig und alles perfekt eingerichtet, dann 
speichere das Dokument (möglichst ohne super viel Text) als *Dokumentenvorlage* 
ab!\
Damit öffnest du für deine nächste Hausarbeit quasi ein "leeres" Dokument mit 
allen Formatvorlagen - und kannst sofort mit dem Schreiben anfangen.

## Übrigens…

* Eigentlich jedes Office unterstützt Formatvorlagen 
* Arbeite nie, nie, nie, nie *ohne* Formatvorlagen!\
  Office ohne Formatvorlagen ist wie Verreisen und den Hund zu Hause vergessen… 
  du wirst schon sehen, was du davon hast!

# Hilfe

- LibreOffice-Hilfe
  * Vorlagen in Writer:
    * <https://help.libreoffice.org/latest/de/text/swriter/01/05130000.html>
  * Kapitelnummerierung:
    + <https://help.libreoffice.org/latest/de/text/swriter/guide/chapter_numbering.html>
  * Dokumente mit dem Navigator neu Anordnen:
    + <https://help.libreoffice.org/latest/de/text/swriter/guide/arrange_chapters.html>
  * Einfügen von Querverweisen
    + <https://help.libreoffice.org/latest/de/text/swriter/guide/references.html>

* Video-Tutorial "LibreOffice Writer Formatvorlage Einrichten" von 
  Diplomwerkstatt & WeKnow Studienwerkstatt* (Sprache: DE, YouTube):
  + <https://www.youtube.com/watch?v=BnOZ_4a4uMU>
