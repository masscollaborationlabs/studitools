# Dokumente schwärzen - mit LibreOffice

- Für BAföG und Krankenkasse brauchst du Nachweise über dein Einkommen - aber 
  nicht über deine Ausgaben für Getränke, Bücher und Sexspielzeug.

Datenschutz ist wichtig - denn der schützt gar keine Daten, sondern dich (und 
deine Freunde, Familie und andere Personen). "Datensparsamkeit" - also Daten 
gar nicht erst anfallen zu lassen - ist deswegen ein grundsätzliches Prinzip 
beim Datenschutz.\
In vielen Fällen darfst und solltest du daher dein Recht auf "informationelle 
Selbstbestimmung" nutzen, um nicht ständig Informationen über dich, deine 
Freunde und Kontakte, Einkünfte, Religion, Sexualität und Ausgaben - kurz: 
Aspekte deiner persönlichen Lebensgestaltung offenzulegen. Die geht nämlich nur 
in Ausnahmefällen andere etwas an.\
Und daher solltest du nicht benötigte Angaben in Dokumenten schwärzen!

Mit 
[LibreOffice](https://blogs.uni-bremen.de/studytools/2020/03/17/libreoffice-dein-office-fuer-alle/) 
kannst du das ganz einfach machen. In der Menüleiste findest du (egal ob in 
Writer, Calc, Impress oder Draw):

`Extras > Redigieren`

Dein Dokument wird dann automatisch im Modul "Draw" von LibreOffice als Bild 
geöffnet, wo du es bearbeiten kannst. Wieder im Menü `Extras > Redigieren` 
kannst du dann wählen, ob du jeweils ein Rechteck oder eine "Freiform" (aus 
mehreren Punkten) zeichnen willst. Hier findest du auch die 
Export-Möglichkeiten, um das Dokument wieder als PDF zu speichern: entweder 
werden die Bereiche einfach weiß gelassen oder wirklich mit schwarzen Balken 
zensiert.

**Tip:**\
*Mit LibreOffice-Draw kannst du auch andere eingescannte Dokumente als Bilder 
öffnen und schwärzen.*

**Hinweis:**\
*Um eine geschwärzte PDF zu exportieren musst du über die Funktion bei `Extras > Redigieren` gehen!\
Dabei wird dein Dokument nämlich auch in ein Bild umgewandelt, in dem der 
zugrundeliegende Text nicht mehr enthalten ist. Ansonsten wäre deine Schwärzung 
nämlich sinnlos - der Text wäre im Hintergrund vorhanden und ließe sich 
wiederherstellen.*

## Hilfe

- Offizielle LibreOffice-Hilfe zum Redigieren (Sprache: DE):
  * <https://help.libreoffice.org/latest/de/text/shared/guide/redaction.html>
