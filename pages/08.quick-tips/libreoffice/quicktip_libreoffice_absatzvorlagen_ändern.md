# Quick-Tip: Absatzvorlagen in LibreOffice schnell ändern

In der Seitenleiste bei den Formatvorlagen gibt es einen "Gießkannenmodus".
Damit wählst du nur noch eine Formatvorlage aus der Liste aus und klickst einmal in jeden Absatz, der die entsprechende Absatzvorlage bekommen soll (oder markierst Buchstaben / Wörter, die Zeichenvorlagen bekommen sollen). 

Alternativ kannst du in LibreOffice bei `Bearbeiten > Suchen & Ersetzen` auch nach Absatzvorlagen suchen und diese durch eine andere Vorlage ersetzen lassen.

