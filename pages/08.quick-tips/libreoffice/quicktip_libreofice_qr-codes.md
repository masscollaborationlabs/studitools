# Quick-Tip: QR-Codes mit LibreOffice erstellen

In 
[LibreOffice](https://blogs.uni-bremen.de/studytools/2020/03/17/libreoffice-dein-office-fuer-alle/) 
ist ein QR-Code-Generator eingebaut. Damit kannst QR-Codes aus einem beliebigen 
Text erstellen - z.B. für eine Webseite.\
Das Tolle ist: es funktioniert sowohl in LibreOffice Writer, als auch bei 
Impress, Draw oder Calc.

Gehe einfach an folgende Stelle im Menü:

`Einfügen > Objekt > QR-Code`

Text eingeben, `OK` anklicken. Fertig.

# Hilfe

- Online-Hilfe für QR-Codes in LibreOffice: 
  * <https://help.libreoffice.org/latest/de/text/shared/guide/qrcode.html>
