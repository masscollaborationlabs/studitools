# Asciidoc - Vorschau im Browser ansehen

* Du hast jetzt einfach mal etwas als Asciidoc geschrieben… aber wie kannst du 
  dir das jetzt ansehen?  

Ganz einfach:\
mit deinem Browser!

Es gibt eine Erweiterung für alle gängigen Browser, in dem du Asciidoc-Dateien 
öffnen kannst. Sie zeigt dir eine gerenderte HTML Live-Vorschau deiner 
Asciidoc-Datei an. Wenn du währenddessen die Datei bearbeitest und speicherst, 
wird dir die Datei erneut mit dem geänderten Text angezeigt. 

Damit kannst du wirklich mit *jedem* Editor arbeiten - und trotzdem deinen 
fertigen Text sehen.

<https://github.com/asciidoctor/asciidoctor-browser-extension>
