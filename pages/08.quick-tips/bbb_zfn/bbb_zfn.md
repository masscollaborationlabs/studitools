# Meetings mit BigBlueButton - teilnehmen auch ohne Uni-Account

BigBlueButton ist eine freie Konferenz-Software (Open Source), mit der du auch deinen Bildschirm oder eine Präsentation zeigen kannst. Außerdem gibt es ein gemeinsames Whiteboard, unter-Räume für Kleingruppen ("Break-Out-Rooms") und einen gemeinsamen Chat und Notizen.

Neben den ["Meetings" in Stud.IP](https://blogs.uni-bremen.de/studytools/2020/06/04/meetings-in-studiengruppen-mit-bigbluebutton/) wird BigBlueButton auch vom ZfN angeboten.  
Was der Unterschied dabei ist? Eigentlich nur, dass es unterschiedliche Schwerpunkte hat: in Stud.IP vor allem für die Lehre und Veranstaltungen, vom ZfN für alle Universitäts-Angehörigen bzw. Mitarbeiter\*innen.

Mit BigBlueButton können sowohl beim ZfN als auch in Stud.IP Menschen *ohne Uni-Account* (ohne Zugang zu Stud.IP), Konferenzen oder Seminare beitreten. Nur **die erste Person einer Sitzung muss sich mit Namen und Passwort vom Uni-Account anmelden**, alle anderen können der Konferenz einfach so beitreten. Bei BigBlueButton in Stud.IP kannst du einen Link erstellen, mit dem du Personen ohne Uni-Account einladen kannst werden.

<https://bbb.zfn.uni-bremen.de/>

**Anmerkung zu Videokonferenzen:**\
*Videokonferenzen funktionieren generell unterschiedlich gut. Das kann an vielem liegen: Internetverbindung, Router, Browser-Version, Betriebssystem, der Konferenz-Software… \
Außerdem ändert sich das ständig, da die Software-Entwicklung sehr schnell geht. Möglicherweise funktioniert daher eine andere Software in eurer Gruppe besser, ist angemessener für euren Zweck oder die Gruppengröße.*\
**Achte immer darauf, dass du die aktuellste Version deines Browsers installiert hast.**
- ZMML Info-Portal zu digitaler Lehre:
  - <https://www.uni-bremen.de/zmml/lehre-digital/digitale-werkzeuge/kommunikation-und-information>

## Hilfe

- Informationen zu BigBlueButton vom ZfN:
  - <https://www.uni-bremen.de/zfn/weitere-it-dienste/chat-konferenzsysteme/bigbluebutton-videokonferenzen> 
- Informationen des ZMML, um BigBlueButton einzurichten und zu nutzen:
  - <https://www.uni-bremen.de/zmml/lehre-digital/digitale-werkzeuge/videokonferenzen-mit-bigbluebutton>
- Dokumentation und Handbuch zu BigBlueButton:
  * <https://docs.bigbluebutton.org/>
* offizielle Video-Tutorials für BigBlueButton:
  * <https://bigbluebutton.org/html5/>
