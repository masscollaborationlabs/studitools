# Feste Konferenzräume in BigBlueButton anlegen

* In welcher E-Mail stand nochmal der Link zu dem Meeting?
* Hatte irgendwer aus der Gruppe überhaupt ein Meeting angelegt?

Mach dir das Meeten doch ein bisschen einfacher - und richte dir einen 
*dauerhaften* Konferenzraum ein!

**Tipp:**\
*So einen dauerhaften Raum kannst du als [Lesezeichen in deinem 
Browser](https://blogs.uni-bremen.de/studytools/2021/03/05/lesezeichen-im-browser/) 
speichern, um ihn sofort aufrufen zu können..*

Das kannst du ganz einfach mit [BigBlueButton (BBB) beim 
ZfN](https://blogs.uni-bremen.de/studytools/2020/06/09/meetings-mit-bigbluebutton-teilnehmen-auch-ohne-uni-account/) 
machen. Dazu musst du nur zum BBB-Portal des ZfN gehen und dich dort anmelden.

Du hast dort *immer* einen `Startraum`, kannst aber beliebig viele weitere 
Räume hinzufügen. Jeder Raum hat dabei eine unveränderliche URL (wird dir 
angezeigt in einem kleinen Feld, wenn du auf einen Raum klickst). Unter dieser 
URL ist der Raum *immer* erreichbar!\
Aber weil du vielleicht unterschiedliche Räume haben willst, kannst du sie dir 
auch unterschiedlich einrichten: den Namen, wer Moderations-Rechte bekommt, wie 
Personen beitreten, ein zusätzliches Passwort und viele andere Einstellungen.

Mit einem Klick auf das "3-Punkte-Menü" kannst du den Raum sogar mit anderen 
Menschen teilen (ihr seid dann beide "Besitzer\*innen" und könnt den Raum 
verwalten).

Und mit einem Klick auf `Start`… naja, du weißt schon…

**Hinweis:**\
*Wenn du in den Raum-Einstellungen `Jeder Teilnehmer kann die Konferenz 
starten` aktivierst, können andere Personen mit Uni-Account sich als 
"Organisator\*in" anmelden und die Konferenz starten. Das kann ziemlich 
nützlich sein, damit bei einer Verspätung z.B. nicht alle auf eine Person 
warten müssen.*
