# Sticken mit Inkscape: Ink/Stitch

Mit 
[Inkscape](https://blogs.uni-bremen.de/studytools/2020/10/08/inkscape-vektorgrafiken-fuer-logos-layouts-laser-cutter/) 
kannst du nicht nur Grafiken am Computer anfertigen, sondern kannst deine Werke 
sogar anfassen!

Mit der Erweiterung *Ink/Stitch* fertigst du Motive für Stickmachinen an. Diese 
lassen sich dann an die Stickmaschine übertragen, die dann deine Entwürfe auf 
Stoffe stickt - Schriftzüge, Badges, Symbole… was du willst.

<https://inkstitch.org/>

## Hilfe

- Offizielle Handbuch-Seite von Ink/Stitch (Sprache: EN):
  * <https://inkstitch.org/docs/install/>
- Tutorials aus der Ink/Stitch-Community (Sprache EN):
  * <https://inkstitch.org/tutorials/>

- Offizieller Kanal von Ink/Stitch auf YouTube (Sprache EN/DE):
  * <https://www.youtube.com/channel/UCJCDCFuT_xQoI55e10HRiRw/videos>
- Video-Präsentation *Talk: Stickdateien erstellen mit Inkscape und Ink/Stitch (AberDerBart)* von STRATUM 0 (YouTube, Sprache:DE):
  * <https://www.youtube.com/watch?v=_JiuSqDvFTo>
