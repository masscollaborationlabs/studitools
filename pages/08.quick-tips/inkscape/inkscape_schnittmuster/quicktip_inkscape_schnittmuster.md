# Schnittmuster für Stoffe mit Inkscape erstellen

* All deine Hosen sind kaputt?
* "One size fits" allen außer dir?

Falls du dir schon mal überlegt hast selbst Kleidung zu nähen oder zu ändern: 
Inkscape kann dir auch dabei helfen. Mit dem vielseitigen Programm für 
Vektorgrafiken lassen sich nämlich auch Schnittmuster für Textilien anfertigen!

Vorlagen von Schnittmustern kannst du mit Inkscape so zusammenstellen, dass du 
z.B. nur noch deine geeignete Größe auf der Vorlage sichtbar machst. Außerdem 
kannst du Vorlagen und einzelne Teile auf einem großen Bogen Papier (z.B. A0) 
so anordnen, dass du das Schnittmuster in dieser Größe auch ausdrucken (lassen) 
kannst.

**Hinweis:**\
*Zu Hause haben die meisten Menschen nicht gerade einen Drucker, der 
Schnittmuster in der benötigten Größe (z.B. A0) ausdrucken kann. Solche 
Ausdrucke heißen "Plot" bzw. werden "geplottet" - und lassen sich über z.B.  
Copy-Shops und andere Anbieter*innen bestellen.*

**Tip:**\
*Falls du einen Beamer besitzt, kannst du diesen auch z.B. an einer Wand 
montieren und so das Schnittmuster auf den Stoff projizieren.*

## Hilfe

- Video-Kanal von Marta Gvozdinskaya (YouTube; Sprache: EN)
  * <https://www.youtube.com/c/MartaGvozdinskaya/videos>
- Blog *drehumdiebolzeningenieur* zum Plotten von A4-Bögen auf A0 mit Inkscape:
  * <https://drehumdiebolzeningenieur.com/2015/10/16/download-schnittmuster-aus-a4-kacheln-ausplotten/>
- "Introduction to using projectors for pdf sewing patterns" auf *craftstorming.com* (Sprache: EN):
  * <https://www.craftstorming.com/2020/05/introduction-to-using-projectors-for-pdf-sewing-patterns>
