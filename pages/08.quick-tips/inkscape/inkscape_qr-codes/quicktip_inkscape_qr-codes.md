---
title: 'Quick-Tipp: QR-Codes mit Inkscape erstellen'
date: '08-10-2020'
publish_date: '08-10-2020'
taxonomy:
    category:
        - Grafik
        - Gestaltung
        - Quicktip
---

![](img/inkscape_qr-code.png)

===

# Quick-Tipp: QR-Codes mit Inkscape erstellen

Das Vektorgrafikprogramm 
[Inkscape](https://blogs.uni-bremen.de/studytools/2020/10/08/inkscape-vektorgrafiken-fuer-logos-layouts-laser-cutter/) 
kann nicht nur Layouts, Logos, Plakate, Schnittmuster und Lasercutter-Vorlagen 
zaubern.\
Auch QR-Codes kannst du damit ganz einfach erstellen:

Im Menü unter `Erweiterungen > Rendern > Strichcode` findest alles Nötige, um 
aus einem Text einen QR-Code oder klassischen Strichcode zu generieren.

Easy-Peasy!
