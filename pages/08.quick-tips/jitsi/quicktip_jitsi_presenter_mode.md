# Quick-Tipp: "Presenter-Mode" in Jitsi aktivieren

Die vom ZfN angebotene [Videokonferenz-Software 
Jitsi-Meet](https://blogs.uni-bremen.de/studytools/2020/06/04/planen-plaudern-pesprechen-mit-jitsi-meet/) 
sieht einfach aus, hat aber viele nützliche Funktionen.

Du willst deinen Bildschirm freigeben *und* wie bei einer Präsentation dein 
Webcam-Bild trotzdem sichtbar schalten? Klar!

Klicke einfach während deiner Bildschirmfreigabe wieder auf "Kamera freigeben" 
und schon wird dein Webcam-Bild unten rechts neben deiner Bildschirmfreigabe 
angezeigt!

## Hilfe

<https://jitsi.org/blog/introducing-presenter-mode/>
