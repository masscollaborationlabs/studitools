# Studytools - Tools und Tipps zum digital gestützten Studium an der Uni Bremen (Startseite)


Hi,

Willkommen bei den *Studytools* der Uni Bremen!

## Was findest du auf dieser Webseite?

Auf dieser Webseite haben wir (vom ZMML) dir Dienste, Angebote und Materialien der Uni Bremen und anderen Stellen zusammengestellt, die dir während deines Studiums (und darüber hinaus) nützlich sein können.

Lehrende finden hier sicherlich auch nützliche Tipps, aber das Blog richtet sich vor allem an Studierende. ;)

## Wusstest du z.B., dass…

- du 100 GB Speicherplatz für all deine Dateien in der Campus-Cloud der Uni Bremen hast?
- du in Stud.IP eigene Studiengruppen für Gruppenarbeiten anlegen kannst, die wie eine eigene Veranstaltung nur für dich bzw. euch sind?
- du dank dem Zugang der Bibliothek Filme streamen kannst?
- du dein Literaturverzeichnis nicht selbst erstellen und sortieren musst, weil es dafür extra Programme gibt?

## Wo findest du was?

Wir haben uns bemüht, alles in übersichtlichen Kategorien zu Ordnen. 
Wenn du dort nichts passendes für dich findest, benutze einfach mal die *Suchfunktion* oder sieh dir die [Liste aller Tools](https://blogs.uni-bremen.de/studytools/liste-aller-tools/) an.

## Feedback

Wir haben etwas übersehen?
Du hast einen Tipp für uns?
Das war alles mega-hilfreich und du willst einfach mal "Danke" sagen?

Schick uns gern eine E-Mail und wir sehen uns das mal an, kümmern uns drum oder schreiben "Danke" zurück:

**E-Mail:** `studytools@uni-bremen.de`

                   *Viel Spaß beim Stöbern und viel Erfolg!*

